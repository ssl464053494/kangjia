package com.kangjia.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "ts_report_third")
public class ReportThird implements java.io.Serializable {

	private static final long serialVersionUID = 1142266690137551130L;
	private ReportThirdId id;
	private Long firstId;
	private Long secondId;
	private Integer score;
	private Integer lvl;
	private Integer stand;

	public ReportThird() {
	}

	@EmbeddedId
	@AttributeOverrides({ @AttributeOverride(name = "ymd", column = @Column(name = "ymd", nullable = false)),
			@AttributeOverride(name = "uid", column = @Column(name = "uid", nullable = false)),
			@AttributeOverride(name = "targetId", column = @Column(name = "target_id", nullable = false)) })
	public ReportThirdId getId() {
		return this.id;
	}

	public void setId(ReportThirdId id) {
		this.id = id;
	}

	@Column(name = "first_id")
	public Long getFirstId() {
		return this.firstId;
	}

	public void setFirstId(Long firstId) {
		this.firstId = firstId;
	}

	@Column(name = "second_id")
	public Long getSecondId() {
		return this.secondId;
	}

	public void setSecondId(Long secondId) {
		this.secondId = secondId;
	}

	@Column(name = "score")
	public Integer getScore() {
		return this.score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	@Column(name = "lvl")
	public Integer getLvl() {
		return this.lvl;
	}

	public void setLvl(Integer lvl) {
		this.lvl = lvl;
	}

	@Column(name = "stand")
	public Integer getStand() {
		return this.stand;
	}

	public void setStand(Integer stand) {
		this.stand = stand;
	}

}