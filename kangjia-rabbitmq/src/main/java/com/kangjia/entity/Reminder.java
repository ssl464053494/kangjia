package com.kangjia.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ts_reminder")
public class Reminder implements Serializable {

	private static final long serialVersionUID = -249974499211318533L;

	private Long uid;
	private Long crtime;

	@Id
	@Column(name = "uid", unique = true, nullable = false)
	public Long getUid() {
		return uid;
	}

	public void setUid(Long uid) {
		this.uid = uid;
	}

	@Column(name = "crtime")
	public Long getCrtime() {
		return crtime;
	}

	public void setCrtime(Long crtime) {
		this.crtime = crtime;
	}

}
