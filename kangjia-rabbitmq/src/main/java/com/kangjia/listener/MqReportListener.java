package com.kangjia.listener;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.core.ChannelAwareMessageListener;
import org.springframework.beans.factory.annotation.Autowired;

import com.kangjia.service.MqReportService;
import com.rabbitmq.client.Channel;

import lombok.extern.slf4j.Slf4j;

/**
 * mq_report队列监听
 * 
 * @author sunshulin
 *
 */
@Slf4j
public class MqReportListener implements ChannelAwareMessageListener {

	@Autowired
	private MqReportService mqReportService;
	
	/**
	 * mq_report队列监听
	 */
	public void onMessage(Message message, Channel channel) throws Exception {
		String msg = new String(message.getBody());
		log.info("接收到报告id={}", msg);
		// 确认消息已收到
		channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
		mqReportService.handler(msg);
		log.info("处理完报告id={}内容", msg);
	}

}
