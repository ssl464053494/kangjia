package com.kangjia.service;

import org.apache.commons.lang3.StringUtils;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kangjia.dao.MongoDao;
import com.kangjia.dao.MongoReportCondition;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class MqReportService {

	@Autowired
	private MongoDao mongoDao;

	/**
	 * 处理报告内容数据,将最终处理结果保存到hbase里
	 * 
	 * @param reportId
	 */
	public void handler(String reportId) {
		String reportJson = getDocument(reportId);
		if (StringUtils.isBlank(reportJson)) {
			log.warn("reportId={} no find", reportId);
			return;
		}
		log.info("reportId={}, json={}", reportId, reportJson);
		ParserService service = new ParserService(reportJson);
		service.getAllFirstInfo();

		service.getUid();
		service.getTime();
		service.getTotalScore();
		service.getId();
		log.info(service.getFirsts().toString());
		log.info(service.getSeconds().toString());
		log.info(service.getThirds().toString());
		
		
	}

	/**
	 * 获取报告内容转换json
	 * 
	 * @param reportId
	 * @return
	 */
	private String getDocument(String reportId) {
		MongoReportCondition condition = new MongoReportCondition(reportId);
		Document document = mongoDao.findOne(condition);
		return document != null ? document.toJson() : null;
	}
}
