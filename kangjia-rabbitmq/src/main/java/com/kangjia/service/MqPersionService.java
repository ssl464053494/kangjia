package com.kangjia.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kangjia.bean.IndexBean;
import com.kangjia.dao.MongoDao;
import com.kangjia.dao.MongoReportCondition;
import com.kangjia.dao.ReportDao;
import com.kangjia.entity.Reminder;
import com.kangjia.entity.ReportFirst;
import com.kangjia.entity.ReportFirstId;
import com.kangjia.entity.ReportSecond;
import com.kangjia.entity.ReportSecondId;
import com.kangjia.entity.ReportThird;
import com.kangjia.entity.ReportThirdId;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class MqPersionService {

	@Autowired
	private MongoDao mongoDao;
	@Autowired
	private ReportDao reportDao;

	/**
	 * 处理报告内容数据,将最终处理结果保存到数据库里
	 * 
	 * @param reportId
	 */
	public void handler(String reportId) {
		String reportJson = getDocument(reportId);
		if (StringUtils.isBlank(reportJson)) {
			log.warn("reportId={} no find", reportId);
			return;
		}
		log.info("reportId={}, json={}", reportId, reportJson);
		ParserService service = new ParserService(reportJson);
		service.getAllFirstInfo();

		Integer uid = service.getUid();
		Long ymd = service.getTime();

		List<IndexBean> firsts = service.getFirsts();
		List<ReportFirst> fList = handlerFirst(firsts, uid, ymd);
		List<IndexBean> seconds = service.getSeconds();
		List<ReportSecond> sList = handlerSecond(seconds, uid, ymd);
		List<IndexBean> thirds = service.getThirds();
		List<ReportThird> tList = handlerThird(thirds, uid, ymd);
		reportDao.batch(fList, sList, tList);
		
		Integer size = service.get("4");
		Reminder reminder = null;
		int add = 0;
		if (size > 0) {
			reminder = new Reminder();
			add = 7;
		} else {
			size = service.get("3");
			if (size > 0) {				
				reminder = new Reminder();
				add = 15;
			}
		}
		if (reminder != null) {	
			reminder.setUid(new Long(uid));
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DAY_OF_MONTH, add);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			reminder.setCrtime(new Long(sdf.format(cal.getTime())));
			reportDao.add(reminder);
		}
	}
	
	/**
	 * 处理一级指标
	 * 
	 * @param firsts
	 * @param uid
	 * @param ymd
	 * @return
	 */
	private List<ReportThird> handlerThird(List<IndexBean> seconds, Integer uid, Long ymd) {
		List<ReportThird> list = new ArrayList<>();
		for (IndexBean ib : seconds) {
			ReportThird rf = new ReportThird();
			ReportThirdId rfid = new ReportThirdId();
			rfid.setTargetId(new Long(ib.getTargetId()));
			rfid.setUid(new Long(uid));
			rfid.setYmd(ymd);
			rf.setId(rfid);
			rf.setScore(ib.getScore());
			rf.setFirstId(new Long(ib.getFirstId()));
			rf.setSecondId(new Long(ib.getSecondId()));
			rf.setLvl(ib.getAbnormal());
			rf.setStand(ib.getStandard());
			list.add(rf);
		}
		return list;
	}
	
	/**
	 * 处理一级指标
	 * 
	 * @param firsts
	 * @param uid
	 * @param ymd
	 * @return
	 */
	private List<ReportSecond> handlerSecond(List<IndexBean> seconds, Integer uid, Long ymd) {
		List<ReportSecond> list = new ArrayList<>();
		for (IndexBean ib : seconds) {
			ReportSecond rf = new ReportSecond();
			ReportSecondId rfid = new ReportSecondId();
			rfid.setTargetId(new Long(ib.getTargetId()));
			rfid.setUid(new Long(uid));
			rfid.setYmd(ymd);
			rf.setId(rfid);
			rf.setScore(ib.getScore());
			rf.setFirstId(new Long(ib.getFirstId()));
			list.add(rf);
		}
		return list;
	}

	/**
	 * 处理一级指标
	 * 
	 * @param firsts
	 * @param uid
	 * @param ymd
	 * @return
	 */
	private List<ReportFirst> handlerFirst(List<IndexBean> firsts, Integer uid, Long ymd) {
		List<ReportFirst> list = new ArrayList<>();
		for (IndexBean ib : firsts) {
			ReportFirst rf = new ReportFirst();
			ReportFirstId rfid = new ReportFirstId();
			rfid.setTargetId(new Long(ib.getTargetId()));
			rfid.setUid(new Long(uid));
			rfid.setYmd(ymd);
			rf.setId(rfid);
			rf.setScore(ib.getScore());
			list.add(rf);
		}
		return list;
	}

	/**
	 * 获取报告内容转换json
	 * 
	 * @param reportId
	 * @return
	 */
	private String getDocument(String reportId) {
		MongoReportCondition condition = new MongoReportCondition(reportId);
		Document document = mongoDao.findOne(condition);
		return document != null ? document.toJson() : null;
	}
}
