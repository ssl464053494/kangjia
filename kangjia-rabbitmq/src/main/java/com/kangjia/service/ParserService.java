package com.kangjia.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.kangjia.bean.IndexBean;

import net.minidev.json.JSONArray;

public class ParserService {

	private DocumentContext context;

	private List<IndexBean> firsts = new ArrayList<>();
	private List<IndexBean> seconds = new ArrayList<>();
	private List<IndexBean> thirds = new ArrayList<>();

	public ParserService(String reportJson) {
		context = JsonPath.parse(reportJson);
	}

	/**
	 * 获取用户ID
	 * 
	 * @return
	 */
	public Integer getUid() {
		return context.read("$.uid");
	}

	/**
	 * 获取用户报告得分
	 * 
	 * @return
	 */
	public Integer getTotalScore() {
		return context.read("$.totalScore");
	}

	/**
	 * 获取报告ID
	 * 
	 * @return
	 */
	public String getId() {
		return context.read("$.inspectId");
	}

	/**
	 * 获取报告ID
	 * 
	 * @return
	 */
	public Long getTime() {
		String date = context.read("$.inspectDate");
		String[] s = date.split(" ");
		return new Long(s[0].replaceAll("-", ""));
	}

	/**
	 * 指标
	 */
	public void getAllFirstInfo() {
		JSONArray jsonArray = context.read("$.healthIndex");
		Map<?, ?> maps = null;
		for (int i = 0; i < jsonArray.size(); i++) {
			maps = (Map<?, ?>) jsonArray.get(i);
			IndexBean first = createIndexBean(maps, null, null);
			firsts.add(first);
			JSONArray secondJsonArray = (JSONArray) maps.get("secondItems");
			for (int j = 0; j < secondJsonArray.size(); j++) {
				maps = (Map<?, ?>) secondJsonArray.get(j);
				IndexBean second = createIndexBean(maps, first.getTargetId(), null);
				seconds.add(second);
				JSONArray thirdJsonArray = (JSONArray) maps.get("thirdItems");
				for (int k = 0; k < thirdJsonArray.size(); k++) {
					maps = (Map<?, ?>) thirdJsonArray.get(k);
					IndexBean third = createIndexBean(maps, first.getTargetId(), second.getTargetId());
					thirds.add(third);
				}
			}
		}
	}

	/**
	 * 创建index bean
	 * 
	 * @param maps
	 * @return
	 */
	private IndexBean createIndexBean(Map<?, ?> maps, Integer firstId, Integer secondId) {
		IndexBean bean = new IndexBean();
		bean.setAbnormal(maps.get("abnormalLevel") == null ? null : (Integer) maps.get("abnormalLevel"));
		bean.setAge(maps.get("age") == null ? null : (Integer) maps.get("age"));
		bean.setFirstId(firstId);
		bean.setScore(maps.get("score") == null ? null : (Integer) maps.get("score"));
		bean.setSecondId(secondId);
		bean.setStandard(maps.get("inspectStandard") == null ? null : (Integer) maps.get("inspectStandard"));
		bean.setTargetId(maps.get("targetId") == null ? null : (Integer) maps.get("targetId"));
		bean.setTargetName(maps.get("targetName") == null ? null : (String) maps.get("targetName"));
		return bean;
	}

	/**
	 * 重度,中度指标数量
	 * 
	 * @param max
	 * @return
	 */
	public Integer get(String max) {
		String key = "$..thirdItems[?(@.abnormalLevel==" + max + ")]";
		JSONArray jsonArray = context.read(key);
		if (jsonArray.isEmpty()) {
			return 0;
		} else {
			return jsonArray.size();
		}
	}

	public List<IndexBean> getFirsts() {
		return firsts;
	}

	public List<IndexBean> getSeconds() {
		return seconds;
	}

	public List<IndexBean> getThirds() {
		return thirds;
	}
}
