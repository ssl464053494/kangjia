package com.kangjia.dao;

import java.util.ArrayList;
import java.util.List;

import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mongodb.BasicDBObject;
import com.mongodb.Block;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

import lombok.extern.slf4j.Slf4j;

/**
 * 康加报告数据保存mongodb
 * 
 * @author sunshulin
 *
 */
@Repository
@Slf4j
public class MongoDao {

	private MongoCollection<Document> kangjiaReport;

	@Autowired
	public MongoDao(MongoDatabase mdb) {
		kangjiaReport = mdb.getCollection("kangjia_report");
	}

	public Document findOne(MongoReportCondition mrc) {
		List<Document> list = findData(mrc);
		return list.isEmpty() ? null : list.get(0);
	}
	
	public List<Document> findData(MongoReportCondition mrc) {
		log.info("查找原始报告数据 mrc={}", mrc);
		List<Document> list = new ArrayList<>();
		FindIterable<Document> finds = kangjiaReport.find(mrc.createCondition()).sort(new BasicDBObject("crtime", 1))
				.limit(mrc.getLimit());
		finds.forEach(new Block<Document>() {
			public void apply(Document _doc) {
				list.add(_doc);
			}
		});
		return list;
	}
}
