package com.kangjia.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.kangjia.entity.Reminder;
import com.kangjia.entity.ReportFirst;
import com.kangjia.entity.ReportSecond;
import com.kangjia.entity.ReportThird;

@Repository
public class ReportDao {

	@Autowired
	private SessionFactory sessionFactory;

	/**
	 * 批量保存报告数据
	 * 
	 * @param firsts
	 * @param seconds
	 * @param thirds
	 */
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void batch(List<ReportFirst> firsts, List<ReportSecond> seconds, List<ReportThird> thirds) {
		Session session = sessionFactory.getCurrentSession();
		for (ReportFirst rf : firsts) {
			session.saveOrUpdate(rf);
		}
		session.flush();
		for (ReportSecond rs : seconds) {
			session.saveOrUpdate(rs);
		}
		session.flush();
		for (ReportThird rt : thirds) {
			session.saveOrUpdate(rt);
		}
	}
	
	/**
	 * 保存7天提醒
	 */
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void add(Reminder reminder) {
		Session session = sessionFactory.getCurrentSession();
		session.saveOrUpdate(reminder);
	}
}
