package com.kangjia.bean;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class IndexBean {

	/**
	 * 指标ID
	 */
	private Integer targetId;
	/**
	 * 指标名称
	 */
	private String targetName;
	/**
	 * 分数
	 */
	private Integer score;
	/**
	 * 生理年龄
	 */
	private Integer age;
	/**
	 * 一级指标ID
	 */
	private Integer firstId;
	/**
	 * 二级指标ID
	 */
	private Integer secondId;
	/**
	 * 检测标准
	 */
	private Integer standard;
	/**
	 * 异常程度
	 */
	private Integer abnormal;
}
