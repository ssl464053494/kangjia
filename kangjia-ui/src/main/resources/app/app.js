var required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';

Ext.Loader.setConfig({
	enabled : true,
	paths : {
		'app' : 'app'
	}
});

Ext.override(Ext.grid.RowNumberer, {
	width : 60
});

Ext.Ajax.on('requestexception', function(conn, response, options) {
	var status = response.status;
	var statusText = response.statusText;
	var responseText = response.responseText;
	console.log('status:' + status + 'statusText:' + statusText
			+ 'responseText:' + responseText);
});

Ext.override(Ext.form.Panel, {
	defaults : {
		xtype : 'textfield',
		labelAlign : 'top',
		msgTarget : 'qtip',
		anchor : '100%',
		labelSeparator : ''
	}
});

Ext.override(Ext.data.Store, {
	pageSize : 50
});

Ext.override(Ext.form.FieldSet, {
	defaults : {
		xtype : 'textfield',
		labelAlign : 'top',
		msgTarget : 'qtip',
		anchor : '100%',
		labelSeparator : ''
	}
});

//var baseurl = 'http://127.0.0.1:8081';
//var reporturl = 'http://127.0.0.1:8080';
//var baseurl = 'http://123.57.27.38:8081';
//var reporturl = 'http://123.57.27.38:8080';
var baseurl = 'http://218.17.0.118:8081';
var reporturl = 'http://218.17.0.118:8082';
var widget = Ext.create('app.widget');
var utils = Ext.create('app.util');
var evt = Ext.create('app.event');

Ext.onReady(function() {
	Ext.tip.QuickTipManager.init();
	Ext.Ajax.request({
		url : baseurl + '/a/index',
		method : 'POST',
		success : function(response, opts) {
			var json = Ext.JSON.decode(response.responseText);
			if (json.rpc == 200) {
				utils.setValue('HWBK', json.msg);
				Ext.create('app.login', {
					
				});
			} else {
				Ext.Msg.alert('提示', '系统错误');
			}
		}
	});

});