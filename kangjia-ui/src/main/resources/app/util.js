/**
 * ext 辅助工具类
 * 
 * @author sunshulin
 */
Ext.define("app.util", {
	contasnt : {},
	/**
	 * 获取form表单数据
	 * 
	 * @author sunshulin
	 */
	formdata : function(form) {
		var f = form.getForm();
		var fields = f.getFields();
		var jsonData = {};
		fields.each(function(r) {
			var key = r.getName();
			var value = r.getValue();
			if (value != '') {
				if (r.xtype == 'datefield') {
					value = r.getSubmitValue();
				}
				jsonData[key] = value;
			} else {
				jsonData[key] = null;
			}
		});
		return jsonData;
	},
	/**
	 * 选择记录多选
	 * 
	 * @author sunshulin
	 */
	multiple : function(grid) {
		var records = grid.getSelectionModel().getSelection();
		if (records.length > 0) {
			return records;
		} else {
			return null;
		}
	},
	/**
	 * 选择记录单选
	 * 
	 * @author sunshulin
	 */
	single : function(grid) {
		var records = this.multiple(grid);
		if (records != null && records.length > 0) {
			var record = records[0];
			return record;
		} else {
			return null;
		}
	},
	/**
	 * ext tree 复选框
	 * 
	 * @author sunshulin
	 */
	parent : function(node, checked) {
		var me = this;
		if (checked) {
			var parentNode = node.parentNode;
			if (parentNode != undefined) {
				parentNode.checked = checked;
				if (checked) {
					parentNode.set("checked", true);
				}
				me.parent(parentNode, checked);
			}
		}
	},
	/**
	 * ext tree 复选框
	 * 
	 * @author sunshulin
	 */
	child : function(node, checked) {
		var me = this;
		if (node.hasChildNodes()) {
			node.eachChild(function(child) {
				child.checked = checked;
				if (checked) {
					child.set('checked', true);
				} else {
					child.set('checked', false);
				}
				me.child(child, checked);
			});
		}
	},
	/**
	 * 值是否在数组中存在
	 * 
	 * @author sunshulin
	 */
	inarray : function(value, array) {
		var i = array.length;
		while (i--) {
			if (array[i] === value) {
				return true;
			}
		}
		return false;
	},
	/**
	 * 功能按钮添加
	 * 
	 * @author sunshulin
	 */
	pushbtnarray : function(array, param) {
		var me = this;
		// if (me.inarray(param['btnid'], loginer['authorityids'])) {
		array.push(widget.btn(param));
		// }
	},
	/**
	 * 菜单按钮添加
	 * 
	 * @author sunshulin
	 */
	pushmenuarray : function(array, param) {
		var me = this;
		// if (me.inarray(param['btnid'], loginer['authorityids'])) {
		array.push(param);
		// }
	},
	/**
	 * field 元素,type类型1,2,3,4,5,6,7,8,9表示常用类型
	 * 
	 * @author sunshulin
	 */
	pushfieldarray : function(array, type, param) {
		var field = null;
		switch (type) {
		case '1': // 文本
			field = widget.txt(param);
			break;
		case '2': // 密码框
			field = widget.pass(param);
			break;
		case '3': // 数值框
			field = widget.number(param);
			break;
		case '4': // 隐藏文本
			field = widget.hide(param);
			break;
		case '5': // 日期文本
			field = widget.date(param);
			break;
		case '6': // 文本域
			field = widget.textarea(param);
			break;
		case '7':
			field = widget.file(param);
			break;
		case '8':
			field = widget.display(param);
			break;
		case '9':
			field = widget.html(param);
			break;
		default:
			field = widget.field(type, param);
		}
		if (field != null) {
			array.push(field);
		}
	},
	/**
	 * 加载form数据
	 * 
	 * @author sunshulin
	 */
	loadform : function(param) {
		var me = this;
		var ds = widget.ds({
			clazz : param['model'],
			url : param['url'],
			extraParams : param['extraParams']
		});
		ds.load(function(records, operation, success) {
			var rec = null;
			if (success) {
				rec = records.length > 0 ? records[0] : null;
			}
			if (param['fn']) {
				param['fn'].call(me, rec)
			}
		});
	},
	/**
	 * 获取常量值
	 */
	getconstant : function(key, val) {
		var vs = contasnt[key];
		return vs[val];
	},
	getRsaKey : function() {
		return 'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAlDhh11MUIWLen/IVoLi1wFAQmUETTRyd2qLPhsItW43IjwVUVkGABgcy30FMkSA4/AuLLpHmfrmToKk+FvjIBdsaUT3yBfd3VwSfDzai+6a4C59w52fm/kMog2plGjS2yHhhoRsbELH40f9EI0raGZUkMFVR87uNNH+EfoKMhAxG8s/59hUvenxfIPxFiCvO8hWBE2CfJzYwDnV77ue9fnAT23gSbWzp3JebypHEczwNBI2W3oXT/9p+dtv4CVMeF0iJt4crsh1u3qR8G13/WvneswdILRak3zRISTtBmO4VYhx6C2EREbsPWQzN5UPxq+fzyFPUVpyZAtHDOKBAvQIDAQAB';
	},
	setValue : function(key, val) {
		this.contasnt[key] = val;
	},
	getValue : function(key) {
		return this.contasnt[key];
	}
});
