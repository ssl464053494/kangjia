Ext.define("app.layout.container", {
	extend : 'Ext.container.Viewport',
	layout : 'border',
	requires : [ 'app.m.m1.employee.pass.win', 'app.m.m1.org.model' ],
	initComponent : function() {
		var me = this;
		var titlePanel = me.getTitlePanel();
		var tabPanel = me.getTabPanel();
		me.tabPanel = tabPanel;
		var westPanel = me.getNavigationPanel(tabPanel);
		Ext.apply(me, {
			items : [ titlePanel, westPanel, tabPanel ],
			listeners : {
				beforerender : function() {

				}
			}
		});
		me.callParent();
	},
	getTitlePanel : function() {
		var me = this;
		return Ext.create("Ext.panel.Panel", {
			region : 'north',
			border : false,
			bbar : [ {
				iconCls : 'icon-user',
				text : me.cname
			}, '-', {
				iconCls : 'icon-time',
				text : "当前时间 " + Ext.Date.format(new Date(), 'Y年m月d日')
			}, '->',  {
				iconCls : 'icon-log-out',
				text : "修改密码",
				handler : function() {
					Ext.create('app.m.m1.employee.pass.win', {
						employeeId : me.employeeId,
						_target_ : me
					}).show();
				}
			}, '-', {
				iconCls : 'icon-log-out',
				text : "退出",
				handler : function() {
					Ext.Ajax.request({
						url : baseurl + '/b/logout',
						method : 'POST',
						success : function(response) {
							location.reload();
						}
					});
				}
			} ]
		});
	},
	getNavigationPanel : function(tabPanel) {
		var me = this;
		var store = widget.ts({
			clazz : 'app.m.m1.org.model',
			url : baseurl + '/b/menu'
		});
		var tree = Ext.create('Ext.tree.Panel', {
			width : 240,
			region : 'west',
			title : '功能菜单',
			store : store,
			rootVisible : false,
			displayField : 'cname',
			listeners : {
				cellclick : function(o, td, cellIndex, record, tr, rowIndex, e,
						eOpts) {
					var container = record.get('compant');
					var code = record.get('menuId');
					var panel = tabPanel.getComponent('T_' + code);
					if (panel) {
						tabPanel.setActiveTab(panel);
					} else {
						panel = Ext.create(container, {
							parentTab : tabPanel,
							closable : true,
							tabPanel : tabPanel,
							title : record.get('cname'),
							id : 'T_' + code,
							_target_ : me
						});
						tabPanel.add(panel);
						tabPanel.setActiveTab(panel);
					}
				}
			}
		});
		return tree;
	},
	getTabPanel : function() {
		return Ext.create("Ext.tab.Panel", {
			region : 'center',
			activeTab : 0,
			enableTabScroll : true,
			animScroll : true,
			border : true,
			autoScroll : true,
			split : true,
			items : [ {
				iconCls : 'icon-console',
				title : '工作台'
			} ]
		});
	},
	doRefresh : function() {
		
	},
	addGrid : function(code, container, name, param) {
		var me = this;
		var tabPanel = me.tabPanel;
		var panel = tabPanel.getComponent(code);
		if (panel) {
			tabPanel.setActiveTab(panel);
		} else {
			var defaultConfig = {
				parentTab : tabPanel,
				closable : true,
				tabPanel : tabPanel,
				title : name,
				id : code,
				_target_ : me
			};
			Ext.apply(defaultConfig, param, {});
			panel = Ext.create(container, defaultConfig);
			tabPanel.add(panel);
			tabPanel.setActiveTab(panel);
		}
	}
});