Ext.define('app.validator.verification', {
	extend : 'Ext.form.field.Text',
	alias : 'widget.verifycode',
	inputTyle : 'codefield',
	codeUrl : Ext.BLANK_IMAGE_URL,
	isLoader : true,
	onRender : function(ct, position) {
		this.callParent(arguments);
//		this.codeEl = this.bodyEl.createChild({
		this.codeEl = this.triggerWrap.createChild({
			tag : 'img',
			src : Ext.BLANK_IMAGE_URL,
			style : 'width:75px; height:22px; vertical-align:top; cursor:pointer; margin-left:5px;'
		});
		// this.codeEl.addCls('x-form-verifycode');
		this.codeEl.on('click', this.loadCodeImg, this);
		if (this.isLoader) {
			this.loadCodeImg();
		}

	},
	alignErrorIcon : function() {
		this.errorIcon.alignTo(this.codeEl, 'tl-tr', [2, 0]);
	},
	// 如果浏览器发现url不变，就认为图片没有改变，就会使用缓存中的图片，
	// 而不是重新向服务器请求，所以需要加一个参数，改变url
	loadCodeImg : function() {
		var me = this;
		Ext.Ajax.request({
			url : baseurl + '/a/img',
			method : 'POST',
			success : function(response, opts) {
				var json = Ext.JSON.decode(response.responseText);
				if (json.rpc == 200) {
					me.codeEl.set({
						src : json.msg
					});
				} else {
					Ext.Msg.alert('提示', json.msg);
				}
			}
		});
	},
	listeners : {
		'boxready' : function(me, width, height, eOpts) {
			var inputElWidth = me.getWidth() - me.labelWidth
					- me.codeEl.dom.width - 12;
		}
	}
});