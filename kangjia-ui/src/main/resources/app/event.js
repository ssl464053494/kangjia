/**
 * 事件类,包含ajax请求、form表单提交
 * 
 * @author sunshulin
 */
Ext.define("app.event", {
	/**
	 * post请求
	 * 
	 * @author sunshulin
	 */
	post : function(me, url, param, fn) {
		Ext.Ajax.request({
			url : url,
			method : 'POST',
			params : param,
			success : function(response) {
				var rpc = Ext.decode(response.responseText);
				if (rpc['success']) {
					if (fn) {
						fn.call(me, rpc);
					}
				} else {
					Ext.Msg.alert('提示', rpc.msg + '');
					if (fn) {
						fn.call(me, rpc);
					}
				}
			},
			failure : function(form, action) {
				switch (action.failureType) {
				case Ext.form.action.Action.CLIENT_INVALID:
					Ext.Msg.alert('错误', '表单验证失败,无法提交');
					break;
				case Ext.form.action.Action.CONNECT_FAILURE:
					try {
						var result = Ext.JSON
								.decode(action.response.responseText);
						Ext.Msg.alert('提示', '错误码' + result.rpc + ','
								+ result.msg + '');
					} catch (e) {
						Ext.Msg.alert('错误', 'Ajax communication failed');
					}
					break;
				case Ext.form.action.Action.SERVER_INVALID:
					Ext.Msg.alert('错误', action.result.msg);
				}
			}
		});
	},
	/**
	 * 表单提交
	 * 
	 * @author sunshulin
	 */
	submit : function(formpanel, url, params, fn) {
		var me = this;
		var form = formpanel.getForm();
		if (form.isValid()) {
			form.submit({
				clientValidation : true,
				url : url,
				params : params,
				waitMsg : '处理中，请稍后...',
				waitTitle : '等待',
				success : function(form, action) {
					if (action.result.rpc == 200) {
						if (fn) {
							fn.call(me, action.result);
						}
					} else {
						var code = action.result.rpc;
						var msg = action.result.msg;
						Ext.Msg.alert('提示', '错误码' + code + ',' + msg + '');
					}
				},
				failure : function(form, action) {
					switch (action.failureType) {
					case Ext.form.action.Action.CLIENT_INVALID:
						Ext.Msg.alert('错误', '表单验证失败,无法提交');
						break;
					case Ext.form.action.Action.CONNECT_FAILURE:
						try {
							var result = Ext.JSON
									.decode(action.response.responseText);
							Ext.Msg.alert('提示', '错误码' + result.rpc + ','
									+ result.msg + '');
						} catch (e) {
							Ext.Msg.alert('错误', 'Ajax communication failed');
						}
						break;
					case Ext.form.action.Action.SERVER_INVALID:
						Ext.Msg.alert('错误', action.result.msg);
					}
				}
			});
		}
	},
	/**
	 * 同步请求
	 * 
	 * @author sunshulin
	 */
	async : function(url, param, fn) {
		var me = this;
		Ext.Ajax.request({
			url : url,
			jsonData : param,
			async : false,
			success : function(response) {
				var rpc = Ext.decode(response.responseText);
				if (rpc['success']) {
					if (fn) {
						fn.call(me, rpc);
					}
				}
			},
			failure : function(action) {
				switch (action.failureType) {
				case Ext.form.action.Action.CLIENT_INVALID:
					Ext.Msg.alert('错误', '表单验证失败,无法提交');
					break;
				case Ext.form.action.Action.CONNECT_FAILURE:
					Ext.Msg.alert('错误', 'Ajax communication failed');
					break;
				case Ext.form.action.Action.SERVER_INVALID:
					Ext.Msg.alert('错误', action.result.msg);
				}
			}
		});
	},
	confirm : function(target, msg, url, param, fn) {
		var me = this;
		Ext.MessageBox.confirm('提示', msg, function(id) {
			if (id == 'yes') {
				evt.post(target, url, param, function(res) {
					if (fn) {
						fn.call(me, res);
					}
				});
			}
		});
	}
});
