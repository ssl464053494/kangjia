/**
 * 控件定义类,主要包含表单控件,模型控件等构件
 * 
 * @author sunshulin
 */
Ext.define("app.widget", {
	/**
	 * 表单控件默认配置
	 * 
	 * @author sunshulin
	 */
	defaultconfig : function() {
		return {
			style : 'margin: 0 5px 0 5px',
			labelAlign : 'top',
			columnWidth : '1'
		// afterLabelTextTpl : required
		};
	},
	file : function(param) {
		var option = this.defaultconfig();
		Ext.apply(option, param, {});
		return Ext.create('Ext.form.field.File', option);
	},
	/**
	 * 自定义
	 * 
	 * @author sunshulin
	 */
	field : function(type, param) {
		var option = this.defaultconfig();
		Ext.apply(option, param, {});
		return Ext.create(type, option);
	},
	/**
	 * 文本框
	 * 
	 * @author sunshulin
	 */
	display : function(param) {
		var option = this.defaultconfig();
		Ext.apply(option, param, {});
		return Ext.create('Ext.form.field.Display', option);
	},
	/**
	 * 文本框
	 * 
	 * @author sunshulin
	 */
	txt : function(param) {
		var option = this.defaultconfig();
		Ext.apply(option, param, {});
		return Ext.create('Ext.form.field.Text', option);
	},
	/**
	 * 文本框
	 * 
	 * @author sunshulin
	 */
	html : function(param) {
		var option = this.defaultconfig();
		Ext.apply(option, param, {});
		return Ext.create('Ext.form.field.HtmlEditor', option);
	},
	/**
	 * 隐藏框
	 * 
	 * @author sunshulin
	 */
	hide : function(param) {
		var option = this.defaultconfig();
		Ext.apply(option, param, {});
		return Ext.create('Ext.form.field.Hidden', option);
	},
	/**
	 * 数值框
	 * 
	 * @author sunshulin
	 */
	number : function(param) {
		var option = this.defaultconfig();
		Ext.apply(option, param, {});
		return Ext.create('Ext.form.field.Number', option);
	},
	/**
	 * 文本区域框
	 * 
	 * @author sunshulin
	 */
	textarea : function(param) {
		var option = this.defaultconfig();
		Ext.apply(option, param, {});
		return Ext.create('Ext.form.field.TextArea', option);
	},
	/**
	 * 密码框
	 * 
	 * @author sunshulin
	 */
	pass : function(param) {
		var option = this.defaultconfig();
		Ext.apply(option, {
			inputType : 'password'
		}, {});
		Ext.apply(option, param, {});
		return Ext.create('Ext.form.field.Text', option);
	},
	/**
	 * 日期框
	 * 
	 * @author sunshulin
	 */
	date : function(param) {
		var option = this.defaultconfig();
		Ext.apply(option, param, {});
		return Ext.create('Ext.form.field.Date', option);
	},
	/**
	 * 按钮使用gridpanel,window,panel等
	 * 
	 * @author sunshulin
	 */
	btn : function(param) {
		var option = {};
		Ext.apply(option, param, {});
		return Ext.create('Ext.button.Button', option);
	},
	/**
	 * grid column
	 * 
	 * @author sunshulin
	 */
	column : function(param) {
		var option = {
			menuDisabled : true,
			sortable : false,
			flex : 1
		};
		Ext.apply(option, param, {});
		return Ext.create('Ext.grid.column.Column', option);
	},
	column2 : function(param) {
		var option = {
			menuDisabled : true,
			sortable : false
		};
		Ext.apply(option, param, {});
		return Ext.create('Ext.grid.column.Column', option);
	},
	/**
	 * tree column
	 * 
	 * @author sunshulin
	 */
	treecolumn : function(param) {
		var option = {
			menuDisabled : true,
			sortable : false,
			flex : 1
		};
		Ext.apply(option, param, {});
		return Ext.create('Ext.tree.Column', option);
	},
	/**
	 * form
	 * 
	 * @author sunshulin
	 */
	form : function(param) {
		var option = {
			frame : false,
			border : false,
			region : 'center',
			layout : 'column'
		};
		Ext.apply(option, param, {});
		return Ext.create('Ext.form.Panel', option);
	},
	/**
	 * 窗口
	 * 
	 * @author sunshulin
	 */
	win : function(param) {
		var option = {
			closable : true,
			resizable : false,
			modal : true,
			constrain : true,
			plain : true,
			height : 400,
			width : 600,
			layout : 'border'
		};
		Ext.apply(option, param, {});
		return Ext.create('Ext.window.Window', option);
	},
	/**
	 * 表格
	 * 
	 * @author sunshulin
	 */
	grid : function(param) {
		var me = this;
		var option = {
			border : false,
			frame : false,
			region : 'center'
		};
		Ext.apply(option, param, {});
		return Ext.create('Ext.grid.Panel', option);
	},
	/**
	 * 分页
	 * 
	 * @author sunshulin
	 */
	paging : function(ds) {
		return {
			xtype : "pagingtoolbar",
			store : ds,
			dock : "bottom",
			emptyMsg : "没有数据",
			displayInfo : true,
			displayMsg : "当前显示{0}-{1}条记录 /共{2}条记录 ",
			beforePageText : "第",
			afterPageText : "页/共{0}页"
		};
	},
	/**
	 * 工具条
	 * 
	 * @author sunshulin
	 */
	toolbar : function(buttons) {
		return Ext.create('Ext.toolbar.Toolbar', {
			dock : 'top',
			items : buttons
		})
	},
	/**
	 * datastore 加载数据
	 * 
	 * @author sunshulin
	 */
	ds : function(param) {
		var me = this;
		var extraParams = param['extraParams'] || {};
		var option = {
			model : param['clazz'],
			proxy : {
				actionMethods: {
		            create: 'POST',
		            read: 'POST',
		            update: 'POST',
		            destroy: 'POST'
		        },
				type : 'ajax',
				url : param['url'],
				reader : {
					totalProperty : 'total',
					rootProperty : 'data'
				},
				extraParams : extraParams
			},
			listeners : {
				/** store加载数据事件 */
				load : function(store, records, successful, eOpts) {
					if (!successful) {
						Ext.Msg.alert('提示', '数据加载失败!');
					}
				},
				update : function(store, record, operation, eOpts) {
					if (param['upfn']) {
						param['upfn'].call(me, record);
					}
				},
				remove : function(store, record, index, isMove, eOpts) {
					if (param['rmfn']) {
						param['rmfn'].call(me, record);
					}
				},
				add : function(store, records, index, eOpts) {
					if (param['adfn']) {
						param['adfn'].call(me, records);
					}
				}
			}
		};
		return Ext.create('Ext.data.Store', option);
	},
	/**
	 * 查询表单
	 * 
	 * @author sunshulin
	 */
	search : function(param) {
		var me = this;
		var form = null;
		var bl = param['bl'] ? param['bl'] : 'right';
		var array = param['btn'];
		if(!array) {			
			array = new Array();
		}
		array.push({
			text : '查询',
			handler : function() {
				me.doSearch(form, param['ds']);
				if (param['fn']) {
					param['fn'].call(me);
				}
			}
		}, {
			text : '重置',
			handler : function() {
				form.getForm().reset();
			}
		});
		
		form = me.form({
			dock : 'top',
			items : param['items'],
			buttonAlign : bl,
			buttons : array
		});
		return form;
	},
	/**
	 * 查询方法
	 * 
	 * @author sunshulin
	 */
	doSearch : function(form, ds) {
		var extraParams = {};
		var param = utils.formdata(form);
		Ext.apply(ds.getProxy().extraParams, param);
		ds.reload();
	},
	/**
	 * tree store
	 * 
	 * @author sunshulin
	 */
	ts : function(param) {
		var extraParams = param['extraParams'] || {};
		var option = {
			model : param['clazz'],
			proxy : {
				actionMethods: {
		            create: 'POST',
		            read: 'POST',
		            update: 'POST',
		            destroy: 'POST'
		        },
				type : 'ajax',
				url : param['url'],
				extraParams : extraParams
			},
			listeners : {
				load : function(o, records, successful, eOpts) {
					if (!successful) {
						Ext.Msg.alert('提示', '数据加载失败!');
					}
				}
			}
		};
		return Ext.create('Ext.data.TreeStore', option);
	}
});