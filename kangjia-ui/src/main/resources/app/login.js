Ext.define("app.login", {
	requires : [ 'app.layout.container', 'app.validator.verification' ],
	constructor : function() {
		var me = this;
		me.account = widget.txt({
			name : 'login',
			fieldLabel : '登录账户'
		});
		me.pass = widget.pass({
			name : 'pass',
			fieldLabel : '登录密码'
		});
		me.vcode = Ext.create('app.validator.verification', {
			style : 'margin: 0 5px 0 5px',
			labelAlign : 'top',
			columnWidth : '1',
			codeUrl : baseurl + '/img',
			name : 'code',
			fieldLabel : '验证码'
		});
		me.loginbtn = widget.btn({
			text : '登录',
			scope : me,
			handler : me.login
		});
		me.form = widget.form({
			bodyPadding : '10 10 0',
			items : [ me.account, me.pass, me.vcode ]
		});
		me.win = widget.win({
			width : 400,
			height : 300,
			closable : false,
			title : '身份认证',
			bodyStyle : 'background:#ffc; padding:0px;',
			items : [ me.form ],
			buttons : [ me.loginbtn ]
		}).show();
	},
	login : function() {
		var me = this;
		var encrypt = new JSEncrypt();
		encrypt.setPublicKey(utils.getRsaKey());
		var account = encrypt.encrypt(me.account.getValue());
		var pass = encrypt.encrypt(me.pass.getValue());
		var code = me.vcode.getValue();		
		evt.post(me, baseurl + '/a/login', {
			'login' : account,
			'pass': pass,
			'code' : code
		}, function(rec) {
			if(rec.rpc == 200) {
				me.win.close();
				Ext.create('app.layout.container', {
					cname : rec.data.employeeName,
					employeeId : rec.data.employeeId
				}).show();				
			} else {
				me.vcode.loadCodeImg();
			}
		});
	}
});