Ext.define("app.m.m2.device.form", {
	extend : 'Ext.form.Panel',
	border : false,
	layout : 'column',
	region : 'center',
	requires : [ 'app.m.m2.device.model', 'app.m.m1.store.field' ],
	initComponent : function() {
		var me = this;
		var fields = me.createfield();
		Ext.apply(me, {
			items : fields,
			listeners : {
				afterrender : function() {
					if (me.deviceId) {
						utils.loadform({
							form : me,
							model : 'app.m.m2.device.model',
							url : baseurl + '/b/device/find',
							extraParams : {
								'deviceId' : me.deviceId
							},
							fn : function(rec) {
								me.getForm().loadRecord(rec);
								me.getForm().findField('sn').setEditable(false);
								me.getForm().findField('mac').setEditable(false);
							}
						});
					}
				}
			}
		});
		me.callParent(arguments);
	},
	createfield : function() {
		var me = this;
		var array = new Array();
		utils.pushfieldarray(array, '1', {
			name : 'sn',
			afterLabelTextTpl : required,
			fieldLabel : '序列号',
			allowBlank : false,
			minLength : 1,
			maxLength : 17
		});
		utils.pushfieldarray(array, '1', {
			name : 'mac',
			afterLabelTextTpl : required,
			fieldLabel : 'mac地址',
			allowBlank : false,
			minLength : 1,
			maxLength : 17
		});
		utils.pushfieldarray(array, '4', {
			name : 'storeId'
		});
		utils.pushfieldarray(array, 'app.m.m1.store.field', {
			name : 'storeName',
			afterLabelTextTpl : required,
			fieldLabel : '所在门店',
			allowBlank : false,
			columnWidth : 1,
			_target_ : me,
			btn : false
		});
		return array;
	},
	setStore : function(rec) {
		var me = this;
		me.getForm().findField('storeId').setValue(rec.get('storeId'));
		me.getForm().findField('storeName').setValue(rec.get('cname'));
	}
});