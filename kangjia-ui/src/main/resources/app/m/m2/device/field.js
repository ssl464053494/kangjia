Ext.define('app.m.m2.device.field', {
	extend : 'Ext.form.field.Picker',
	editable : false,
	alias : 'widget.devicefield',
	requires : [ 'app.m.m2.device.grid' ],
	onTriggerClick : function() {
		var me = this;
		var tree = Ext.create('app.m.m2.device.grid', {
			region : 'center',
			url : me.url,
			btn : false,
			extraParams : me.extraParams
		});
		var win = widget.win({
			width : 600,
			items : [ tree ],
			buttons : [ {
				text : '确认',
				handler : function() {
					var rec = tree.getDeviceRecord();
					if (rec != null) {
						me._target_.setDevice(rec);
						win.close();
					} else {
						Ext.Msg.alert('提示', '选择设备');
						return;
					}
				}
			}, {
				text : '关闭',
				handler : function() {
					win.close();
				}
			} ]
		}).show();
	}
});