Ext.define('app.m.m2.device.model', {
	extend : 'Ext.data.Model',
	fields : [ {
		name : 'deviceId',
		type : 'int'
	}, {
		name : 'sn',
		type : 'string'
	}, {
		name : 'mac',
		type : 'string'
	}, {
		name : 'orgId',
		type : 'int'
	}, {
		name : 'storeId',
		type : 'int'
	}, {
		name : 'employeeId',
		type : 'int'
	}, {
		name : 'flag',
		type : 'int'
	}, {
		name : 'code',
		type : 'string'
	} , {
		name : 'storeName',
		type : 'string'
	}, {
		name : 'orgName',
		type : 'string'
	}]
});