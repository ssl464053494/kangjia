Ext.define("app.m.m2.device.win", {
	extend : 'Ext.window.Window',
	title : '编辑设备',
	height : 400,
	width : 400,
	closable : true,
	resizable : false,
	modal : true,
	constrain : true,
	plain : true,
	requires : [ 'app.m.m2.device.form' ],
	initComponent : function() {
		var me = this;
		var panel = Ext.create('app.m.m2.device.form', {
			deviceId : me.deviceId
		});
		Ext.apply(me, {
			layout : {
				type : 'border'
			},
			items : [ panel ],
			buttons : [ {
				text : '确认',
				bodyStyle : 'padding:0 0 0 5px',
				handler : function() {
					var param = {
						deviceId : me.deviceId
					};
					var url = baseurl + '/b/device/editor';
					evt.submit(panel, url, param, function(rec) {
						me.close();
						me._target_.doRefresh();
						Ext.Ajax.request({
							url : reporturl + '/b/report/bind',
							params : {
								sn : rec.data.sn
							},
							method : 'POST',
							success : function(response, opts) {								
							}
						});
					});
				}
			}, {
				text : '关闭',
				bodyStyle : 'padding:0 0 0 5px',
				handler : function() {
					me.close();
				}
			} ]
		});
		me.callParent(arguments);
	}
});