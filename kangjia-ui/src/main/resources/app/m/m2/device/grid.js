Ext.define('app.m.m2.device.grid', {
	extend : "Ext.grid.Panel",
	requires : [ 'app.m.m2.device.model', 'app.m.m2.device.win', 'app.m.m1.store.field' ],
	frame : false,
	border : false,
	btn : true,
	initComponent : function() {
		var me = this;
		var columns = me.createcolumns();
		var store = me.createstore();
		var dockitems = new Array();
		var queryarry = me.createquery(store);
		if (queryarry != null) {
			me.qf = widget.search(queryarry);
			dockitems.push(me.qf);
		}
		var toolarray = me.createtool();
		if (toolarray.length > 0) {
			dockitems.push(widget.toolbar(toolarray));
		}
		dockitems.push(widget.paging(store));
		Ext.apply(me, {
			store : store,
			columns : columns,
			singleExpand : me.singleExpand || true,
			dockedItems : dockitems
		});
		store.load();
		me.callParent(arguments);
	},
	createquery : function(store) {
		var me = this;
		var param = {};
		var array = new Array();
		array.push({
			xtype : 'textfield',
			name : 'sn',
			labelWidth : 60,
			style : 'margin: 5px 5px 0 5px',
			fieldLabel : 'sn:',
			labelAlign : 'right',
			labelSeparator : ''
		});
		array.push({
			xtype : 'textfield',
			name : 'mac',
			labelWidth : 60,
			style : 'margin: 5px 5px 0 5px',
			fieldLabel : 'mac:',
			labelAlign : 'right',
			labelSeparator : ''
		});
		array.push({
			xtype : 'hidden',
			name : 'storeId'
		});
		array.push({
			xtype : 'storefield',
			name : 'storeName',
			labelWidth : 60,
			style : 'margin: 5px 5px 0 5px',
			fieldLabel : '门店:',
			labelAlign : 'right',
			_target_ : me,
			btn : false,
			labelSeparator : ''
		});
		param['bl'] = 'center';
		param['items'] = array;
		param['ds'] = store;
		var array = new Array();
		param['btn'] = array;
		return param;
	},
	doAdd : function() {
		var me = this;
		Ext.create('app.m.m2.device.win', {
			_target_ : me
		}).show();
	},
	doUpdate : function() {
		var me = this;
		var rec = utils.single(me);
		if (rec != null) {
			Ext.create('app.m.m2.device.win', {
				deviceId : rec.get('deviceId'),
				_target_ : me
			}).show();
		} else {
			Ext.Msg.alert('提示', '请选择记录');
		}
	},
	doDelete : function() {
		var me = this;
		var rec = utils.single(me);
		if (rec != null) {
			var param = {
					deviceId : rec.get('deviceId')
			};
			evt.confirm(me, '是否删除?', baseurl + '/b/device/delete', param, function() {
				me.store.reload();
			});
		} else {
			Ext.Msg.alert('提示', '请选择记录');
		}
	},
	doStartAndStop : function() {
		var me = this;
		var rec = utils.single(me);
		if (rec != null) {
			var param = {
				deviceId : rec.get('deviceId')
			};
			evt.confirm(me, '是否执行此操作?', baseurl + '/b/device/startAndStop', param, function() {
				me.store.reload();
			});
		} else {
			Ext.Msg.alert('提示', '请选择记录');
		}
	},
	createtool : function() {
		var me = this;
		var array = new Array();
		if (!me.btn) {
			return array;
		}
		utils.pushbtnarray(array, {
			text : '新增',
			handler : function() {
				me.doAdd(me);
			}
		});
		utils.pushbtnarray(array, {
			text : '修改',
			btnid : 49,
			handler : function() {
				me.doUpdate(me);
			}
		});
		utils.pushbtnarray(array, {
			text : '删除',
			btnid : 51,
			handler : function() {
				me.doDelete(me);
			}
		});
		/*utils.pushbtnarray(array, {
			text : '启用/禁用',
			handler : function() {
				me.doStartAndStop(me);
			}
		});*/
		return array;
	},
	createcolumns : function() {
		var columns = new Array();
		columns.push(Ext.create('Ext.grid.RowNumberer'));
		var storeName = widget.column({
			text : '门店',
			dataIndex : 'storeName'
		});
		columns.push(storeName);
		var sn = widget.column({
			text : '序列号',
			dataIndex : 'sn'
		});
		columns.push(sn);
		var mac = widget.column({
			text : 'mac地址',
			dataIndex : 'mac'
		});
		columns.push(mac);
		var flag = widget.column({
			text : '状态',
			dataIndex : 'flag',
			renderer : function(v) {
				return v == 0 ? '启用' : '禁用';
			}
		});
		columns.push(flag);
		return columns;
	},
	createstore : function() {
		return widget.ds({
			clazz : 'app.m.m2.device.model',
			url : baseurl + '/b/device/search'
		});
	},
	doRefresh : function() {
		var me = this;
		me.store.reload();
	},
	setStore : function(rec) {
		var me = this;
		var m = me.qf;
		m.getForm().findField('storeId').setValue(rec.get('storeId'));
		m.getForm().findField('storeName').setValue(rec.get('cname'));
	},
	getDeviceRecord : function() {
		var me = this;
		return utils.single(me);
	}
});