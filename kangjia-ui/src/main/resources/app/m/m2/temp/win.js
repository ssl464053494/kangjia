Ext.define("app.m.m2.temp.win", {
	extend : 'Ext.window.Window',
	title : '编辑模板',
	height : 400,
	width : 400,
	closable : true,
	resizable : false,
	modal : true,
	constrain : true,
	plain : true,
	requires : [ 'app.m.m2.temp.form' ],
	initComponent : function() {
		var me = this;
		var panel = Ext.create('app.m.m2.temp.form', {
			tempId : me.tempId
		});
		Ext.apply(me, {
			layout : {
				type : 'border'
			},
			items : [ panel ],
			buttons : [ {
				text : '确认',
				bodyStyle : 'padding:0 0 0 5px',
				handler : function() {
					var param = {
						tempId : me.tempId
					};
					var url = baseurl + '/b/temp/editor';
					evt.submit(panel, url, param, function(rec) {
						me.close();
						me._target_.doRefresh();
					});
				}
			}, {
				text : '关闭',
				bodyStyle : 'padding:0 0 0 5px',
				handler : function() {
					me.close();
				}
			} ]
		});
		me.callParent(arguments);
	}
});