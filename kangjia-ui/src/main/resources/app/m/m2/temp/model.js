Ext.define('app.m.m2.temp.model', {
	extend : 'Ext.data.Model',
	fields : [ {
		name : 'tempId',
		type : 'int'
	}, {
		name : 'depict',
		type : 'string'
	}, {
		name : 'content',
		type : 'string'
	}]
});