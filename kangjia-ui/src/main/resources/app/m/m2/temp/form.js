Ext.define("app.m.m2.temp.form", {
	extend : 'Ext.form.Panel',
	border : false,
	layout : 'column',
	region : 'center',
	requires : [ 'app.m.m2.temp.model' ],
	initComponent : function() {
		var me = this;
		var fields = me.createfield();
		Ext.apply(me, {
			items : fields,
			listeners : {
				afterrender : function() {
					if (me.tempId) {
						utils.loadform({
							form : me,
							model : 'app.m.m2.temp.model',
							url : baseurl + '/b/temp/find',
							extraParams : {
								'tempId' : me.tempId
							},
							fn : function(rec) {
								me.getForm().loadRecord(rec);
								me.getForm().findField('depict').setEditable(false);
								me.getForm().findField('cname').setEditable(false);
							}
						});
					}
				}
			}
		});
		me.callParent(arguments);
	},
	createfield : function() {
		var me = this;
		var array = new Array();
		utils.pushfieldarray(array, '1', {
			name : 'cname',
			afterLabelTextTpl : required,
			fieldLabel : '模板',
			allowBlank : false,
			minLength : 1,
			maxLength : 17
		});
		utils.pushfieldarray(array, '6', {
			name : 'depict',
			afterLabelTextTpl : required,
			fieldLabel : '描述',
			allowBlank : false
		});
		utils.pushfieldarray(array, '6', {
			name : 'content',
			afterLabelTextTpl : required,
			fieldLabel : '内容',
			allowBlank : false
		});
		return array;
	}
});