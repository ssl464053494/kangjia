Ext.define('app.m.m2.temp.grid', {
	extend : "Ext.grid.Panel",
	requires : [ 'app.m.m2.temp.model', 'app.m.m2.temp.win' ],
	frame : false,
	border : false,
	btn : true,
	initComponent : function() {
		var me = this;
		var columns = me.createcolumns();
		var store = me.createstore();
		var dockitems = new Array();
		var toolarray = me.createtool();
		if (toolarray.length > 0) {
			dockitems.push(widget.toolbar(toolarray));
		}
		dockitems.push(widget.paging(store));
		Ext.apply(me, {
			store : store,
			columns : columns,
			singleExpand : me.singleExpand || true,
			dockedItems : dockitems
		});
		store.load();
		me.callParent(arguments);
	},
	doUpdate : function() {
		var me = this;
		var rec = utils.single(me);
		if (rec != null) {
			Ext.create('app.m.m2.temp.win', {
				tempId : rec.get('tempId'),
				_target_ : me
			}).show();
		} else {
			Ext.Msg.alert('提示', '请选择记录');
		}
	},
	createtool : function() {
		var me = this;
		var array = new Array();
		if (!me.btn) {
			return array;
		}
		utils.pushbtnarray(array, {
			text : '修改',
			btnid : 49,
			handler : function() {
				me.doUpdate(me);
			}
		});
		return array;
	},
	createcolumns : function() {
		var columns = new Array();
		columns.push(Ext.create('Ext.grid.RowNumberer'));
		var tempId = widget.column({
			text : '序号',
			flex : .1,
			dataIndex : 'tempId'
		});
		columns.push(tempId);
		var cname = widget.column({
			text : '模板名称',
			flex : .1,
			dataIndex : 'cname'
		});
		columns.push(cname);
		var depict = widget.column({
			text : '描述',
			flex : .2,
			dataIndex : 'depict'
		});
		columns.push(depict);
		var content = widget.column({
			text : '内容',
			flex : .6,
			dataIndex : 'content'
		});
		columns.push(content);
		return columns;
	},
	createstore : function() {
		return widget.ds({
			clazz : 'app.m.m2.temp.model',
			url : baseurl + '/b/temp/search'
		});
	},
	doRefresh : function() {
		var me = this;
		me.store.reload();
	}
});