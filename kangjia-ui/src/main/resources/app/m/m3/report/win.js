Ext.define("app.m.m3.report.win", {
	extend : 'Ext.window.Window',
	title : '编辑报告',
	height : 400,
	width : 400,
	closable : true,
	resizable : false,
	modal : true,
	constrain : true,
	plain : true,
	requires : [ 'app.m.m3.report.form' ],
	initComponent : function() {
		var me = this;
		var panel = Ext.create('app.m.m3.report.form', {
			storeId : me.storeId,
			inspectId : me.inspectId
		});
		Ext.apply(me, {
			layout : {
				type : 'border'
			},
			items : [ panel ],
			buttons : [ {
				text : '确认',
				bodyStyle : 'padding:0 0 0 5px',
				handler : function() {
					var param = {
						storeId : me.storeId,
						inspectId : me.inspectId
					};
					var url = reporturl + '/b/report/editor';
					evt.submit(panel, url, param, function(rec) {
						me.close();
						me._target_.doRefresh();
					});
				}
			}, {
				text : '关闭',
				bodyStyle : 'padding:0 0 0 5px',
				handler : function() {
					me.close();
				}
			} ]
		});
		me.callParent(arguments);
	}
});