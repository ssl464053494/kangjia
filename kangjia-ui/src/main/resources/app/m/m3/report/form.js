Ext.define("app.m.m3.report.form", {
	extend : 'Ext.form.Panel',
	border : false,
	layout : 'column',
	region : 'center',
	requires : [ 'app.m.m1.store.model' ],
	initComponent : function() {
		var me = this;
		var fields = me.createfield();
		Ext.apply(me, {
			items : fields
		});
		me.callParent(arguments);
	},
	createfield : function() {
		var me = this;
		var array = new Array();
		utils.pushfieldarray(array, '1', {
			name : 'name',
			afterLabelTextTpl : required,
			fieldLabel : '姓名',
			allowBlank : false,
			minLength : 1,
			maxLength : 8
		});
		utils.pushfieldarray(array, '1', {
			name : 'mobile',
			afterLabelTextTpl : required,
			fieldLabel : '电话',
			allowBlank : false,
			minLength : 1,
			maxLength : 11
		});
		return array;
	}
});