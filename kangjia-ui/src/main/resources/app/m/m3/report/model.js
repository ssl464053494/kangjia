Ext.define('app.m.m3.report.model', {
	extend : 'Ext.data.Model',
	fields : [ {
		name : 'userId',
		type : 'string'
	}, {
		name : 'mobile',
		type : 'string'
	}, {
		name : 'name',
		type : 'string'
	}, {
		name : 'crtime',
		type : 'int'
	}, {
		name : 'storeName',
		type : 'string'
	}, {
		name : 'storeId',
		type : 'int'
	}, {
		name : 'inspectId',
		type : 'string'
	}, {
		name : 'dateTime',
		type : 'string'
	} , {
		name : 'score',
		type : 'int'
	} , {
		name : 'employeeName',
		type : 'string'
	} ]
});