Ext.define('app.m.m3.kangjiakpi.relation.panel', {
	extend : "Ext.panel.Panel",
	requires : [ 'app.m.m3.index.type.grid' ],
	reserveScrollbar : true,
	useArrows : true,
	rootVisible : false,
	multiSelect : true,
	singleExpand : true,
	frame : false,
	border : false,
	bodyStyle :'overflow-x:hidden;overflow-y:scroll',
	initComponent : function() {
		var me = this;
		var g1 = me.createGrid(1, 2, '膳食建议---偏高');
		var g2 = me.createGrid(1, 3, '膳食建议---偏低');
		var g3 = me.createGrid(2, 2, '营养建议---偏高');
		var g4 = me.createGrid(2, 3, '营养建议---偏低');
		var g5 = me.createGrid(3, 2, '运动和生活方式建议---偏高');
		var g6 = me.createGrid(3, 3, '运动和生活方式建议---偏低');
		Ext.apply(me, {
			viewConfig : {
				forceFit : true
			},
			items : [ g1, g2, g3, g4, g5, g6 ]
		});
		me.callParent(arguments);
	},
	createGrid : function(classfiy, inspectStandard, title) {
		var me = this;
		return Ext.create('app.m.m3.index.type.grid', {
			title : title,
			url : baseurl + '/b/relation/search',
			targetId : me.targetId,
			classfiy : classfiy,
			inspectStandard : inspectStandard,
			btn : false,
			height : 300
		});
	}
});