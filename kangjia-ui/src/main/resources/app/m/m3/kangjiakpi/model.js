Ext.define('app.m.m3.kangjiakpi.model', {
	extend : 'Ext.data.TreeModel',
	fields : [ {
		name : 'targetId',
		type : 'int'
	}, {
		name : 'inspectName',
		type : 'string'
	}, {
		name : 'parentId',
		type : 'int'
	}, {
		name : 'inspectLevel',
		type : 'int'
	}, {
		name : 'inspectExplain',
		type : 'string'
	}, {
		name : 'guideContent',
		type : 'string'
	}, {
		name : 'improvementProgram',
		type : 'string'
	}, {
		name : 'type',
		type : 'int'
	}, {
		name : 'sorts',
		type : 'int'
	}, {
		name : 'updateTime',
		type : 'string'
	} ]
});