Ext.define('app.m.m3.kangjiakpi.grid', {
	extend : "Ext.tree.Panel",
	requires : [ 'app.m.m3.kangjiakpi.model' ],
	reserveScrollbar : true,
	useArrows : true,
	rootVisible : false,
	multiSelect : true,
	singleExpand : true,
	frame : false,
	border : false,
	initComponent : function() {
		var me = this;
		var columns = me.createcolumns();
		var store = me.createstore();
		var dockitems = new Array();
		var toolarray = me.createtool();
		if (toolarray.length > 0) {
			dockitems.push(widget.toolbar(toolarray));
		}
		dockitems.push(widget.paging(store));
		Ext.apply(me, {
			store : store,
			columns : columns,
			singleExpand : me.singleExpand || true,
			dockedItems : dockitems
		});
		store.load();
		me.callParent(arguments);
	},
	doSync : function() {
		var me = this;
		evt.confirm(me, '是否同步?', baseurl + '/b/kangjiakpi/getBaseData', {}, function() {
			me.store.reload();
		});
	},
	doRelation : function() {
		var me = this;
		var rec = utils.single(me);
		if (rec != null) {
			me._target_.addGrid('target_' + rec.get('targetId'),
					'app.m.m3.kangjiakpi.relation.panel', rec.get('inspectName'), {
						targetId : rec.get('targetId')
					});
		} else {
			Ext.Msg.alert('提示', '请选择记录');
		}
	},
	doClear : function() {
		var me = this;
		evt.confirm(me, '是否清除报告内存?', reporturl + '/b/report/destory', {}, function() {
			Ext.Msg.alert('提示', '清除内存');
		});
	},
	createtool : function() {
		var me = this;
		var array = new Array();
		utils.pushbtnarray(array, {
			text : '同步',
			handler : function() {
				me.doSync(me);
			}
		});
		utils.pushbtnarray(array, {
			text : '关联设置',
			handler : function() {
				me.doRelation(me);
			}
		});
		utils.pushbtnarray(array, {
			text : '清除内存',
			handler : function() {
				me.doClear(me);
			}
		});
		return array;
	},
	createcolumns : function() {
		var columns = new Array();
		columns.push(Ext.create('Ext.grid.RowNumberer'));
		var inspectName = widget.treecolumn({
			text : '指标名称',
			dataIndex : 'inspectName'
		});
		columns.push(inspectName);
		var inspectLevel = widget.column({
			text : '级别',
			dataIndex : 'inspectLevel'
		});
		columns.push(inspectLevel);
		var inspectExplain = widget.column({
			text : '指标解释',
			dataIndex : 'inspectExplain'
		});
		columns.push(inspectExplain);
		var guideContent = widget.column({
			text : '指导意义',
			dataIndex : 'guideContent'
		});
		columns.push(guideContent);
		var suggestContent = widget.column({
			text : '改进建议',
			dataIndex : 'suggestContent'
		});
		columns.push(suggestContent);
		var improvementProgram = widget.column({
			text : '改善方案',
			dataIndex : 'improvementProgram'
		});
		columns.push(improvementProgram);
		return columns;
	},
	createstore : function() {
		return widget.ts({
			clazz : 'app.m.m3.kangjiakpi.model',
			url : baseurl + '/b/kangjiakpi/search'
		});
	},
	doRefresh : function() {
		var me = this;
		me.store.reload();
	}
});