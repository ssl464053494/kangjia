Ext.define('app.m.m3.full.grid', {
	extend : "Ext.grid.Panel",
	requires : [ 'app.m.m3.report.model', 'app.m.m1.store.field', 'app.m.m3.report.win', 'app.m.m2.device.field' ],
	frame : false,
	border : false,
	initComponent : function() {
		var me = this;
		var columns = me.createcolumns();
		var store = me.createstore();
		var dockitems = new Array();
		var toolarray = me.createtool();
		var queryarry = me.createquery(store);
		if (queryarry != null) {
			me.qf = widget.search(queryarry);
			dockitems.push(me.qf);
		}
		if (toolarray.length > 0) {
			dockitems.push(widget.toolbar(toolarray));
		}
		dockitems.push(widget.paging(store));
		Ext.apply(me, {
			store : store,
			columns : columns,
			singleExpand : me.singleExpand || true,
			dockedItems : dockitems
		});
		store.load();
		me.callParent(arguments);
	},
	createquery : function(store) {
		var me = this;
		var param = {};
		var array = new Array();
		array.push({
			xtype : 'hidden',
			name : 'storeId'
		});
		array.push({
			xtype : 'storefield',
			name : 'storeName',
			labelWidth : 60,
			style : 'margin: 5px 5px 0 5px',
			fieldLabel : '门店:',
			labelAlign : 'right',
			btn : false,
			_target_ : me,
			labelSeparator : ''
		});
		array.push({
			xtype : 'devicefield',
			name : 'sn',
			labelWidth : 60,
			style : 'margin: 5px 5px 0 5px',
			fieldLabel : '设备:',
			labelAlign : 'right',
			btn : false,
			_target_ : me,
			labelSeparator : ''
		});
		array.push({
			xtype : 'textfield',
			name : 'mobile',
			labelWidth : 60,
			style : 'margin: 5px 5px 0 5px',
			fieldLabel : '电话:',
			labelAlign : 'right',
			labelSeparator : ''
		});
		array.push({
			xtype : 'textfield',
			name : 'name',
			labelWidth : 60,
			style : 'margin: 5px 5px 0 5px',
			fieldLabel : '姓名:',
			labelAlign : 'right',
			labelSeparator : ''
		});
		array.push({
			xtype : 'datefield',
			name : 'startDate',
			labelWidth : 60,
			style : 'margin: 5px 5px 0 5px',
			fieldLabel : '开始时间:',
			labelAlign : 'right',
			format : 'Y-m-d',
			labelSeparator : ''
		});
		array.push({
			xtype : 'datefield',
			name : 'endDate',
			labelWidth : 60,
			style : 'margin: 5px 5px 0 5px',
			fieldLabel : '结束时间:',
			labelAlign : 'right',
			format : 'Y-m-d',
			labelSeparator : ''
		});
		param['bl'] = 'center';
		param['items'] = array;
		param['ds'] = store;
		var array = new Array();
		array.push({
			text : '导出',
			handler : function() {
				var form = me.qf.getForm();
				var s = form.findField('startDate').getValue();
				var e = form.findField('endDate').getValue();
				if (s == null && e == null) {
					Ext.Msg.alert('提示', '开始时间和结束时间填写其中一个');
					return;
				}
				var param = utils.formdata(me.qf);
				evt.post(me, reporturl + '/b/report/full/export', param, function(rpc) {
					location.href = reporturl + '/a/full/excel?uuid=' + rpc.msg;
				});
			}
		});
		param['btn'] = array;
		return param;
	},
	doUpdate : function() {
		var me = this;
		var rec = utils.single(me);
		if (rec != null) {
			Ext.create('app.m.m3.report.win', {
				storeId : rec.get('storeId'),
				inspectId : rec.get('inspectId'),
				_target_ : me
			}).show();
		} else {
			Ext.Msg.alert('提示', '请选择记录');
		}
	},
	doReport : function() {
		var me = this;
		var rec = utils.single(me);
		if (rec != null) {
			Ext.Ajax.request({
				url : reporturl + '/b/report/pringReport',
				params : {
					storeId : rec.get('storeId'),
					inspectId : rec.get('inspectId')
				},
				method : 'POST',
				success : function(response, opts) {
					var json = Ext.JSON.decode(response.responseText);
					if (json.rpc == 200) {						
						window.open(reporturl + '/a/report/pdf?uuid=' + json.msg, '_blank');
					} else {
						Ext.Msg.alert('提示', '系统错误');
					}
				}
			});
		} else {
			Ext.Msg.alert('提示', '请选择记录');
		}
	},
	createtool : function() {
		var me = this;
		var array = new Array();
		/*utils.pushbtnarray(array, {
			text : '修改',
			handler : function() {
				me.doUpdate(me);
			}
		});*/
		utils.pushbtnarray(array, {
			text : '报告',
			handler : function() {
				me.doReport(me);
			}
		});
		return array;
	},
	createcolumns : function() {
		var columns = new Array();
		columns.push(Ext.create('Ext.grid.RowNumberer'));
		var storeName = widget.column({
			text : '门店',
			dataIndex : 'storeName'
		});
		columns.push(storeName);
		var inspectId = widget.column({
			text : '报告编号',
			dataIndex : 'inspectId'
		});
		columns.push(inspectId);
		var name = widget.column({
			text : '姓名',
			dataIndex : 'name'
		});
		columns.push(name);
		var mobile = widget.column({
			text : '电话',
			dataIndex : 'mobile'
		});
		columns.push(mobile);
		var score = widget.column({
			text : '分数',
			dataIndex : 'score'
		});
		columns.push(score);
		var employee = widget.column({
			text : '店员',
			dataIndex : 'employeeName'
		});
		columns.push(employee);
		var crtime = widget.column({
			text : '时间',
			dataIndex : 'dateTime'
		});
		columns.push(crtime);
		return columns;
	},
	createstore : function() {
		return widget.ds({
			clazz : 'app.m.m3.report.model',
			url : reporturl + '/b/report/full'
		});
	},
	doRefresh : function() {
		var me = this;
		me.store.reload();
	},
	setStore : function(rec) {
		var me = this;
		var m = me.qf;
		m.getForm().findField('storeId').setValue(rec.get('storeId'));
		m.getForm().findField('storeName').setValue(rec.get('cname'));
	},
	setDevice : function(rec) {
		var me = this;
		var m = me.qf;
		m.getForm().findField('sn').setValue(rec.get('mac'));
	}
});