Ext.define("app.m.m3.index.product.form", {
	extend : 'Ext.form.Panel',
	border : false,
	layout : 'column',
	region : 'center',
	requires : [ 'app.m.m3.index.product.model' ],
	initComponent : function() {
		var me = this;
		var fields = me.createfield();
		Ext.apply(me, {
			items : fields,
			listeners : {
				afterrender : function() {
					if (me.productId) {
						utils.loadform({
							form : me,
							model : 'app.m.m3.index.product.model',
							url : baseurl + '/b/index/product/find',
							extraParams : {
								'productId' : me.productId
							},
							fn : function(rec) {
								me.getForm().loadRecord(rec);
							}
						});
					}
				}
			}
		});
		me.callParent(arguments);
	},
	createfield : function() {
		var me = this;
		var array = new Array();
		utils.pushfieldarray(array, '1', {
			name : 'relationId',
			columnWidth : .33,
			afterLabelTextTpl : required,
			fieldLabel : '商品ID',
			allowBlank : false,
			minLength : 1,
			maxLength : 16
		});
		utils.pushfieldarray(array, '1', {
			name : 'productName',
			afterLabelTextTpl : required,
			columnWidth : .33,
			fieldLabel : '商品名称',
			allowBlank : false
		});
		utils.pushfieldarray(array, '3', {
			name : 'price',
			afterLabelTextTpl : required,
			columnWidth : .33,
			fieldLabel : '商品价格',
			allowDecimals: true,
			decimalPrecision: 2,
			maxValue: 10000,
	        minValue: 0,
			allowBlank : false
		});
		utils.pushfieldarray(array, '6', {
			name : 'describe',
			afterLabelTextTpl : required,
			columnWidth : 1,
			fieldLabel : '商品信息描述'
		});
		utils.pushfieldarray(array, '1', {
			name : 'mild',
			afterLabelTextTpl : required,
			columnWidth : 1,
			fieldLabel : '轻度'
		});
		utils.pushfieldarray(array, '1', {
			name : 'moderate',
			afterLabelTextTpl : required,
			columnWidth : 1,
			fieldLabel : '中度'
		});
		utils.pushfieldarray(array, '1', {
			name : 'severe',
			afterLabelTextTpl : required,
			columnWidth : 1,
			fieldLabel : '重度'
		});
		utils.pushfieldarray(array, '1', {
			name : 'imgUrl',
			afterLabelTextTpl : required,
			columnWidth : 1,
			fieldLabel : '图片url'
		});
		utils.pushfieldarray(array, '1', {
			name : 'url',
			afterLabelTextTpl : required,
			columnWidth : 1,
			fieldLabel : '商品url'
		});
		return array;
	}
});