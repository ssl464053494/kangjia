Ext.define('app.m.m3.index.product.grid', {
	extend : "Ext.grid.Panel",
	requires : [ 'app.m.m3.index.product.model', 'app.m.m3.index.product.win' ],
	frame : false,
	border : false,
	btn : true,
	minWidth : 2048,
	columnLines : true,
	initComponent : function() {
		var me = this;
		var columns = me.createcolumns();
		var store = me.createstore();
		var dockitems = new Array();
		var toolarray = me.createtool();
		if (toolarray.length > 0) {
			dockitems.push(widget.toolbar(toolarray));
		}
		dockitems.push(widget.paging(store));
		Ext.apply(me, {
			viewConfig : {
				forceFit : true
			},
			store : store,
			columns : columns,
			singleExpand : me.singleExpand || true,
			dockedItems : dockitems
		});
		store.load();
		me.callParent(arguments);
	},
	doAdd : function() {
		var me = this;
		Ext.create('app.m.m3.index.product.win', {
			indexTypeId : me.indexTypeId,
			_target_ : me
		}).show();
	},
	doUpdate : function() {
		var me = this;
		var rec = utils.single(me);
		if (rec != null) {
			Ext.create('app.m.m3.index.product.win', {
				indexTypeId : me.indexTypeId,
				productId : rec.get('productId'),
				_target_ : me
			}).show();
		} else {
			Ext.Msg.alert('提示', '请选择记录');
		}
	},
	doDelete : function() {
		var me = this;
		var rec = utils.single(me);
		if (rec != null) {
			var param = {
				productId : rec.get('productId'),
				indexTypeId : me.indexTypeId
			};
			evt.confirm(me, '是否删除?', baseurl + '/b/index/product/delete', param, function() {
				me.store.reload();
			});
		} else {
			Ext.Msg.alert('提示', '请选择记录');
		}
	},
	createtool : function() {
		var me = this;
		var array = new Array();
		if (me.orgId) {
			return array;
		}
		if (!me.btn) {
			return array;
		}
		utils.pushbtnarray(array, {
			text : '新增',
			btnid : 49,
			handler : function() {
				me.doAdd(me);
			}
		});
		utils.pushbtnarray(array, {
			text : '修改',
			btnid : 49,
			handler : function() {
				me.doUpdate(me);
			}
		});
		utils.pushbtnarray(array, {
			text : '删除',
			btnid : 51,
			handler : function() {
				me.doDelete(me);
			}
		});
		return array;
	},
	createcolumns : function() {
		var columns = new Array();
		columns.push(Ext.create('Ext.grid.RowNumberer'));
		var productCode = widget.column({
			text : '编号',
			dataIndex : 'productCode'
		});
		columns.push(productCode);
		var productName = widget.column({
			text : '名称',
			dataIndex : 'productName'
		});
		columns.push(productName);
		var price = widget.column({
			text : '商品价格',
			dataIndex : 'price'
		});
		columns.push(price);
		var describe = widget.column({
			text : '描述',
			dataIndex : 'describe'
		});
		columns.push(describe);
		var mild = widget.column({
			text : '轻度',
			dataIndex : 'mild'
		});
		columns.push(mild);
		var moderate = widget.column({
			text : '中度',
			dataIndex : 'moderate'
		});
		columns.push(moderate);
		var severe = widget.column({
			text : '重度',
			dataIndex : 'severe'
		});
		columns.push(severe);
		var imgUrl = widget.column({
			text : '图片url',
			dataIndex : 'imgUrl'
		});
		columns.push(imgUrl);
		var url = widget.column({
			text : '商品url',
			dataIndex : 'url'
		});
		columns.push(url);
		return columns;
	},
	createstore : function() {
		var me = this;
		return widget.ds({
			clazz : 'app.m.m3.index.product.model',
			url : baseurl + '/b/index/product/search',
			extraParams : {
				indexTypeId : me.indexTypeId
			}
		});
	},
	doRefresh : function() {
		var me = this;
		me.store.reload();
	}
});