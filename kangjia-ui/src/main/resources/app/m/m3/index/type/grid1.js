Ext.define('app.m.m3.index.type.grid1', {
	extend : "Ext.grid.Panel",
	requires : [ 'app.m.m3.index.type.model', 'app.m.m3.index.type.win' ],
	frame : false,
	border : false,
	btn : true,
	minWidth : 2048,
	columnLines : true,
	region : 'center',
	initComponent : function() {
		var me = this;
		var columns = me.createcolumns();
		var store = me.createstore();
		var dockitems = new Array();
		dockitems.push(widget.paging(store));
		Ext.apply(me, {
			viewConfig : {
				forceFit : true
			},
			store : store,
			columns : columns,
			singleExpand : false,
			selModel: {
	            type: 'checkboxmodel',
	            checkOnly: true
	        },
			dockedItems : dockitems
		});
		store.load();
		me.callParent(arguments);
	},
	createcolumns : function() {
		var columns = new Array();
		columns.push(Ext.create('Ext.grid.RowNumberer'));
		var indexType = widget.column({
			text : '类型',
			flex : 0.05,
			dataIndex : 'indexType',
			renderer : function(v) {
				if (v == 1) {
					return '膳食建议';
				} else if (v == 2) {
					return '营养建议';
				} else if (v == 3) {
					return '运动和生活方式建议';
				}
			}
		});
		columns.push(indexType);
		var indexCode = widget.column({
			text : '编号',
			flex : 0.05,
			dataIndex : 'indexCode'
		});
		columns.push(indexCode);
		var index = widget.column({
			text : '指标名称',
			flex : 0.1,
			dataIndex : 'index'
		});
		columns.push(index);
		var personality = widget.column({
			text : '建议',
			flex : 0.3,
			dataIndex : 'proposal'
		});
		columns.push(personality);
		var personality = widget.column({
			text : '个性化建议',
			flex : 0.3,
			dataIndex : 'personality'
		});
		columns.push(personality);
		var describe = widget.column({
			text : '简单描述',
			flex : 0.3,
			dataIndex : 'describe'
		});
		columns.push(describe);
		return columns;
	},
	createstore : function() {
		var me = this;
		return widget.ds({
			clazz : 'app.m.m3.index.type.model',
			url : baseurl + '/b/index/type/search',
			extraParams : me.extraParams
		});
	},
	doRefresh : function() {
		var me = this;
		me.store.reload();
	},
	getIndexTypeRecord : function() {
		var me = this;
		return utils.multiple(me);
	}
});