Ext.define("app.m.m3.index.type.widget", {
	extend : 'Ext.window.Window',
	title : '关联指标',
	height : 600,
	width : 800,
	closable : true,
	resizable : false,
	modal : true,
	constrain : true,
	plain : true,
	bodyStyle :'overflow-x:scroll;overflow-y:hidden;',
	requires : [ 'app.m.m3.index.type.grid1' ],
	initComponent : function() {
		var me = this;
		var targetId = me.targetId;
		var indexType = me.classfiy;
		var inspectStandard = me.inspectStandard
		var panel = Ext.create('app.m.m3.index.type.grid1', {
			region : 'center',
			extraParams : {
				indexType : indexType
			}
		});
		Ext.apply(me, {
			layout : {
				type : 'border'
			},
			viewConfig : {
				forceFit : true
			},
			items : [ panel ],
			buttons : [ {
				text : '确认',
				bodyStyle : 'padding:0 0 0 5px',
				handler : function() {
					var recs = panel.getIndexTypeRecord();
					if (recs != null) {
						var arrays = new Array();
						for (var i = 0; i < recs.length; i++) {
							var relation = {};
							var rec = recs[i];
							relation['targetId'] = targetId;
							relation['classify'] = indexType;
							relation['inspectStandard'] = inspectStandard;
							relation['indexTypeId'] = rec.get('indexTypeId');
							arrays.push(relation);
						}
						evt.post(me, baseurl + '/b/relation/editor', {
							list : arrays
						}, function() {
							me._target_.doRefresh();
							me.close();
						});
					} else {
						Ext.Msg.alert('提示', '选择记录');
						return;
					}
				}
			}, {
				text : '关闭',
				bodyStyle : 'padding:0 0 0 5px',
				handler : function() {
					me.close();
				}
			} ]
		});
		me.callParent(arguments);
	}
});