Ext.define('app.m.m3.index.product.model', {
	extend : 'Ext.data.Model',
	fields : [ {
		name : 'productId',
		type : 'int'
	}, {
		name : 'indexTypeId',
		type : 'int'
	}, {
		name : 'productCode',
		type : 'string'
	}, {
		name : 'relationId',
		type : 'int'
	}, {
		name : 'productName',
		type : 'string'
	}, {
		name : 'describe',
		type : 'string'
	}, {
		name : 'mild',
		type : 'string'
	},  {
		name : 'moderate',
		type : 'string'
	}, {
		name : 'severe',
		type : 'string'
	}, {
		name : 'imgUrl',
		type : 'string'
	}, {
		name : 'url',
		type : 'string'
	}, {
		name : 'price',
		type : 'float'
	} ]
});