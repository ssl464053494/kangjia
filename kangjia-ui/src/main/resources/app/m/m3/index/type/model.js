Ext.define('app.m.m3.index.type.model', {
	extend : 'Ext.data.Model',
	fields : [ {
		name : 'indexTypeId',
		type : 'int'
	}, {
		name : 'indexType',
		type : 'int'
	}, {
		name : 'relationId',
		type : 'int'
	}, {
		name : 'indexCode',
		type : 'string'
	}, {
		name : 'index',
		type : 'string'
	}, {
		name : 'describe',
		type : 'string'
	}, {
		name : 'proposal',
		type : 'string'
	},  {
		name : 'personality',
		type : 'string'
	}, {
		name : 'expression',
		type : 'string'
	}, {
		name : 'status',
		type : 'int'
	} ]
});