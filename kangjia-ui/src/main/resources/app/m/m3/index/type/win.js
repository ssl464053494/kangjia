Ext.define("app.m.m3.index.type.win", {
	extend : 'Ext.window.Window',
	title : '编辑建议指标',
	height : 600,
	width : 800,
	closable : true,
	resizable : false,
	modal : true,
	constrain : true,
	plain : true,
	requires : [ 'app.m.m3.index.type.form' ],
	initComponent : function() {
		var me = this;
		var panel = Ext.create('app.m.m3.index.type.form', {
			indexTypeId : me.indexTypeId
		});
		Ext.apply(me, {
			layout : {
				type : 'border'
			},
			viewConfig : {
				forceFit : true
			},
			items : [ panel ],
			buttons : [ {
				text : '确认',
				bodyStyle : 'padding:0 0 0 5px',
				handler : function() {
					var param = {
						indexTypeId : me.indexTypeId
					};
					var url = baseurl + '/b/index/type/editor';
					evt.submit(panel, url, param, function(rec) {
						me.close();
						me._target_.doRefresh();
					});
				}
			}, {
				text : '关闭',
				bodyStyle : 'padding:0 0 0 5px',
				handler : function() {
					me.close();
				}
			} ]
		});
		me.callParent(arguments);
	}
});