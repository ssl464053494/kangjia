Ext.define('app.m.m3.index.type.grid', {
	extend : "Ext.grid.Panel",
	requires : [ 'app.m.m3.index.type.model', 'app.m.m3.index.type.win', 'app.m.m3.index.type.widget', 'app.m.m3.index.product.grid' ],
	frame : false,
	border : false,
	btn : true,
	minWidth : 2048,
	columnLines : true,
	initComponent : function() {
		var me = this;
		var columns = me.createcolumns();
		var store = me.createstore();
		var dockitems = new Array();
		if (me.btn) {
			var queryarry = me.createquery(store);
			if (queryarry != null) {
				me.qf = widget.search(queryarry);
				dockitems.push(me.qf);
			}
		}
		var toolarray = me.createtool();
		if (toolarray.length > 0) {
			dockitems.push(widget.toolbar(toolarray));
		}
		if (me.btn) {
			dockitems.push(widget.paging(store));
		}
		Ext.apply(me, {
			viewConfig : {
				forceFit : true
			},
			store : store,
			columns : columns,
			singleExpand : me.singleExpand || true,
			dockedItems : dockitems
		});
		store.load();
		me.callParent(arguments);
	},
	createquery : function(store) {
		var me = this;
		var param = {};
		var array = new Array();
		var indexTypeStore = Ext.create('Ext.data.Store', {
			fields : [ 'value', 'name' ],
			data : [ {
				"value" : 1,
				"name" : "膳食建议"
			}, {
				"value" : 2,
				"name" : "营养建议"
			}, {
				"value" : 3,
				"name" : "运动生活方式建议"
			} ]
		});
		array.push({
			xtype : 'combo',
			name : 'indexType',
			labelWidth : 60,
			style : 'margin: 5px 5px 0 5px',
			fieldLabel : '建议类型:',
			labelAlign : 'right',
			store : indexTypeStore,
			queryMode : 'local',
			displayField : 'name',
			valueField : 'value',
			labelSeparator : ''
		});
		param['bl'] = 'center';
		param['items'] = array;
		param['ds'] = store;
		var array = new Array();
		param['btn'] = array;
		return param;
	},
	doAdd : function() {
		var me = this;
		Ext.create('app.m.m3.index.type.win', {
			_target_ : me
		}).show();
	},
	doUpdate : function() {
		var me = this;
		var rec = utils.single(me);
		if (rec != null) {
			Ext.create('app.m.m3.index.type.win', {
				indexTypeId : rec.get('indexTypeId'),
				_target_ : me
			}).show();
		} else {
			Ext.Msg.alert('提示', '请选择记录');
		}
	},
	doDelete : function() {
		var me = this;
		var rec = utils.single(me);
		if (rec != null) {
			var param = {
				indexTypeId : rec.get('indexTypeId')
			};
			evt.confirm(me, '是否删除?', baseurl + '/b/index/type/delete', param,
					function() {
						me.store.reload();
					});
		} else {
			Ext.Msg.alert('提示', '请选择记录');
		}
	},
	doRelationProduct : function() {
		var me = this;
		var rec = utils.single(me);
		if (rec != null) {
			var indexTypeId = rec.get('indexTypeId');
			me._target_.addGrid('product_' + indexTypeId,
					'app.m.m3.index.product.grid', rec.get('index'), {
						indexTypeId : indexTypeId
					});
		} else {
			Ext.Msg.alert('提示', '请选择记录');
		}
	},
	doAdd1 : function() {
		var me = this;
		Ext.create('app.m.m3.index.type.widget', {
			_target_ : me,
			targetId : me.targetId,
			classfiy : me.classfiy,
			inspectStandard : me.inspectStandard
		}).show();
	},
	doDelete1 : function() {
		var me = this;
		var rec = utils.single(me);
		if (rec != null) {
			var param = {
				relationId : rec.get('relationId')
			};
			evt.confirm(me, '是否删除?', baseurl + '/b/relation/delete', param,
					function() {
						me.store.reload();
					});
		} else {
			Ext.Msg.alert('提示', '请选择记录');
		}
	},
	createtool : function() {
		var me = this;
		var array = new Array();
		if (!me.btn) {
			utils.pushbtnarray(array, {
				text : '新增',
				handler : function() {
					me.doAdd1(me);
				}
			});
			utils.pushbtnarray(array, {
				text : '删除',
				handler : function() {
					me.doDelete1(me);
				}
			});
			return array;
		}
		utils.pushbtnarray(array, {
			text : '新增',
			handler : function() {
				me.doAdd(me);
			}
		});
		utils.pushbtnarray(array, {
			text : '修改',
			handler : function() {
				me.doUpdate(me);
			}
		});
		utils.pushbtnarray(array, {
			text : '删除',
			handler : function() {
				me.doDelete(me);
			}
		});
		utils.pushbtnarray(array, {
			text : '关联商品',
			handler : function() {
				me.doRelationProduct(me);
			}
		});
		return array;
	},
	createcolumns : function() {
		var columns = new Array();
		columns.push(Ext.create('Ext.grid.RowNumberer'));
		var indexType = widget.column({
			text : '类型',
			flex : 0.05,
			dataIndex : 'indexType',
			renderer : function(v) {
				if (v == 1) {
					return '膳食建议';
				} else if (v == 2) {
					return '营养建议';
				} else if (v == 3) {
					return '运动和生活方式建议';
				}
			}
		});
		columns.push(indexType);
		var indexCode = widget.column({
			text : '编号',
			flex : 0.05,
			dataIndex : 'indexCode'
		});
		columns.push(indexCode);
		var index = widget.column({
			text : '指标名称',
			flex : 0.1,
			dataIndex : 'index'
		});
		columns.push(index);
		var personality = widget.column({
			text : '建议',
			flex : 0.3,
			dataIndex : 'proposal'
		});
		columns.push(personality);
		var personality = widget.column({
			text : '个性化建议',
			flex : 0.3,
			dataIndex : 'personality'
		});
		columns.push(personality);
		var describe = widget.column({
			text : '简单描述',
			flex : 0.3,
			dataIndex : 'describe'
		});
		columns.push(describe);
		return columns;
	},
	createstore : function() {
		var me = this;
		var url = baseurl + '/b/index/type/search';
		var extraParams = {};
		if (!me.btn) {
			url = me.url;
			extraParams = {
				targetId : me.targetId,
				classify : me.classfiy,
				inspectStandard : me.inspectStandard
			};
		}
		return widget.ds({
			clazz : 'app.m.m3.index.type.model',
			url : url,
			extraParams : extraParams
		});
	},
	doRefresh : function() {
		var me = this;
		me.store.reload();
	},
	getIndexTypeRecord : function() {
		var me = this;
		return utils.multiple(me);
	}
});