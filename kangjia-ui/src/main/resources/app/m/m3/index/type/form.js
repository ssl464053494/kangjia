Ext.define("app.m.m3.index.type.form", {
	extend : 'Ext.form.Panel',
	border : false,
	layout : 'column',
	region : 'center',
	requires : [ 'app.m.m3.index.type.model' ],
	initComponent : function() {
		var me = this;
		var fields = me.createfield();
		Ext.apply(me, {
			items : fields,
			listeners : {
				afterrender : function() {
					if (me.indexTypeId) {
						utils.loadform({
							form : me,
							model : 'app.m.m3.index.type.model',
							url : baseurl + '/b/index/type/find',
							extraParams : {
								'indexTypeId' : me.indexTypeId
							},
							fn : function(rec) {
								me.getForm().loadRecord(rec);
								me.getForm().findField('indexType').setReadOnly(true);
							}
						});
					}
				}
			}
		});
		me.callParent(arguments);
	},
	createfield : function() {
		var me = this;
		var array = new Array();
		var indexTypeStore = Ext.create('Ext.data.Store', {
			fields : [ 'value', 'name' ],
			data : [ {
				"value" : 1,
				"name" : "膳食建议"
			}, {
				"value" : 2,
				"name" : "营养建议"
			}, {
				"value" : 3,
				"name" : "运动生活方式建议"
			} ]
		});
		utils.pushfieldarray(array, 'Ext.form.ComboBox', {
			name : 'indexType',
			afterLabelTextTpl : required,
			columnWidth : .5,
			store : indexTypeStore,
			fieldLabel : '建议类型',
			queryMode : 'local',
			allowBlank : false,
			displayField : 'name',
			valueField : 'value'
		});
		utils.pushfieldarray(array, '1', {
			name : 'index',
			columnWidth : .5,
			afterLabelTextTpl : required,
			fieldLabel : '名称'
		});
		utils.pushfieldarray(array, '6', {
			name : 'proposal',
			afterLabelTextTpl : required,
			columnWidth : 1,
			fieldLabel : '建议'
		});
		utils.pushfieldarray(array, '6', {
			name : 'personality',
			afterLabelTextTpl : required,
			columnWidth : 1,
			fieldLabel : '个性化建议'
		});
		utils.pushfieldarray(array, '6', {
			name : 'expression',
			afterLabelTextTpl : required,
			columnWidth : 1,
			fieldLabel : '表达式'
		});
		utils.pushfieldarray(array, '6', {
			name : 'describe',
			afterLabelTextTpl : required,
			columnWidth : 1,
			fieldLabel : '简单描述'
		});
		return array;
	}
});