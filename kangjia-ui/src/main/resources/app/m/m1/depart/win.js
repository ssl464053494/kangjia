Ext.define("app.m.m1.depart.win", {
	extend : 'Ext.window.Window',
	title : '编辑部门',
	height : 400,
	width : 400,
	closable : true,
	resizable : false,
	modal : true,
	constrain : true,
	plain : true,
	requires : [ 'app.m.m1.depart.form' ],
	initComponent : function() {
		var me = this;
		var panel = Ext.create('app.m.m1.depart.form', {
			deptId : me.deptId,
			parentId : me.parentId
		});
		Ext.apply(me, {
			layout : {
				type : 'border'
			},
			items : [ panel ],
			buttons : [ {
				text : '确认',
				bodyStyle : 'padding:0 0 0 5px',
				handler : function() {
					var param = {
						deptId : me.deptId,
						parentId : me.parentId
					};
					var url = baseurl + '/b/depart/editor';
					evt.submit(panel, url, param, function(rec) {
						me.close();
						me._target_.doRefresh();
					});
				}
			}, {
				text : '关闭',
				bodyStyle : 'padding:0 0 0 5px',
				handler : function() {
					me.close();
				}
			} ]
		});
		me.callParent(arguments);
	}
});