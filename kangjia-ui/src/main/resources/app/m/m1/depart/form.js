Ext.define("app.m.m1.depart.form", {
	extend : 'Ext.form.Panel',
	border : false,
	layout : 'column',
	region : 'center',
	requires : [ 'app.m.m1.depart.model' ],
	initComponent : function() {
		var me = this;
		var fields = me.createfield();
		Ext.apply(me, {
			items : fields,
			listeners : {
				afterrender : function() {
					if (me.deptId) {
						utils.loadform({
							form : me,
							model : 'app.m.m1.depart.model',
							url : baseurl + '/b/depart/find',
							extraParams : {
								'deptId' : me.deptId
							},
							fn : function(rec) {
								me.getForm().loadRecord(rec);
							}
						});
					}
				}
			}
		});
		me.callParent(arguments);
	},
	createfield : function() {
		var me = this;
		var array = new Array();
		utils.pushfieldarray(array, '1', {
			name : 'cname',
			afterLabelTextTpl : required,
			fieldLabel : '名称',
			allowBlank : false,
			minLength : 1,
			maxLength : 16
		});
		return array;
	}
});