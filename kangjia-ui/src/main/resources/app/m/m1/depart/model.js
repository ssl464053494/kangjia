Ext.define('app.m.m1.depart.model', {
	extend : 'Ext.data.TreeModel',
	fields : [ {
		name : 'deptId',
		type : 'int'
	}, {
		name : 'parentId',
		type : 'int'
	}, {
		name : 'cname',
		type : 'string'
	}, {
		name : 'code',
		type : 'string'
	} , {
		name : 'orgId',
		type : 'int'
	}  ]
});