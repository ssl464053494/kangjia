Ext.define('app.m.m1.depart.grid', {
	extend : 'Ext.tree.Panel',
	requires : [ 'app.m.m1.depart.model', 'app.m.m1.depart.win' ],
	region : 'center',
	reserveScrollbar : true,
	useArrows : true,
	rootVisible : false,
	multiSelect : true,
	singleExpand : true,
	frame : false,
	border : false,
	initComponent : function() {
		var me = this;
		var columns = me.createcolumns();
		var store = me.createstore();
		var dockitems = new Array();
		var toolarray = me.createtool();
		if (toolarray.length > 0) {
			dockitems.push(widget.toolbar(toolarray));
		}
		Ext.apply(me, {
			store : store,
			columns : columns,
			singleExpand : me.singleExpand || true,
			plugins: [{
		        ptype: 'rowediting',
		        clicksToMoveEditor: 1,
		        autoCancel: false
		    }],
			dockedItems : dockitems
		});
		me.callParent(arguments);
	},
	createcolumns : function() {
		var columns = new Array();
		columns.push(Ext.create('Ext.grid.RowNumberer'));
		var code = widget.treecolumn({
			text : '编码',
			dataIndex : 'code'
		});
		columns.push(code);
		var cname = widget.treecolumn({
			text : '名称',
			dataIndex : 'cname'
		});
		columns.push(cname);
		return columns;
	},
	createstore : function() {
		var me = this;
		return widget.ts({
			clazz : 'app.m.m1.depart.model',
			url : baseurl + '/b/depart/search'
		});
	},
	createtool : function() {
		var me = this;
		var array = new Array();
		utils.pushbtnarray(array, {
			text : '新增',
			handler : function() {
				me.doAdd();
			}
		});
		utils.pushbtnarray(array, {
			text : '修改',
			handler : function() {
				me.doUpdate();
			}
		});
		utils.pushbtnarray(array, {
			text : '删除',
			handler : function() {
				me.doDelete();
			}
		});
		return array;
	},
	doAdd : function() {
		var me = this;
		var rec = utils.single(me);
		var parentId = null;
		if (rec != null) {
			parentId = rec.get('deptId');
		}
		Ext.create('app.m.m1.depart.win', {
			parentId : parentId,
			_target_ : me
		}).show()
	},
	doUpdate : function() {
		var me = this;
		var rec = utils.single(me);
		if (rec != null) {
			Ext.create('app.m.m1.depart.win', {
				deptId : rec.get('deptId'),
				_target_ : me
			}).show();
		} else {
			Ext.Msg.alert('提示', '请选择记录');
		}
	},
	doDelete : function() {
		var me = this;
		var rec = utils.single(me);
		if (rec != null) {
			var param = {
				deptId : rec.get('deptId')
			};
			evt.confirm(me, '是否删除?', baseurl + '/b/depart/delete', param, function() {
				me.store.reload();
			});
		} else {
			Ext.Msg.alert('提示', '请选择记录');
		}
	},
	doRefresh : function() {
		var me = this;
		me.store.reload();
	}
});