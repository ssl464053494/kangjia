Ext.define("app.m.m1.employee.win", {
	extend : 'Ext.window.Window',
	title : '编辑员工',
	height : 400,
	width : 600,
	closable : true,
	resizable : false,
	modal : true,
	constrain : true,
	plain : true,
	requires : [ 'app.m.m1.employee.form' ],
	initComponent : function() {
		var me = this;
		var panel = Ext.create('app.m.m1.employee.form', {
			employeeId : me.employeeId
		});
		Ext.apply(me, {
			layout : {
				type : 'border'
			},
			items : [ panel ],
			buttons : [ {
				text : '确认',
				bodyStyle : 'padding:0 0 0 5px',
				handler : function() {
					var param = {
						employeeId : me.employeeId
					};
					var url = baseurl + '/b/employee/editor';
					evt.submit(panel, url, param, function(rec) {
						if (!me.employeeId) {
							Ext.Msg.alert('提示', rec.msg);
						}
						me.close();
						me._target_.doRefresh();
					});
				}
			}, {
				text : '关闭',
				bodyStyle : 'padding:0 0 0 5px',
				handler : function() {
					me.close();
				}
			} ]
		});
		me.callParent(arguments);
	}
});