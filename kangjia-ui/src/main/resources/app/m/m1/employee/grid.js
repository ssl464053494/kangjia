Ext.define('app.m.m1.employee.grid', {
	extend : "Ext.grid.Panel",
	requires : [ 'app.m.m1.employee.model', 'app.m.m1.employee.win',
			'app.m.m1.employee.pass.win', 'app.m.m1.org.field',
			'app.m.m1.store.field', 'app.m.m0.role.field' ],
	frame : false,
	border : false,
	initComponent : function() {
		var me = this;
		var columns = me.createcolumns();
		var store = me.createstore();
		var dockitems = new Array();
		var toolarray = me.createtool();
		var queryarry = me.createquery(store);
		if (queryarry != null) {
			me.qf = widget.search(queryarry);
			dockitems.push(me.qf);
		}
		if (toolarray.length > 0) {
			dockitems.push(widget.toolbar(toolarray));
		}
		dockitems.push(widget.paging(store));
		Ext.apply(me, {
			store : store,
			columns : columns,
			singleExpand : me.singleExpand || true,
			dockedItems : dockitems
		});
		store.load();
		me.callParent(arguments);
	},
	createquery : function(store) {
		var me = this;
		var param = {};
		var array = new Array();
		array.push({
			xtype : 'textfield',
			name : 'employeeName',
			labelWidth : 60,
			style : 'margin: 5px 5px 0 5px',
			fieldLabel : '姓名:',
			labelAlign : 'right',
			labelSeparator : ''
		});
		array.push({
			xtype : 'rolefield',
			name : 'roleId',
			labelWidth : 60,
			style : 'margin: 5px 5px 0 5px',
			fieldLabel : '角色:',
			labelAlign : 'right',
			labelSeparator : ''
		});
		array.push({
			xtype : 'hidden',
			name : 'storeId'
		});
		array.push({
			xtype : 'storefield',
			name : 'storeName',
			labelWidth : 60,
			style : 'margin: 5px 5px 0 5px',
			fieldLabel : '门店:',
			labelAlign : 'right',
			btn : false,
			_target_ : me,
			labelSeparator : ''
		});
		var states = Ext.create('Ext.data.Store', {
			fields : [ 'value', 'key' ],
			data : [ {
				"value" : 0,
				"key" : "启用"
			}, {
				"value" : 1,
				"key" : "禁用"
			} ]
		});
		array.push({
			xtype : 'combo',
			name : 'flag',
			labelWidth : 60,
			style : 'margin: 5px 5px 0 5px',
			fieldLabel : '状态:',
			queryMode : 'local',
			store : states,
			displayField : 'key',
			valueField : 'value',
			labelAlign : 'right',
			labelSeparator : ''
		});
		param['bl'] = 'center';
		param['items'] = array;
		param['ds'] = store;
		var array = new Array();
		param['btn'] = array;
		return param;
	},
	doAdd : function() {
		var me = this;
		Ext.create('app.m.m1.employee.win', {
			_target_ : me
		}).show();
	},
	doUpdate : function() {
		var me = this;
		var rec = utils.single(me);
		if (rec != null) {
			Ext.create('app.m.m1.employee.win', {
				employeeId : rec.get('employeeId'),
				_target_ : me
			}).show();
		} else {
			Ext.Msg.alert('提示', '请选择记录');
		}
	},
	doDelete : function() {
		var me = this;
		var rec = utils.single(me);
		if (rec != null) {
			var param = {
				employeeId : rec.get('employeeId')
			};
			evt.confirm(me, '是否删除?', baseurl + '/b/employee/delete', param,
					function() {
						me.store.reload();
					});
		} else {
			Ext.Msg.alert('提示', '请选择记录');
		}
	},
	doStartAndStop : function() {
		var me = this;
		var rec = utils.single(me);
		if (rec != null) {
			var param = {
				employeeId : rec.get('employeeId')
			};
			evt.confirm(me, '是否执行操作?', baseurl + '/b/employee/startAndStop',
					param, function() {
						me.store.reload();
					});
		} else {
			Ext.Msg.alert('提示', '请选择记录');
		}
	},
	doRepeat : function() {
		var me = this;
		var rec = utils.single(me);
		if (rec != null) {
			var param = {
				employeeId : rec.get('employeeId')
			};
			evt.confirm(me, '是否执行操作?', baseurl + '/b/employee/repeat', param,
					function(res) {
						Ext.Msg.alert('提示', res.msg);
						me.store.reload();
					});
		} else {
			Ext.Msg.alert('提示', '请选择记录');
		}
	},
	doPass : function() {
		var me = this;
		var rec = utils.single(me);
		if (rec != null) {
			Ext.create('app.m.m1.employee.pass.win', {
				employeeId : rec.get('employeeId'),
				_target_ : me
			}).show();
		} else {
			Ext.Msg.alert('提示', '请选择记录');
		}
	},
	createtool : function() {
		var me = this;
		var array = new Array();
		utils.pushbtnarray(array, {
			text : '新增',
			handler : function() {
				me.doAdd(me);
			}
		});
		utils.pushbtnarray(array, {
			text : '修改',
			handler : function() {
				me.doUpdate(me);
			}
		});
		utils.pushbtnarray(array, {
			text : '删除',
			handler : function() {
				me.doDelete(me);
			}
		});
		utils.pushbtnarray(array, {
			text : '修改密码',
			handler : function() {
				me.doPass(me);
			}
		});
		utils.pushbtnarray(array, {
			text : '启用/禁用',
			handler : function() {
				me.doStartAndStop(me);
			}
		});
		utils.pushbtnarray(array, {
			text : '重置密码',
			handler : function() {
				me.doRepeat(me);
			}
		});
		return array;
	},
	createcolumns : function() {
		var columns = new Array();
		columns.push(Ext.create('Ext.grid.RowNumberer'));
		var employeeName = widget.column({
			text : '名称',
			dataIndex : 'employeeName'
		});
		columns.push(employeeName);
		var phone = widget.column({
			text : '电话',
			dataIndex : 'phone'
		});
		columns.push(phone);
		var login = widget.column({
			text : '账号',
			dataIndex : 'login'
		});
		columns.push(login);
		var org = widget.column({
			text : '所在机构',
			dataIndex : 'orgName'
		});
		columns.push(org);
		var store = widget.column({
			text : '所在门店',
			dataIndex : 'storeName'
		});
		columns.push(store);
		var role = widget.column({
			text : '角色',
			dataIndex : 'roleName'
		});
		columns.push(role);
		var flag = widget.column({
			text : '状态',
			dataIndex : 'flag',
			renderer : function(v) {
				return v == 0 ? '启用' : '禁用';
			}
		});
		columns.push(flag);
		return columns;
	},
	createstore : function() {
		return widget.ds({
			clazz : 'app.m.m1.employee.model',
			url : baseurl + '/b/employee/search'
		});
	},
	doRefresh : function() {
		var me = this;
		me.store.reload();
	},
	setStore : function(rec) {
		var me = this;
		var m = me.qf;
		m.getForm().findField('storeId').setValue(rec.get('storeId'));
		m.getForm().findField('storeName').setValue(rec.get('cname'));
	}
});