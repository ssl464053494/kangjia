Ext.define('app.m.m1.employee.model', {
	extend : 'Ext.data.Model',
	fields : [ {
		name : 'employeeId',
		type : 'int'
	}, {
		name : 'login',
		type : 'string'
	}, {
		name : 'pass',
		type : 'string'
	}, {
		name : 'employeeName',
		type : 'string'
	}, {
		name : 'phone',
		type : 'string'
	}, {
		name : 'orgId',
		type : 'int'
	}, {
		name : 'storeId',
		type : 'int'
	}, {
		name : 'roleId',
		type : 'int'
	}, {
		name : 'busId',
		type : 'int'
	}, {
		name : 'deptId',
		type : 'int'
	}, {
		name : 'storeName',
		type : 'string'
	}, {
		name : 'orgName',
		type : 'string'
	}, {
		name : 'roleName',
		type : 'string'
	}, {
		name : 'flag',
		type : 'int'
	} ]
});