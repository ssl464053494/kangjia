Ext.define("app.m.m1.employee.pass.win", {
	extend : 'Ext.window.Window',
	title : '修改密码',
	height : 400,
	width : 600,
	closable : true,
	resizable : false,
	modal : true,
	constrain : true,
	plain : true,
	requires : [ 'app.m.m1.employee.pass.form' ],
	initComponent : function() {
		var me = this;
		var panel = Ext.create('app.m.m1.employee.pass.form', {
			employeeId : me.employeeId
		});
		Ext.apply(me, {
			layout : {
				type : 'border'
			},
			items : [ panel ],
			buttons : [ {
				text : '确认',
				bodyStyle : 'padding:0 0 0 5px',
				handler : function() {
					var param = {
						employeeId : me.employeeId,
						oper : me.oper
					};
					var url = baseurl + '/b/employee/pass';
					evt.submit(panel, url, param, function(rec) {
						me.close();
						me._target_.doRefresh();
					});
				}
			}, {
				text : '关闭',
				bodyStyle : 'padding:0 0 0 5px',
				handler : function() {
					me.close();
				}
			} ]
		});
		me.callParent(arguments);
	}
});