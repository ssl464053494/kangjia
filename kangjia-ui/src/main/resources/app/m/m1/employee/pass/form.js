Ext.define("app.m.m1.employee.pass.form", {
	extend : 'Ext.form.Panel',
	border : false,
	layout : 'column',
	region : 'center',
	requires : [ 'app.m.m1.employee.model' ],
	initComponent : function() {
		var me = this;
		var fields = me.createfield();
		Ext.apply(me, {
			items : fields,
			listeners : {
				
			}
		});
		me.callParent(arguments);
	},
	createfield : function() {
		var me = this;
		var array = new Array();
		utils.pushfieldarray(array, '2', {
			name : 'oldpass',
			afterLabelTextTpl : required,
			fieldLabel : '旧密码',
			columnWidth : 1,
			allowBlank : false,
			minLength : 6,
			maxLength : 8
		});
		utils.pushfieldarray(array, '2', {
			name : 'newpass',
			afterLabelTextTpl : required,
			fieldLabel : '新密码',
			columnWidth : 1,
			allowBlank : false,
			minLength : 6,
			maxLength : 8
		});
		utils.pushfieldarray(array, '2', {
			name : 'repeatpass',
			afterLabelTextTpl : required,
			fieldLabel : '重复密码',
			columnWidth : 1,
			allowBlank : false,
			minLength : 6,
			maxLength : 8
		});
		return array;
	}
});