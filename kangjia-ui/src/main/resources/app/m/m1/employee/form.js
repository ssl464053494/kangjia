Ext.define("app.m.m1.employee.form", {
	extend : 'Ext.form.Panel',
	border : false,
	layout : 'column',
	region : 'center',
	requires : [ 'app.m.m1.employee.model', 'app.m.m1.org.field',
			'app.m.m1.store.field', 'app.m.m0.role.field' ],
	initComponent : function() {
		var me = this;
		var fields = me.createfield();
		Ext.apply(me, {
			items : fields,
			listeners : {
				afterrender : function() {
					if (me.employeeId) {
						utils.loadform({
							form : me,
							model : 'app.m.m1.employee.model',
							url : baseurl + '/b/employee/find',
							extraParams : {
								'employeeId' : me.employeeId
							},
							fn : function(rec) {
								me.getForm().loadRecord(rec);
							}
						});
					}
				}
			}
		});
		me.callParent(arguments);
	},
	createfield : function() {
		var me = this;
		var array = new Array();
		utils.pushfieldarray(array, '1', {
			name : 'employeeName',
			afterLabelTextTpl : required,
			fieldLabel : '姓名',
			columnWidth : .5,
			allowBlank : false,
			minLength : 1,
			maxLength : 16
		});
		utils.pushfieldarray(array, '1', {
			name : 'phone',
			afterLabelTextTpl : required,
			fieldLabel : '电话',
			columnWidth : .5,
			allowBlank : false,
			minLength : 11,
			maxLength : 11
		});
		utils.pushfieldarray(array, '1', {
			name : 'login',
			afterLabelTextTpl : required,
			fieldLabel : '账号',
			columnWidth : .5,
			allowBlank : false,
			minLength : 5,
			maxLength : 12
		});
		utils.pushfieldarray(array, '4', {
			name : 'orgId'
		});
		utils.pushfieldarray(array, 'app.m.m1.org.field', {
			name : 'orgName',
			afterLabelTextTpl : required,
			fieldLabel : '所在机构',
			columnWidth : .5,
			url : baseurl + '/b/org/search',
			extraParams : {},
			_target_ : me,
			allowBlank : false
		});
		utils.pushfieldarray(array, '4', {
			name : 'storeId'
		});
		utils.pushfieldarray(array, 'app.m.m1.store.field', {
			name : 'storeName',
			fieldLabel : '所在门店',
			columnWidth : .5,
			_target_ : me,
			btn : false,
			listeners : {

			}
		});
		var states = Ext.create('Ext.data.Store', {
			fields : [ 'value', 'name' ],
			data : [ {
				"value" : 0,
				"name" : "店员"
			}, {
				"value" : 1,
				"name" : "店长"
			}, {
				"value" : 2,
				"name" : "普通职员"
			} ]
		});
		utils.pushfieldarray(array, 'Ext.form.ComboBox', {
			name : 'busId',
			afterLabelTextTpl : required,
			columnWidth : .5,
			store : states,
			fieldLabel : '职务',
			queryMode : 'local',
			allowBlank : false,
			displayField : 'name',
			valueField : 'value'
		});
		utils.pushfieldarray(array, 'app.m.m0.role.field', {
			name : 'roleId',
			afterLabelTextTpl : required,
			allowBlank : false,
			fieldLabel : '角色',
			columnWidth : .5
		});
		return array;
	},
	setOrg : function(rec) {
		var me = this;
		me.getForm().findField('orgId').setValue(rec.get('orgId'));
		me.getForm().findField('orgName').setValue(rec.get('orgName'));
	},
	setStore : function(rec) {
		var me = this;
		me.getForm().findField('storeId').setValue(rec.get('storeId'));
		me.getForm().findField('storeName').setValue(rec.get('cname'));
	}
});