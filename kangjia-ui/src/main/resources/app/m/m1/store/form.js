Ext.define("app.m.m1.store.form", {
	extend : 'Ext.form.Panel',
	border : false,
	layout : 'column',
	region : 'center',
	requires : [ 'app.m.m1.store.model' ],
	initComponent : function() {
		var me = this;
		var fields = me.createfield();
		Ext.apply(me, {
			items : fields,
			listeners : {
				afterrender : function() {
					if (me.storeId) {
						utils.loadform({
							form : me,
							model : 'app.m.m1.store.model',
							url : baseurl + '/b/store/find',
							extraParams : {
								'storeId' : me.storeId
							},
							fn : function(rec) {
								me.getForm().loadRecord(rec);
							}
						});
					}
				}
			}
		});
		me.callParent(arguments);
	},
	createfield : function() {
		var me = this;
		var array = new Array();
		utils.pushfieldarray(array, '1', {
			name : 'storeNum',
			afterLabelTextTpl : required,
			fieldLabel : '编号',
			allowBlank : false,
			minLength : 1,
			maxLength : 8
		});
		utils.pushfieldarray(array, '1', {
			name : 'cname',
			afterLabelTextTpl : required,
			fieldLabel : '名称',
			allowBlank : false,
			minLength : 1,
			maxLength : 16
		});
		utils.pushfieldarray(array, '1', {
			name : 'address',
			fieldLabel : '地址',
			maxLength : 64
		});
		return array;
	}
});