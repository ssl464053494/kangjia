Ext.define('app.m.m1.store.field', {
	extend : 'Ext.form.field.Picker',
	editable : false,
	alias : 'widget.storefield',
	requires : [ 'app.m.m1.store.grid' ],
	onTriggerClick : function() {
		var me = this;
		var tree = Ext.create('app.m.m1.store.grid', {
			region : 'center',
			url : me.url,
			btn : false,
			extraParams : me.extraParams
		});
		var win = widget.win({
			width : 600,
			items : [ tree ],
			buttons : [ {
				text : '确认',
				handler : function() {
					var rec = tree.getStoreRecord();
					if (rec != null) {
						me._target_.setStore(rec);
						win.close();
					} else {
						Ext.Msg.alert('提示', '选择门店');
						return;
					}
				}
			}, {
				text : '关闭',
				handler : function() {
					win.close();
				}
			} ]
		}).show();
	}
});