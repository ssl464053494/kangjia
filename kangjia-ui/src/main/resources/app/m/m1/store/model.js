Ext.define('app.m.m1.store.model', {
	extend : 'Ext.data.Model',
	fields : [ {
		name : 'storeId',
		type : 'int'
	}, {
		name : 'storeNum',
		type : 'string'
	}, {
		name : 'cname',
		type : 'string'
	}, {
		name : 'address',
		type : 'string'
	}, {
		name : 'orgId',
		type : 'int'
	} ]
});