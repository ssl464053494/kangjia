Ext.define('app.m.m1.store.grid', {
	extend : "Ext.grid.Panel",
	requires : [ 'app.m.m1.store.model', 'app.m.m1.store.win' ],
	frame : false,
	border : false,
	btn : true,
	initComponent : function() {
		var me = this;
		var columns = me.createcolumns();
		var store = me.createstore();
		var dockitems = new Array();
		var queryarry = me.createquery(store);
		if (queryarry != null) {
			me.qf = widget.search(queryarry);
			dockitems.push(me.qf);
		}
		var toolarray = me.createtool();
		if (toolarray.length > 0) {
			dockitems.push(widget.toolbar(toolarray));
		}
		dockitems.push(widget.paging(store));
		Ext.apply(me, {
			store : store,
			columns : columns,
			singleExpand : me.singleExpand || true,
			dockedItems : dockitems
		});
		store.load();
		me.callParent(arguments);
	},
	createquery : function(store) {
		var me = this;
		var param = {};
		var array = new Array();
		array.push({
			xtype : 'textfield',
			name : 'cname',
			labelWidth : 60,
			style : 'margin: 5px 5px 0 5px',
			fieldLabel : '名称:',
			labelAlign : 'right',
			labelSeparator : ''
		});
		param['bl'] = 'center';
		param['items'] = array;
		param['ds'] = store;
		var array = new Array();
		param['btn'] = array;
		return param;
	},
	doAdd : function() {
		var me = this;
		Ext.create('app.m.m1.store.win', {
			_target_ : me
		}).show();
	},
	doUpdate : function() {
		var me = this;
		var rec = utils.single(me);
		if (rec != null) {
			Ext.create('app.m.m1.store.win', {
				storeId : rec.get('storeId'),
				_target_ : me
			}).show();
		} else {
			Ext.Msg.alert('提示', '请选择记录');
		}
	},
	doDelete : function() {
		var me = this;
		var rec = utils.single(me);
		if (rec != null) {
			var param = {
				storeId : rec.get('storeId')
			};
			evt.confirm(me, '是否删除?', baseurl + '/b/store/delete', param, function() {
				me.store.reload();
			});
		} else {
			Ext.Msg.alert('提示', '请选择记录');
		}
	},
	createtool : function() {
		var me = this;
		var array = new Array();
		if (me.orgId) {
			return array;
		}
		if (!me.btn) {
			return array;
		}
		utils.pushbtnarray(array, {
			text : '新增',
			btnid : 49,
			handler : function() {
				me.doAdd(me);
			}
		});
		utils.pushbtnarray(array, {
			text : '修改',
			btnid : 49,
			handler : function() {
				me.doUpdate(me);
			}
		});
		utils.pushbtnarray(array, {
			text : '删除',
			btnid : 51,
			handler : function() {
				me.doDelete(me);
			}
		});
		return array;
	},
	createcolumns : function() {
		var columns = new Array();
		columns.push(Ext.create('Ext.grid.RowNumberer'));
		var storeName = widget.column({
			text : '名称',
			dataIndex : 'cname'
		});
		columns.push(storeName);
		var storeNum = widget.column({
			text : '编号',
			dataIndex : 'storeNum'
		});
		columns.push(storeNum);
		var address = widget.column({
			text : '地址',
			dataIndex : 'address'
		});
		columns.push(address);
		return columns;
	},
	createstore : function() {
		var me = this;
		var orgId = me.orgId
		var extraParams = {
			orgId : me.orgId
		};
		return widget.ds({
			clazz : 'app.m.m1.store.model',
			url : baseurl + '/b/store/search'
		});
	},
	doRefresh : function() {
		var me = this;
		me.store.reload();
	},
	getStoreRecord : function() {
		var me = this;
		return utils.single(me);
	}
});