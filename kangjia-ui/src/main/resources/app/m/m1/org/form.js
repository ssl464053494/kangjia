Ext.define("app.m.m1.org.form", {
	extend : 'Ext.form.Panel',
	border : false,
	layout : 'column',
	region : 'center',
	requires : [ 'app.m.m1.org.model' ],
	initComponent : function() {
		var me = this;
		var fields = me.createfield();
		Ext.apply(me, {
			items : fields,
			listeners : {
				afterrender : function() {
					if (me.orgId) {
						utils.loadform({
							form : me,
							model : 'app.m.m1.org.model',
							url : baseurl + '/b/org/find',
							extraParams : {
								'orgId' : me.orgId
							},
							fn : function(rec) {
								me.getForm().loadRecord(rec);
							}
						});
					}
				}
			}
		});
		me.callParent(arguments);
	},
	createfield : function() {
		var me = this;
		var array = new Array();
		utils.pushfieldarray(array, '1', {
			name : 'orgName',
			afterLabelTextTpl : required,
			fieldLabel : '名称',
			allowBlank : false,
			minLength : 1,
			maxLength : 16
		});
		return array;
	}
});