Ext.define('app.m.m1.org.model', {
	extend : 'Ext.data.TreeModel',
	fields : [ {
		name : 'deptId',
		type : 'int'
	}, {
		name : 'parentId',
		type : 'int'
	}, {
		name : 'orgName',
		type : 'string'
	}, {
		name : 'code',
		type : 'string'
	} ]
});