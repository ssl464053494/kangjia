Ext.define('app.m.m1.org.tree', {
	extend : 'Ext.tree.Panel',
	requires : [ 'app.m.m1.org.model' ],
	useArrows : true,
	rootVisible : false,
	multiSelect : false,
	singleExpand : false,
	frame : false,
	border : false,
	displayField : 'orgName',
	initComponent : function() {
		var me = this;
		var store = me.createstore();
		Ext.apply(me, {
			store : store,
			singleExpand : me.singleExpand || true,
			listeners : {
				checkchange : function(node, checked, eOpts) {
					utils.parent(node, checked);
					utils.child(node, checked);
				}
			}
		});
		me.callParent(arguments);
	},
	createstore : function() {
		var me = this;
		return widget.ts({
			clazz : 'app.m.m1.org.model',
			url : me.url,
			extraParams : me.extraParams
		});
	},
	getCheckedRecord : function() {
		var me = this;
		var ds = utils.single(me);
		return ds;
	}
});