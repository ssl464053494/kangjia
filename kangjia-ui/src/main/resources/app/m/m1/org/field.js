Ext.define('app.m.m1.org.field', {
	extend : 'Ext.form.field.Picker',
	editable : false,
	requires : [ 'app.m.m1.org.tree' ],
	onTriggerClick : function() {
		var me = this;
		var tree = Ext.create('app.m.m1.org.tree', {
			region : 'center',
			url : me.url,
			extraParams : me.extraParams
		});
		var win = widget.win({
			width : 600,
			items : [ tree ],
			buttons : [ {
				text : '确认',
				handler : function() {
					var rec = tree.getCheckedRecord();
					if (rec != null) {
						me._target_.setOrg(rec);
						win.close();
					} else {
						Ext.Msg.alert('提示', '选择组织机构');
						return;
					}
				}
			}, {
				text : '关闭',
				handler : function() {
					win.close();
				}
			} ]
		}).show();
	}
});