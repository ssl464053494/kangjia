Ext.define("app.m.m1.org.win", {
	extend : 'Ext.window.Window',
	title : '编辑机构',
	height : 400,
	width : 600,
	closable : true,
	resizable : false,
	modal : true,
	constrain : true,
	plain : true,
	requires : [ 'app.m.m1.org.form' ],
	initComponent : function() {
		var me = this;
		var panel = Ext.create('app.m.m1.org.form', {
			orgId : me.orgId,
			parentId : me.parentId
		});
		Ext.apply(me, {
			layout : {
				type : 'border'
			},
			items : [ panel ],
			buttons : [ {
				text : '确认',
				bodyStyle : 'padding:0 0 0 5px',
				handler : function() {
					var param = {
						orgId : me.orgId,
						parentId : me.parentId
					};
					var url = baseurl + '/b/org/editor';
					evt.submit(panel, url, param, function(rec) {
						me.close();
						me._target_.doRefresh();
					});
				}
			}, {
				text : '关闭',
				bodyStyle : 'padding:0 0 0 5px',
				handler : function() {
					me.close();
				}
			} ]
		});
		me.callParent(arguments);
	}
});