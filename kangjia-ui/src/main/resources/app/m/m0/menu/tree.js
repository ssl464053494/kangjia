Ext.define('app.m.m0.menu.tree', {
	extend : 'Ext.tree.Panel',
	requires : [ 'app.m.m0.menu.model' ],
	useArrows : true,
	rootVisible : false,
	multiSelect : false,
	singleExpand : false,
	frame : false,
	border : false,
	displayField : 'cname',
	initComponent : function() {
		var me = this;
		var store = me.createstore();
		Ext.apply(me, {
			store : store,
			singleExpand : me.singleExpand || true,
			listeners : {
				checkchange : function(node, checked, eOpts) {
					utils.parent(node, checked);
					utils.child(node, checked);
				}
			}
		});
		me.callParent(arguments);
	},
	createstore : function() {
		var me = this;
		return widget.ts({
			clazz : 'app.m.m0.menu.model',
			url : me.url,
			extraParams : me.extraParams
		});
	},
	getCheckedRecord : function() {
		var me = this;
		var ds = me.getChecked();
		var array = new Array();
		for (var i = 0; i < ds.length; i++) {
			array.push(ds[i].data);
		}
		return array;
	}
});