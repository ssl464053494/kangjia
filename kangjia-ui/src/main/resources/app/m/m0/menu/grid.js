Ext.define('app.m.m0.menu.grid', {
	extend : 'Ext.tree.Panel',
	requires : [ 'app.m.m0.menu.model', 'app.m.m0.menu.win' ],
	region : 'center',
	reserveScrollbar : true,
	useArrows : true,
	rootVisible : false,
	multiSelect : true,
	singleExpand : true,
	frame : false,
	border : false,
	initComponent : function() {
		var me = this;
		var columns = me.createcolumns();
		var store = me.createstore();
		var dockitems = new Array();
		var toolarray = me.createtool();
		if (toolarray.length > 0) {
			dockitems.push(widget.toolbar(toolarray));
		}
		Ext.apply(me, {
			store : store,
			columns : columns,
			singleExpand : me.singleExpand || true,
			plugins: [{
		        ptype: 'rowediting',
		        clicksToMoveEditor: 1,
		        autoCancel: false
		    }],
			dockedItems : dockitems
		});
		me.callParent(arguments);
	},
	createcolumns : function() {
		var columns = new Array();
		columns.push(Ext.create('Ext.grid.RowNumberer'));
		var cname = widget.treecolumn({
			text : '菜单名称',
			dataIndex : 'cname'
		});
		columns.push(cname);
		var compant = widget.column({
			text : '控件',
			dataIndex : 'compant'
		});
		columns.push(compant);
		var img = widget.column({
			text : '图片',
			dataIndex : 'img'
		});
		columns.push(img);
		var type = widget.column({
			text : '类型',
			dataIndex : 'type',
			renderer : function(v) {
				return v == 1 ? '功能' : '菜单';
			}
		});
		columns.push(type);
		var remark = widget.column({
			text : '说明',
			dataIndex : 'remark'
		});
		columns.push(remark);
		var operation = widget.column({
			text : '操作',
			dataIndex : 'operation'
		});
		columns.push(operation);
		var flag = widget.column({
			text : '状态',
			dataIndex : 'flag',
			renderer : function(v) {
				console.log(v);
				return v == 0 ? '启用' : '禁用';
			}
		});
		columns.push(flag);
		return columns;
	},
	createstore : function() {
		var me = this;
		return widget.ts({
			clazz : 'app.m.m0.menu.model',
			url : baseurl + '/b/menu/search'
		});
	},
	createtool : function() {
		var me = this;
		var array = new Array();
		utils.pushbtnarray(array, {
			text : '新增',
			handler : function() {
				me.doAdd();
			}
		});
		utils.pushbtnarray(array, {
			text : '修改',
			handler : function() {
				me.doUpdate();
			}
		});
		utils.pushbtnarray(array, {
			text : '删除',
			handler : function() {
				me.doDelete();
			}
		});
		utils.pushbtnarray(array, {
			text : '启用/禁用',
			handler : function() {
				me.doStartAndStop();
			}
		});
		return array;
	},
	doStartAndStop : function() {
		var me = this;
		var rec = utils.single(me);
		if (rec != null) {
			var param = {
				menuId : rec.get('menuId')
			};
			evt.confirm(me, '是否执行操作?', baseurl + '/b/menu/startAndStop',
					param, function() {
						me.store.reload();
					});
		} else {
			Ext.Msg.alert('提示', '请选择记录');
		}
	},
	doAdd : function() {
		var me = this;
		var rec = utils.single(me);
		var parentId = null;
		if (rec != null) {
			parentId = rec.get('menuId');
		}
		Ext.create('app.m.m0.menu.win', {
			parentId : parentId,
			_target_ : me
		}).show()
	},
	doUpdate : function() {
		var me = this;
		var rec = utils.single(me);
		if (rec != null) {
			Ext.create('app.m.m0.menu.win', {
				menuId : rec.get('menuId'),
				_target_ : me
			}).show();
		} else {
			Ext.Msg.alert('提示', '请选择记录');
		}
	},
	doDelete : function() {
		var me = this;
		var rec = utils.single(me);
		if (rec != null) {
			var param = {
				menuId : rec.get('menuId')
			};
			evt.confirm(me, '是否删除?', baseurl + '/b/menu/delete', param, function() {
				me.store.reload();
			});
		} else {
			Ext.Msg.alert('提示', '请选择记录');
		}
	},
	doRefresh : function() {
		var me = this;
		me.store.reload();
	}
});