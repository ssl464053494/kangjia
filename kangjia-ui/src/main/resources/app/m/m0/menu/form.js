Ext.define("app.m.m0.menu.form", {
	extend : 'Ext.form.Panel',
	border : false,
	layout : 'column',
	region : 'center',
	requires : [ 'app.m.m0.menu.model' ],
	initComponent : function() {
		var me = this;
		var fields = me.createfield();
		Ext.apply(me, {
			items : fields,
			listeners : {
				afterrender : function() {
					if (me.menuId) {
						utils.loadform({
							form : me,
							model : 'app.m.m0.menu.model',
							url : baseurl + '/b/menu/find',
							extraParams : {
								'menuId' : me.menuId
							},
							fn : function(rec) {
								me.getForm().loadRecord(rec);
							}
						});
					}
				}
			}
		});
		me.callParent(arguments);
	},
	createfield : function() {
		var me = this;
		var array = new Array();
		utils.pushfieldarray(array, '1', {
			columnWidth : .5,
			name : 'cname',
			afterLabelTextTpl : required,
			fieldLabel : '名称',
			allowBlank : false,
			minLength : 1,
			maxLength : 16
		});
		utils.pushfieldarray(array, '1', {
			columnWidth : .5,
			name : 'compant',
			afterLabelTextTpl : required,
			fieldLabel : '控件',
			minLength : 0,
			maxLength : 64
		});
		var states = Ext.create('Ext.data.Store', {
		    fields: ['value', 'name'],
		    data : [
		        {"value":0, "name":"菜单"},
		        {"value":1, "name":"功能"}
		    ]
		});
		utils.pushfieldarray(array, 'Ext.form.ComboBox', {
			name : 'type',
			fieldLabel: '菜单类型',
		    store: states,
		    columnWidth : .5,
		    queryMode: 'local',
		    allowBlank : false,
		    displayField: 'name',
		    valueField: 'value'
		});
		utils.pushfieldarray(array, '1', {
			name : 'img',
			fieldLabel : '图片',
			columnWidth : .5
		});
		utils.pushfieldarray(array, '6', {
			name : 'operation',
			fieldLabel : '操作',
			minLength : 0,
			maxLength : 128
		});
		utils.pushfieldarray(array, '6', {
			name : 'remark',
			fieldLabel : '备注',
			minLength : 0,
			maxLength : 128
		});
		return array;
	}
});