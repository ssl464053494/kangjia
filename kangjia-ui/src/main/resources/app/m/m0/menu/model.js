Ext.define('app.m.m0.menu.model', {
	extend : 'Ext.data.TreeModel',
	fields : [ {
		name : 'menuId',
		type : 'int'
	}, {
		name : 'parentId',
		type : 'int'
	}, {
		name : 'type',
		type : 'int'
	}, {
		name : 'cname',
		type : 'string'
	}, {
		name : 'compant',
		type : 'string'
	}, {
		name : 'img',
		type : 'string'
	}, {
		name : 'remark',
		type : 'string'
	}, {
		name : 'operation',
		type : 'string'
	}, {
		name : 'flag',
		type : 'int'
	} ]
});