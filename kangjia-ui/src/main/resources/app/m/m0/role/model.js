Ext.define('app.m.m0.role.model', {
	extend : 'Ext.data.Model',
	fields : [ {
		name : 'roleId',
		type : 'int'
	}, {
		name : 'roleName',
		type : 'string'
	}, {
		name : 'authority',
		type : 'string'
	} ]
});