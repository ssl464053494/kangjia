Ext.define('app.m.m0.role.field', {
	extend : 'Ext.form.ComboBox',
	editable : false,
	alias : 'widget.rolefield',
	requires : [ 'app.m.m0.role.model' ],
	queryMode: 'local',
    displayField: 'roleName',
    valueField: 'roleId',
	initComponent : function() {
		var me = this;
		var store = Ext.create('Ext.data.Store', {
			model : 'app.m.m0.role.model',
			proxy : {
				actionMethods: {
		            create: 'POST',
		            read: 'POST',
		            update: 'POST',
		            destroy: 'POST'
		        },
				type : 'ajax',
				url : baseurl + '/b/role/query',
				reader : {
					rootProperty : 'data'
				}
			},
			autoLoad : true
		})
		Ext.apply(me, {
			store : store
		});
		me.callParent(arguments);
	}
});