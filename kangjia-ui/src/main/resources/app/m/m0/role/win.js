Ext.define("app.m.m0.role.win", {
	extend : 'Ext.window.Window',
	title : '编辑角色',
	height : 500,
	width : 600,
	closable : true,
	resizable : false,
	modal : true,
	constrain : true,
	plain : true,
	requires : [ 'app.m.m0.role.form' ],
	initComponent : function() {
		var me = this;
		var panel = Ext.create('app.m.m0.role.form', {
			roleId : me.roleId
		});
		Ext.apply(me, {
			layout : {
				type : 'border'
			},
			items : [ panel ],
			buttons : [ {
				text : '确认',
				bodyStyle : 'padding:0 0 0 5px',
				handler : function() {
					var authority = panel.getAuthority();
					var param = {
						roleId : me.roleId,
						authority : authority
					};
					var url = baseurl + '/b/role/editor';
					evt.submit(panel, url, param, function(rec) {
						me.close();
						me._target_.doRefresh();
					});
				}
			}, {
				text : '关闭',
				bodyStyle : 'padding:0 0 0 5px',
				handler : function() {
					me.close();
				}
			} ]
		});
		me.callParent(arguments);
	}
});