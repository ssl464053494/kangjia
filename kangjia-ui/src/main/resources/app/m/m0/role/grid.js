Ext.define('app.m.m0.role.grid', {
	extend : "Ext.grid.Panel",
	requires : [ 'app.m.m0.role.model', 'app.m.m0.role.win' ],
	frame : false,
	border : false,
	initComponent : function() {
		var me = this;
		var columns = me.createcolumns();
		var store = me.createstore();
		var dockitems = new Array();
		var toolarray = me.createtool();
		if (toolarray.length > 0) {
			dockitems.push(widget.toolbar(toolarray));
		}
		dockitems.push(widget.paging(store));
		Ext.apply(me, {
			store : store,
			columns : columns,
			singleExpand : me.singleExpand || true,
			dockedItems : dockitems
		});
		store.load();
		me.callParent(arguments);
	},
	doAdd : function() {
		var me = this;
		Ext.create('app.m.m0.role.win', {
			_target_ : me
		}).show();
	},
	doUpdate : function() {
		var me = this;
		var rec = utils.single(me);
		if (rec != null) {
			Ext.create('app.m.m0.role.win', {
				roleId : rec.get('roleId'),
				_target_ : me
			}).show();
		} else {
			Ext.Msg.alert('提示', '请选择记录');
		}
	},
	doDelete : function() {
		var me = this;
		var rec = utils.single(me);
		if (rec != null) {
			var param = {
				roleId : rec.get('roleId')
			};
			evt.confirm(me, '是否删除?', baseurl + '/b/role/delete', param, function() {
				me.store.reload();
			});
		} else {
			Ext.Msg.alert('提示', '请选择记录');
		}
	},
	createtool : function() {
		var me = this;
		var array = new Array();
		utils.pushbtnarray(array, {
			text : '新增',
			btnid : 49,
			handler : function() {
				me.doAdd(me);
			}
		});
		utils.pushbtnarray(array, {
			text : '修改',
			btnid : 49,
			handler : function() {
				me.doUpdate(me);
			}
		});
		utils.pushbtnarray(array, {
			text : '删除',
			btnid : 51,
			handler : function() {
				me.doDelete(me);
			}
		});
		return array;
	},
	createcolumns : function() {
		var columns = new Array();
		columns.push(Ext.create('Ext.grid.RowNumberer'));
		var roleName = widget.column({
			text : '名称',
			dataIndex : 'roleName'
		});
		columns.push(roleName);
		return columns;
	},
	createstore : function() {
		return widget.ds({
			clazz : 'app.m.m0.role.model',
			url : baseurl + '/b/role/search'
		});
	},
	doRefresh : function() {
		var me = this;
		me.store.reload();
	}
});