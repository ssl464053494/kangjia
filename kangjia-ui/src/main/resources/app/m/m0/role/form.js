Ext.define("app.m.m0.role.form", {
	extend : 'Ext.form.Panel',
	border : false,
	layout : 'column',
	region : 'center',
	requires : [ 'app.m.m0.role.model' , 'app.m.m0.menu.tree'],
	initComponent : function() {
		var me = this;
		var fields = me.createfield();
		Ext.apply(me, {
			items : fields,
			listeners : {
				afterrender : function() {
					if (me.roleId) {
						utils.loadform({
							form : me,
							model : 'app.m.m0.role.model',
							url : baseurl + '/b/role/find',
							extraParams : {
								'roleId' : me.roleId
							},
							fn : function(rec) {
								me.getForm().loadRecord(rec);
							}
						});
					}
				}
			}
		});
		me.callParent(arguments);
	},
	createfield : function() {
		var me = this;
		var array = new Array();
		utils.pushfieldarray(array, '1', {
			name : 'roleName',
			afterLabelTextTpl : required,
			fieldLabel : '名称',
			allowBlank : false,
			minLength : 1,
			maxLength : 16
		});
		me.tree = Ext.create('app.m.m0.menu.tree', {
			title : '设置权限',
			margin : '10 10 10 10',
			columnWidth : 1,
			height : 300,
			width : 400,
			url : baseurl + '/b/role/authority',
			extraParams : {
				roleId : me.roleId
			}
		});
		array.push(me.tree);
		return array;
	},
	getAuthority : function() {
		var me = this;
		var arrays = me.tree.getCheckedRecord();
		var n = "";
		for (var i = 0; i < arrays.length; i++) {
			if (i == 0) {
				n = arrays[i].menuId;
			} else {
				n = n + "," + arrays[i].menuId;
			}
		}
		return n;
	}
});