package com.kangjia.response;

import java.util.ArrayList;
import java.util.List;

public class PageResponse<T> extends AbstractResponse {

	private Integer total;
	private List<T> data = new ArrayList<>();

	public PageResponse(Integer total, List<T> data) {
		super(200);
		this.total = total;
		this.data = data;
	}

	public PageResponse(List<T> data) {
		super(200);
		this.data = data;
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	public List<T> getData() {
		return data;
	}

	public void setData(List<T> data) {
		this.data = data;
	}
}
