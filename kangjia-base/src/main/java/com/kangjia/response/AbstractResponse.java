package com.kangjia.response;

/**
 * 响应结果
 * 
 * @author sunshulin
 *
 */
public abstract class AbstractResponse {

	/** 响应结果 */
	private Boolean success;
	/** 响应码 */
	private Integer rpc;

	public AbstractResponse(Integer rpc) {
		super();
		this.rpc = rpc;
		this.success = this.rpc == 200;
	}

	public Boolean getSuccess() {
		return success;
	}

	public void setSuccess(Boolean success) {
		this.success = success;
	}

	public Integer getRpc() {
		return rpc;
	}

	public void setRpc(Integer rpc) {
		this.rpc = rpc;
	}
}
