package com.kangjia.response;

public class ErrorResponse extends AbstractResponse {

	private String msg;

	public ErrorResponse(String msg) {
		super(500);
		this.msg = msg;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
}