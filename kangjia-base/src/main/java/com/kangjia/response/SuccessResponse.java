package com.kangjia.response;

public class SuccessResponse extends AbstractResponse {

	/** 响应内容 */
	private String msg;

	public SuccessResponse(String msg) {
		super(200);
		this.msg = msg;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

}
