package com.kangjia.model.service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kangjia.general.dao.GeneralDao;
import com.kangjia.general.dao.query.OperType;
import com.kangjia.general.dao.query.Param;
import com.kangjia.general.dao.query.QueryParam;
import com.kangjia.jasckson.DataMapper;
import com.kangjia.model.m1.entity.Store;
import com.kangjia.model.m2.entity.Device;
import com.kangjia.model.m3.entity.IndexType;
import com.kangjia.model.m3.entity.KangjiaKpi;
import com.kangjia.model.m3.entity.Product;
import com.kangjia.model.m3.entity.Relation;

import lombok.extern.slf4j.Slf4j;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

@Service
@Slf4j
public class InitService {

	@Autowired
	private GeneralDao generalDao;
	@Autowired
	protected JedisPool jedisPool;

	@PostConstruct
	public void init() {
		log.info("初始化指标项数据");
		initKangjiaKpi();
		initIndexType();
		initProduct();
		initRelation();
		initStore();
		initDevice();
	}

	private void initKangjiaKpi() {
		log.info("初始化康加指标数据项");
		List<KangjiaKpi> list = generalDao.query(KangjiaKpi.class, null);
		Jedis jedis = jedisPool.getResource();
		try {
			jedis.del("kangjia.kpi.data");
			for (KangjiaKpi kangjiaKpi : list) {
				String json = DataMapper.write(kangjiaKpi);
				jedis.hset("kangjia.kpi.data", String.valueOf(kangjiaKpi.getTargetId()), json);
			}
		} finally {
			jedis.close();
		}
	}

	private void initIndexType() {
		log.info("初始化建议指标数据项");
		QueryParam param = new QueryParam();
		param.addParam(new Param<Integer>("status", OperType.eq, 0));
		List<IndexType> list = generalDao.query(IndexType.class, param);
		Jedis jedis = jedisPool.getResource();
		try {
			for (IndexType indexType : list) {
				jedis.hset("indexType", indexType.getIndexTypeId().toString(), DataMapper.write(indexType));
				jedis.del("indexType_" + indexType.getIndexTypeId());
			}
		} finally {
			jedis.close();
		}
	}

	private void initProduct() {
		log.info("初始化建议指标推荐保健产品数据项");
		List<Product> list = generalDao.query(Product.class, null);
		Jedis jedis = jedisPool.getResource();
		try {
			for (Product product : list) {
				jedis.hset("indexType_" + product.getIndexTypeId(), product.getProductId().toString(),
						DataMapper.write(product));
			}
		} finally {
			jedis.close();
		}
	}

	private void initRelation() {
		log.info("初始化康加指标与建议指标关联数据项");
		List<Relation> list = generalDao.query(Relation.class, null);
		Jedis jedis = jedisPool.getResource();
		try {
			jedis.del("relation");
			Set<String> eKey = new HashSet<>();
			for (Relation r : list) {
				if (r.getRelationId() != null) {
					String key = r.getTargetId() + "." + r.getClassify() + "." + r.getInspectStandard();
					if (!eKey.contains(key)) { // 判断key是否清楚过,没有清楚key
						jedis.del(key);
						eKey.add(key);
						jedis.sadd("relation", key);
					}
					jedis.sadd(key, r.getIndexTypeId().toString());
				}
			}
		} finally {
			jedis.close();
		}
	}

	private void initStore() {
		log.info("初始化店铺数据项");
		QueryParam param = new QueryParam();
		param.addParam(new Param<Integer>("status", OperType.eq, 0));
		List<Store> list = generalDao.query(Store.class, param);
		Jedis jedis = jedisPool.getResource();
		try {
			jedis.del("store");
			for (Store store : list) {
				jedis.hset("store", store.getStoreId().toString(), DataMapper.write(store));
			}
		} finally {
			jedis.close();
		}
	}

	private void initDevice() {
		log.info("初始化设备数据项");
		QueryParam param = new QueryParam();
		param.addParam(new Param<Integer>("status", OperType.eq, 0));
		List<Device> list = generalDao.query(Device.class, param);
		Jedis jedis = jedisPool.getResource();
		try {
			jedis.del("device");
			for (Device device : list) {
				jedis.hset("device", device.getMac(), DataMapper.write(device));
			}
		} finally {
			jedis.close();
		}
	}
}
