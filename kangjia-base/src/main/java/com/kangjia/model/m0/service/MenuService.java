package com.kangjia.model.m0.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kangjia.general.dao.GeneralAnnotationDao;
import com.kangjia.general.dao.query.OperType;
import com.kangjia.general.dao.query.Param;
import com.kangjia.general.dao.query.QueryParam;
import com.kangjia.model.m0.entity.Menu;
import com.kangjia.model.service.TreeService;

@Service
public class MenuService extends TreeService<Menu> {

	@Autowired
	private GeneralAnnotationDao<Menu> generalDao;
	
	public void update(Menu entity) {
		generalDao.saveOrUpdate(entity);
	}

	public List<Menu> filter(List<Menu> menus, String[] ms) {
		List<Menu> nlist = new ArrayList<>();
		for (String s : ms) {
			Menu current = null;
			for (Menu m : menus) {
				if (StringUtils.isNotBlank(s) && m.getMenuId().equals(Integer.parseInt(s))) {
					nlist.add(m);
					break;
				}
			}
			menus.remove(current);
		}
		return nlist;
	}

	public List<Menu> query() {
		QueryParam param = new QueryParam();
		param.addParam(new Param<Integer>("status", OperType.eq, 0));
		return generalDao.query(Menu.class, param);
	}

	public Menu createT() {
		return new Menu();
	}

	public Integer getId(Menu entity) {
		return entity.getMenuId();
	}

	public Integer getParent(Menu entity) {
		return entity.getParentId();
	}

	public void addChildren(Menu entity, Menu node) {
		entity.getChildren().add(node);
	}

	public void checked(Menu entity, boolean is) {
		entity.setChecked(is);
	}

	public void expand(Menu entity, boolean is) {
		entity.setExpanded(is);
	}

}
