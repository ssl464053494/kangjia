package com.kangjia.model.m0.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kangjia.general.controller.AbstractController;
import com.kangjia.general.dao.query.OperType;
import com.kangjia.general.dao.query.Page;
import com.kangjia.general.dao.query.Param;
import com.kangjia.general.dao.query.QueryParam;
import com.kangjia.model.m0.entity.Menu;
import com.kangjia.model.m0.entity.Role;
import com.kangjia.model.m0.service.MenuService;
import com.kangjia.response.AbstractResponse;

@Controller
@RequestMapping(value = "/b/role", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class RoleController extends AbstractController<Role> {

	@Autowired
	private MenuService menuService;

	@RequestMapping(value = "/editor")
	@ResponseBody
	public AbstractResponse editor(@RequestBody Role role) {
		if (role.getRoleId() == null) {
			role.setStatus(0);
		}
		return super.editorx(role);
	}

	@RequestMapping(value = "/delete")
	@ResponseBody
	public AbstractResponse delete(@RequestBody Role role) {
		return super.delete(role.getRoleId(), null);
	}

	@RequestMapping(value = "/find")
	@ResponseBody
	public AbstractResponse find(@RequestBody Role role) {
		return super.find(role.getRoleId(), null);
	}

	@RequestMapping(value = "/search")
	@ResponseBody
	public AbstractResponse search(@RequestBody Role role) {
		Page page = new Page();
		page.setStart(role.getStart());
		page.setLimit(role.getLimit());
		QueryParam param = new QueryParam(page);
		param.addParam(new Param<Integer>("status", OperType.eq, 0));
		param.addParam(new Param<Integer>("roleId", OperType.nq, 1));
		param.addParam(new Param<Integer>("roleId", OperType.nq, 2));
		return super.query(param);
	}
	
	@RequestMapping(value = "/query")
	@ResponseBody
	public AbstractResponse query(@RequestBody Role role) {
		Page page = new Page();
		QueryParam param = new QueryParam(page);
		param.addParam(new Param<Integer>("status", OperType.eq, 0));
		param.addParam(new Param<Integer>("roleId", OperType.nq, 1));
		param.addParam(new Param<Integer>("roleId", OperType.nq, 2));
		return super.query(param);
	}

	@RequestMapping(value = "/authority")
	@ResponseBody
	public Menu authority(@RequestBody Role role) {
		List<Menu> menus = menuService.query();
		List<Integer> ids = new ArrayList<>();
		if (role.getRoleId() != null) {
			Role entity = super.findById(role.getRoleId(), null);
			String[] authoritys = entity.getAuthority().split(",");
			for (String s : authoritys) {
				if (StringUtils.isNotBlank(s)) {
					ids.add(Integer.parseInt(s));
				}
			}
		}
		return menuService.createRoot(menus, ids);
	}

	public Serializable getPrimary(Role entity) {
		return entity.getRoleId();
	}

	public Class<Role> getClazz() {
		return Role.class;
	}

	public String[] ignores() {
		return new String[] { "status" };
	}

	public void status(Role entity) {
		entity.setStatus(-1);
	}

}
