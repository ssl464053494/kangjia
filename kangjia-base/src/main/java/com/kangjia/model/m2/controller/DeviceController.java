package com.kangjia.model.m2.controller;

import java.io.Serializable;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kangjia.general.controller.AbstractController;
import com.kangjia.general.controller.call.Call;
import com.kangjia.general.dao.query.OperType;
import com.kangjia.general.dao.query.Page;
import com.kangjia.general.dao.query.Param;
import com.kangjia.general.dao.query.QueryParam;
import com.kangjia.jasckson.DataMapper;
import com.kangjia.model.m1.entity.Employee;
import com.kangjia.model.m2.entity.Device;
import com.kangjia.model.m2.service.DeviceService;
import com.kangjia.response.AbstractResponse;
import com.kangjia.response.ErrorResponse;
import com.kangjia.response.ObjectResponse;
import com.kangjia.response.SuccessResponse;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

@Controller
@RequestMapping(value = "/b/device", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class DeviceController extends AbstractController<Device> {

	@Autowired
	private DeviceService deviceService;
	@Autowired
	protected JedisPool jedisPool;
	
	@RequestMapping(value = "/editor")
	@ResponseBody
	public AbstractResponse editor(@RequestBody Device device) {
		if (device.getDeviceId() == null) {
			boolean is = deviceService.exist("sn", device.getSn());
			if (is) {
				return new ErrorResponse("机器号存在");
			}
			is = deviceService.exist("mac", device.getMac());
			if (is) {
				return new ErrorResponse("mac地址存在");
			}
			device.setStatus(0);
			device.setFlag(0);
		}
		ObjectResponse<Device> response = super.editorx(device);
		redis(response.getData());
		return response;
	}
	
	@RequestMapping(value = "/startAndStop")
	@ResponseBody
	public AbstractResponse startAndStop(@RequestBody Device device) {
		Device entity = findById(device.getDeviceId(), null);
		if (entity != null) {
			if (!entity.getStatus().equals(0)) {
				return new ErrorResponse("设备不存在");
			}
			if (entity.getFlag() != null && entity.getFlag().equals(0)) {
				entity.setFlag(1);
			} else {
				entity.setFlag(0);
			}
			deviceService.update(entity);
			redis(entity);
		} else {
			return new ErrorResponse("设备不存在");
		}
		return new SuccessResponse("操作成功");
	}

	@RequestMapping(value = "/find")
	@ResponseBody
	public AbstractResponse find(@RequestBody Device device) {
		return super.find(device.getDeviceId(), new Call<Device>() {
			public void call(Device entity) {
				deviceService.getStore(entity);
			}
		});
	}
	
	@RequestMapping(value = "/delete")
	@ResponseBody
	public AbstractResponse delete(@RequestBody Device device) {
		Device old = super.findById(device.getDeviceId(), null);
		if (old == null) {	
			return new ErrorResponse("没有找到你了");
		}
		AbstractResponse response = super.deleteById(device.getDeviceId(), null);
		if (response.getRpc().equals(200)) {			
			Jedis jedis = jedisPool.getResource();
			try {
				jedis.hdel("device", old.getMac());
			} finally {
				jedis.close();
			}
		}
		return response;
	}

	@RequestMapping(value = "/search")
	@ResponseBody
	public AbstractResponse tree(@RequestBody Device device) {
		Page page = new Page();
		page.setStart(device.getStart());
		page.setLimit(device.getLimit());
		QueryParam param = new QueryParam(page);
		param.addParam(new Param<Integer>("status", OperType.eq, 0));
		if (device.getStoreId() != null) {
			param.addParam(new Param<Integer>("storeId", OperType.eq, device.getStoreId()));
		}
		if (StringUtils.isNotBlank(device.getSn())) {
			param.addParam(new Param<String>("sn", OperType.eq, device.getSn()));
		}
		if (StringUtils.isNotBlank(device.getMac())) {
			param.addParam(new Param<String>("mac", OperType.eq, device.getMac()));
		}
		setting(param);
		return super.query(param, new Call<Device>() {
			public void call(Device entity) {
				deviceService.getStore(entity);
			}
		});
	}

	public Serializable getPrimary(Device entity) {
		return entity.getDeviceId();
	}

	public Class<Device> getClazz() {
		return Device.class;
	}

	public void status(Device entity) {
		entity.setStatus(-1);
	}

	public String[] ignores() {
		return new String[] { "status" };
	}

	public void setting(QueryParam param) {
		Employee employee = getEmployee();
		if (employee.getBusId().equals(1) || employee.getBusId().equals(0)) {
			param.addParam(new Param<Integer>("storeId", OperType.eq, employee.getStoreId()));			
		} else if (employee.getBusId().equals(2)) {
			
		} else {
			throw new RuntimeException("未知");
		}
	}
	
	private void redis(Device device) {
		Jedis jedis = jedisPool.getResource();
		try {
			jedis.hset("device", device.getMac(), DataMapper.write(device));
		} finally {
			jedis.close();
		}
	}
}
