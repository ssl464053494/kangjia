package com.kangjia.model.m2.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kangjia.general.dao.GeneralDao;
import com.kangjia.general.dao.query.OperType;
import com.kangjia.general.dao.query.Param;
import com.kangjia.general.dao.query.QueryParam;
import com.kangjia.model.m1.entity.Org;
import com.kangjia.model.m1.entity.Store;
import com.kangjia.model.m2.entity.Device;

@Service
public class DeviceService {

	@Autowired
	private GeneralDao generalDao;
	
	public void update(Device entity) {
		generalDao.saveOrUpdate(entity);
	}
	
	public boolean exist(String key, String value) {
		QueryParam param = new QueryParam();
		param.addParam(new Param<String>(key, OperType.eq, value));
		Integer count = generalDao.count(Device.class, param);
		return count != null && count > 0 ? true : false;
	}
	
	public void getStore(Device device) {
		if (device.getOrgId() != null) {
			Org org = generalDao.get(Org.class, device.getOrgId());
			if (org != null) {
				device.setOrgName(org.getOrgName());
			}
		}
		if (device.getStoreId() != null) {
			Store store = generalDao.get(Store.class, device.getStoreId());
			if (store != null) {
				device.setStoreName(store.getCname());
			}
		}
	}
}
