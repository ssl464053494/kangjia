package com.kangjia.model.m3.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Type;

import com.kangjia.general.request.JsonRequest;

@Entity
@Table(name = "ts_kangjia_kpi")
public class KangjiaKpi extends JsonRequest implements Serializable {

	private static final long serialVersionUID = -4858100384006812256L;

	private Integer targetId;
	private String inspectName;
	private Integer parentId;
	private Integer inspectLevel;
	private String inspectExplain;
	private String guideContent;
	private String suggestContent;
	private String improvementProgram;
	private Integer type;
	private Integer sorts;
	private String updateTime;

	private List<KangjiaKpi> children = new ArrayList<>();
	
	@Id
	@Column(name = "target_id", unique = true, nullable = false)
	public Integer getTargetId() {
		return targetId;
	}

	public void setTargetId(Integer targetId) {
		this.targetId = targetId;
	}

	@Column(name = "inspect_name")
	public String getInspectName() {
		return inspectName;
	}

	public void setInspectName(String inspectName) {
		this.inspectName = inspectName;
	}

	@Column(name = "parent_id")
	public Integer getParentId() {
		return parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	@Column(name = "inspect_level")
	public Integer getInspectLevel() {
		return inspectLevel;
	}

	public void setInspectLevel(Integer inspectLevel) {
		this.inspectLevel = inspectLevel;
	}

	@Lob
	@Type(type = "text")
	@Column(name = "inspect_explain")
	public String getInspectExplain() {
		return inspectExplain;
	}

	public void setInspectExplain(String inspectExplain) {
		this.inspectExplain = inspectExplain;
	}
	
	@Lob
	@Type(type = "text")
	@Column(name = "guide_content")
	public String getGuideContent() {
		return guideContent;
	}

	public void setGuideContent(String guideContent) {
		this.guideContent = guideContent;
	}

	@Lob
	@Type(type = "text")
	@Column(name = "suggest_content")
	public String getSuggestContent() {
		return suggestContent;
	}

	public void setSuggestContent(String suggestContent) {
		this.suggestContent = suggestContent;
	}

	@Lob
	@Type(type = "text")
	@Column(name = "improvement_program")
	public String getImprovementProgram() {
		return improvementProgram;
	}

	public void setImprovementProgram(String improvementProgram) {
		this.improvementProgram = improvementProgram;
	}

	@Column(name = "type")
	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	@Column(name = "sort")
	public Integer getSorts() {
		return sorts;
	}

	public void setSorts(Integer sorts) {
		this.sorts = sorts;
	}

	@Column(name = "update_time")
	public String getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}
	
	@Transient
	public List<KangjiaKpi> getChildren() {
		return children;
	}

	public void setChildren(List<KangjiaKpi> children) {
		this.children = children;
	}
}
