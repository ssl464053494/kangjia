package com.kangjia.model.m3.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;

import com.kangjia.general.request.JsonRequest;

@Entity
@Table(name = "ts_temp")
public class Temp extends JsonRequest implements java.io.Serializable {

	private static final long serialVersionUID = 8810260501325343626L;
	private Long tempId;
	private String cname;
	private String depict;
	private String content;

	public Temp() {
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "temp_id", unique = true, nullable = false)
	public Long getTempId() {
		return tempId;
	}

	public void setTempId(Long tempId) {
		this.tempId = tempId;
	}

	@Column(name = "cname")
	public String getCname() {
		return cname;
	}

	public void setCname(String cname) {
		this.cname = cname;
	}

	@Column(name = "depict")
	public String getDepict() {
		return this.depict;
	}

	public void setDepict(String depict) {
		this.depict = depict;
	}

	@Column(name = "content")
	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

}