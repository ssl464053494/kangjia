package com.kangjia.model.m3.service;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

public class CryptUtilImpl {
	 public String cryptMd5(String source, String key) {
	        byte[] k_ipad = new byte[64];
	        byte[] k_opad = new byte[64];

	        byte[] keyb;
	        byte[] value;
	        try {
	            keyb = key.getBytes("UTF-8");
	            value = source.getBytes("UTF-8");
	        } catch (UnsupportedEncodingException var10) {
	            keyb = key.getBytes();
	            value = source.getBytes();
	        }

	        Arrays.fill(k_ipad, keyb.length, 64, (new Integer(54)).byteValue());
	        Arrays.fill(k_opad, keyb.length, 64, (new Integer(92)).byteValue());

	        for(int md = 0; md < keyb.length; ++md) {
	            k_ipad[md] = (byte)(keyb[md] ^ 54);
	            k_opad[md] = (byte)(keyb[md] ^ 92);
	        }

	        MessageDigest var11 = null;

	        try {
	            var11 = MessageDigest.getInstance("MD5");
	        } catch (NoSuchAlgorithmException var9) {
	            return null;
	        }

	        var11.update(k_ipad);
	        var11.update(value);
	        byte[] dg = var11.digest();
	        var11.reset();
	        var11.update(k_opad);
	        var11.update(dg, 0, 16);
	        dg = var11.digest();
	        return toHex(dg);
	    }

	    public static String toHex(byte[] input) {
	        if(input == null) {
	            return null;
	        } else {
	            StringBuffer output = new StringBuffer(input.length * 2);

	            for(int i = 0; i < input.length; ++i) {
	                int current = input[i] & 255;
	                if(current < 16) {
	                    output.append("0");
	                }

	                output.append(Integer.toString(current, 16));
	            }

	            return output.toString();
	        }
	    }
}
