package com.kangjia.model.m3.dao;

import java.util.List;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.kangjia.general.dao.GeneralDao;
import com.kangjia.model.m3.entity.Relation;

@Repository
public class RelationDao extends GeneralDao {

	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void batch(List<Relation> relations) {
		Session session = getSession();
		for (Relation relation : relations) {
			session.saveOrUpdate(relation);
		}
	}

}
