package com.kangjia.model.m3.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.kangjia.general.request.JsonRequest;

@Entity
@Table(name = "ts_relation")
public class Relation extends JsonRequest implements Serializable {

	private static final long serialVersionUID = 3209890347877222526L;

	private Long relationId;
	private Long indexTypeId;
	private Integer inspectStandard;
	private Integer targetId;
	private Integer classify;
	
	private List<Relation> list = new ArrayList<>();

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "relation_id", unique = true, nullable = false)
	public Long getRelationId() {
		return relationId;
	}

	public void setRelationId(Long relationId) {
		this.relationId = relationId;
	}

	@Column(name = "index_type_id")
	public Long getIndexTypeId() {
		return indexTypeId;
	}

	public void setIndexTypeId(Long indexTypeId) {
		this.indexTypeId = indexTypeId;
	}

	@Column(name = "target_id")
	public Integer getTargetId() {
		return targetId;
	}

	public void setTargetId(Integer targetId) {
		this.targetId = targetId;
	}

	@Column(name = "inspect_standard")
	public Integer getInspectStandard() {
		return inspectStandard;
	}

	public void setInspectStandard(Integer inspectStandard) {
		this.inspectStandard = inspectStandard;
	}

	@Column(name = "classify")
	public Integer getClassify() {
		return classify;
	}

	public void setClassify(Integer classify) {
		this.classify = classify;
	}

	@Transient
	public List<Relation> getList() {
		return list;
	}

	public void setList(List<Relation> list) {
		this.list = list;
	}
}
