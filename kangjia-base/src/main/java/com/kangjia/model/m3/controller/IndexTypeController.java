package com.kangjia.model.m3.controller;

import java.io.Serializable;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kangjia.general.controller.AbstractController;
import com.kangjia.general.dao.query.OperType;
import com.kangjia.general.dao.query.Page;
import com.kangjia.general.dao.query.Param;
import com.kangjia.general.dao.query.QueryParam;
import com.kangjia.jasckson.DataMapper;
import com.kangjia.model.m3.entity.IndexType;
import com.kangjia.response.AbstractResponse;
import com.kangjia.response.ObjectResponse;
import com.kangjia.response.SuccessResponse;

@Controller
@RequestMapping(value = "/b/index/type", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class IndexTypeController extends AbstractController<IndexType> {

	private static final String INDEX_TYPE = "index.type";

	@RequestMapping(value = "/editor")
	@ResponseBody
	public AbstractResponse editor(@RequestBody IndexType indexType) {
		Long indexTypeId = indexType.getIndexTypeId();
		if (indexTypeId == null) {
			indexType.setStatus(0);
			Long inc = hinc(HW_INC_KEY, INDEX_TYPE);
			indexType.setIndexCode(String.format("%05d", inc));
		}
		ObjectResponse<IndexType> response = super.editorx(indexType);
		hset("indexType", response.getData().getIndexTypeId().toString(), DataMapper.write(response.getData()));
		return new SuccessResponse("操作成功");
	}

	@RequestMapping(value = "/delete")
	@ResponseBody
	public AbstractResponse delete(@RequestBody IndexType indexType) {
		hdel("indexType", indexType.getIndexTypeId().toString());
		return super.delete(indexType.getIndexTypeId(), null);
	}

	@RequestMapping(value = "/find")
	@ResponseBody
	public AbstractResponse find(@RequestBody IndexType indexType) {
		return super.find(indexType.getIndexTypeId(), null);
	}

	@RequestMapping(value = "/search")
	@ResponseBody
	public AbstractResponse tree(@RequestBody IndexType indexType) {
		Page page = new Page();
		page.setStart(indexType.getStart());
		page.setLimit(indexType.getLimit());
		QueryParam param = new QueryParam(page);
		param.addParam(new Param<Integer>("status", OperType.eq, 0));
		if (indexType.getIndexType() != null) {
			param.addParam(new Param<Integer>("indexType", OperType.eq, indexType.getIndexType()));
		}
		return super.query(param, null);
	}

	@RequestMapping(value = "/query")
	@ResponseBody
	public AbstractResponse query(@RequestBody IndexType indexType) {
		Page page = new Page();
		QueryParam param = new QueryParam(page);
		param.addParam(new Param<Integer>("status", OperType.eq, 0));
		param.addParam(new Param<Integer>("indexType", OperType.eq, indexType.getIndexType()));
		return super.query(param, null);
	}

	public Serializable getPrimary(IndexType entity) {
		return entity.getIndexTypeId();
	}

	public Class<IndexType> getClazz() {
		return IndexType.class;
	}

	public void status(IndexType entity) {
		entity.setStatus(-1);
	}

	public String[] ignores() {
		return new String[] { "status", "indexCode", "indexType" };
	}
}
