package com.kangjia.model.m3.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import com.kangjia.general.request.JsonRequest;

@Entity
@Table(name = "ts_product")
public class Product extends JsonRequest implements Serializable {

	private static final long serialVersionUID = 1537082690175667893L;

	private Long productId;
	private Long indexTypeId;
	private String productCode;
	private Long relationId;
	private String productName;
	private String describe;
	private String mild;
	private String moderate;
	private String severe;
	private String imgUrl;
	private String url;
	private BigDecimal price = new BigDecimal(0);

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "product_id", unique = true, nullable = false)
	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	@Column(name = "index_type_id")
	public Long getIndexTypeId() {
		return indexTypeId;
	}

	public void setIndexTypeId(Long indexTypeId) {
		this.indexTypeId = indexTypeId;
	}

	@Column(name = "product_code", length = 16)
	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	@Column(name = "relation_id")
	public Long getRelationId() {
		return relationId;
	}

	public void setRelationId(Long relationId) {
		this.relationId = relationId;
	}

	@Lob
	@Type(type = "text")
	@Column(name = "product_name")
	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	@Lob
	@Type(type = "text")
	@Column(name = "`describe`")
	public String getDescribe() {
		return describe;
	}

	public void setDescribe(String describe) {
		this.describe = describe;
	}

	@Lob
	@Type(type = "text")
	@Column(name = "mild")
	public String getMild() {
		return mild;
	}

	public void setMild(String mild) {
		this.mild = mild;
	}

	@Lob
	@Type(type = "text")
	@Column(name = "moderate")
	public String getModerate() {
		return moderate;
	}

	public void setModerate(String moderate) {
		this.moderate = moderate;
	}

	@Lob
	@Type(type = "text")
	@Column(name = "severe")
	public String getSevere() {
		return severe;
	}

	public void setSevere(String severe) {
		this.severe = severe;
	}

	@Column(name = "img_url", length = 256)
	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}

	@Column(name = "url", length = 256)
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Column(name = "price", length = 10, scale = 2)
	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}
}
