package com.kangjia.model.m3.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.kangjia.general.controller.AbstractController;
import com.kangjia.model.m3.entity.KangjiaKpi;
import com.kangjia.model.m3.service.BaseDataApiImpl;
import com.kangjia.model.m3.service.KangjiaKpiService;
import com.kangjia.response.AbstractResponse;
import com.kangjia.response.PageResponse;
import com.kangjia.response.SuccessResponse;

/**
 * 康加api请求控制器类
 * 
 * @author sunshulin
 *
 */
@Controller
@RequestMapping(value = "/b/kangjiakpi", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class KangjiaApiController extends AbstractController<KangjiaKpi> {

	@Autowired
	private BaseDataApiImpl baseDataApiImpl;
	@Autowired
	private KangjiaKpiService kangjiaKpiService;

	@RequestMapping(value = "/getBaseData")
	@ResponseBody
	public AbstractResponse getBaseData() throws JsonProcessingException, IOException {
		baseDataApiImpl.getBaseData();
		return new SuccessResponse("数据同步完成");
	}

	@RequestMapping(value = "/search")
	@ResponseBody
	public KangjiaKpi tree(@RequestBody KangjiaKpi entity) {
		PageResponse<KangjiaKpi> pager = super.query();
		List<KangjiaKpi> orgs = pager.getData();
		return kangjiaKpiService.createRoot(orgs);
	}

	public Serializable getPrimary(KangjiaKpi entity) {
		return entity.getTargetId();
	}

	public Class<KangjiaKpi> getClazz() {
		return KangjiaKpi.class;
	}

	public void status(KangjiaKpi entity) {

	}
}