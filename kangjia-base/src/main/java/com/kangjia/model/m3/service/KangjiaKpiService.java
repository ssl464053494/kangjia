package com.kangjia.model.m3.service;

import org.springframework.stereotype.Service;

import com.kangjia.model.m3.entity.KangjiaKpi;
import com.kangjia.model.service.TreeService;

@Service
public class KangjiaKpiService extends TreeService<KangjiaKpi> {

	public KangjiaKpi createT() {
		return new KangjiaKpi();
	}

	public Integer getId(KangjiaKpi entity) {
		return entity.getTargetId();
	}

	public Integer getParent(KangjiaKpi entity) {
		return entity.getParentId();
	}

	public void addChildren(KangjiaKpi entity, KangjiaKpi node) {
		entity.getChildren().add(node);
	}

	public void checked(KangjiaKpi entity, boolean is) {
		entity.setChecked(is);
	}

	public void expand(KangjiaKpi entity, boolean is) {
		entity.setExpanded(is);
	}

}
