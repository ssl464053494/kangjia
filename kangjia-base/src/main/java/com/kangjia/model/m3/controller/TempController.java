package com.kangjia.model.m3.controller;

import java.io.Serializable;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kangjia.general.controller.AbstractController;
import com.kangjia.general.dao.query.Page;
import com.kangjia.general.dao.query.QueryParam;
import com.kangjia.model.m3.entity.Temp;
import com.kangjia.response.AbstractResponse;
import com.kangjia.response.SuccessResponse;

@Controller
@RequestMapping(value = "/b/temp", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class TempController extends AbstractController<Temp> {

	@RequestMapping(value = "/editor")
	@ResponseBody
	public AbstractResponse editor(@RequestBody Temp temp) {
		super.editorx(temp);
		hset("temp", temp.getTempId().toString(), temp.getContent());
		return new SuccessResponse("操作成功");
	}
	
	@RequestMapping(value = "/find")
	@ResponseBody
	public AbstractResponse find(@RequestBody Temp temp) {
		return super.find(temp.getTempId(), null);
	}
	
	@RequestMapping(value = "/search")
	@ResponseBody
	public AbstractResponse tree(@RequestBody Temp temp) {
		Page page = new Page();
		page.setStart(temp.getStart());
		page.setLimit(temp.getLimit());
		QueryParam param = new QueryParam(page);
		return super.query(param, null);
	}
	
	public Serializable getPrimary(Temp entity) {
		return entity.getTempId();
	}

	public Class<Temp> getClazz() {
		return Temp.class;
	}

	public void status(Temp entity) {

	}

}
