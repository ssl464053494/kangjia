package com.kangjia.model.m3.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Type;

import com.kangjia.general.request.JsonRequest;

@Entity
@Table(name = "ts_index_type")
public class IndexType extends JsonRequest implements Serializable {

	private static final long serialVersionUID = -609936930782550465L;

	private Long indexTypeId;
	private Integer indexType;
	private String indexCode;
	private String index;
	private String describe;
	private String proposal;
	private String personality;
	private String expression;
	private Integer status;
	
	private Long relationId;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "index_type_id", unique = true, nullable = false)
	public Long getIndexTypeId() {
		return indexTypeId;
	}

	public void setIndexTypeId(Long indexTypeId) {
		this.indexTypeId = indexTypeId;
	}

	@Column(name = "index_type")
	public Integer getIndexType() {
		return indexType;
	}

	public void setIndexType(Integer indexType) {
		this.indexType = indexType;
	}

	@Column(name = "index_code", length = 12)
	public String getIndexCode() {
		return indexCode;
	}

	public void setIndexCode(String indexCode) {
		this.indexCode = indexCode;
	}

	@Column(name = "`index`", length = 64)
	public String getIndex() {
		return index;
	}

	public void setIndex(String index) {
		this.index = index;
	}

	@Lob
	@Type(type = "text")
	@Column(name = "`describe`")
	public String getDescribe() {
		return describe;
	}

	public void setDescribe(String describe) {
		this.describe = describe;
	}

	@Lob
	@Type(type = "text")
	@Column(name = "`proposal`")
	public String getProposal() {
		return proposal;
	}

	public void setProposal(String proposal) {
		this.proposal = proposal;
	}

	@Lob
	@Type(type = "text")
	@Column(name = "`personality`")
	public String getPersonality() {
		return personality;
	}

	public void setPersonality(String personality) {
		this.personality = personality;
	}

	@Lob
	@Type(type = "text")
	@Column(name = "`expression`")
	public String getExpression() {
		return expression;
	}

	public void setExpression(String expression) {
		this.expression = expression;
	}

	@Column(name = "status")
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	@Transient
	public Long getRelationId() {
		return relationId;
	}

	public void setRelationId(Long relationId) {
		this.relationId = relationId;
	}
}
