package com.kangjia.model.m1.controller;

import java.io.Serializable;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kangjia.general.controller.AbstractController;
import com.kangjia.general.controller.call.Call;
import com.kangjia.general.dao.query.OperType;
import com.kangjia.general.dao.query.Page;
import com.kangjia.general.dao.query.Param;
import com.kangjia.general.dao.query.QueryParam;
import com.kangjia.model.m1.entity.Employee;
import com.kangjia.model.m1.service.EmployeeService;
import com.kangjia.response.AbstractResponse;
import com.kangjia.response.ErrorResponse;
import com.kangjia.response.SuccessResponse;
import com.kangjia.type.Md5;

@Controller
@RequestMapping(value = "/b/employee", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class EmployeeController extends AbstractController<Employee> {

	@Autowired
	private EmployeeService employeeService;

	@RequestMapping(value = "/editor")
	@ResponseBody
	public AbstractResponse editor(@RequestBody Employee employee) {
		Integer employeeId = employee.getEmployeeId();
		String pass = null;
		if (employeeId == null) {
			if (employeeService.exist(employee.getLogin())) {
				return new ErrorResponse("账号已存在");
			}
			employee.setStatus(0);
			employee.setFlag(0);
			pass = "hw12345";//PassWordCreate.createPassWord(6);
			employee.setPass(Md5.encrypt(pass));
		}
		super.editorx(employee);
		return new SuccessResponse("密码:" + pass);
	}

	@RequestMapping(value = "/delete")
	@ResponseBody
	public AbstractResponse delete(@RequestBody Employee employee) {
		return super.delete(employee.getEmployeeId(), null);
	}

	@RequestMapping(value = "/find")
	@ResponseBody
	public AbstractResponse find(@RequestBody Employee employee) {
		return super.find(employee.getEmployeeId(), new Call<Employee>() {
			public void call(Employee entity) {
				employeeService.getOrgAndStore(entity);
			}
		});
	}

	@RequestMapping(value = "/startAndStop")
	@ResponseBody
	public AbstractResponse startAndStop(@RequestBody Employee employee) {
		Employee entity = findById(employee.getEmployeeId(), null);
		if (entity != null) {
			if (!entity.getStatus().equals(0)) {
				return new ErrorResponse("账号不存在");
			}
			if (entity.getFlag() != null && entity.getFlag().equals(0)) {
				entity.setFlag(1);
			} else {
				entity.setFlag(0);
			}
			employeeService.update(entity);
		} else {
			return new ErrorResponse("账号不存在");
		}
		return new SuccessResponse("操作成功");
	}

	@RequestMapping(value = "/repeat")
	@ResponseBody
	public AbstractResponse repeat(@RequestBody Employee employee) {
		Employee entity = findById(employee.getEmployeeId(), null);
		String pass = null;
		if (entity != null) {
			if (!entity.getStatus().equals(0)) {
				return new ErrorResponse("账号不存在");
			}
			if (entity.getFlag() != null && entity.getFlag().equals(0)) {
				pass = "hw12345";//PassWordCreate.createPassWord(6);
				entity.setPass(Md5.encrypt(pass));
				employeeService.update(entity);
			} else {
				return new ErrorResponse("账号已禁用");
			}
		} else {
			return new ErrorResponse("账号不存在");
		}
		return new SuccessResponse("密码:" + pass);
	}

	@RequestMapping(value = "/pass")
	@ResponseBody
	public AbstractResponse pass(@RequestBody Employee employee) {
		Employee entity = findById(employee.getEmployeeId(), null);
		String pass = null;
		if (entity != null) {
			if (!entity.getStatus().equals(0)) {
				return new ErrorResponse("账号不存在");
			}
			if (entity.getFlag() != null && entity.getFlag().equals(0)) {
				if (!employee.getNewpass().equals(employee.getRepeatpass())) {
					return new ErrorResponse("两次输入密码不一致");
				}
				if (!Md5.encrypt(employee.getOldpass()).equalsIgnoreCase(entity.getPass())) {
					return new ErrorResponse("密码不正确");
				}
				entity.setPass(Md5.encrypt(employee.getNewpass()));
				employeeService.update(entity);
			} else {
				return new ErrorResponse("账号已禁用");
			}
		} else {
			return new ErrorResponse("账号不存在");
		}
		return new SuccessResponse("密码:" + pass);
	}

	@RequestMapping(value = "/search")
	@ResponseBody
	public AbstractResponse tree(@RequestBody Employee employee) {
		Page page = new Page();
		page.setStart(employee.getStart());
		page.setLimit(employee.getLimit());
		QueryParam param = new QueryParam(page);
		param.addParam(new Param<Integer>("status", OperType.eq, 0));
		param.addParam(new Param<Integer>("employeeId", OperType.nq, 1));
		param.addParam(new Param<Integer>("employeeId", OperType.nq, 2));
		setting(param);
		if (StringUtils.isNoneBlank(employee.getEmployeeName())) {
			param.addParam(new Param<String>("employeeName", OperType.like, employee.getEmployeeName()));
		}
		if (employee.getRoleId() != null) {
			param.addParam(new Param<Integer>("roleId", OperType.eq, employee.getRoleId()));
		}
		if (employee.getStoreId() != null) {
			param.addParam(new Param<Integer>("storeId", OperType.eq, employee.getStoreId()));
		}
		if (employee.getFlag() != null) {
			param.addParam(new Param<Integer>("flag", OperType.eq, employee.getFlag()));
		}
		return super.query(param, new Call<Employee>() {
			public void call(Employee entity) {
				employeeService.getOrgAndStore(entity);
			}
		});
	}
	
	public void setting(QueryParam param) {
		Employee employee = getEmployee();
		param.addParam(new Param<Integer>("employeeId", OperType.nq, employee.getEmployeeId()));
		if (employee.getStoreId() != null) {
			param.addParam(new Param<Integer>("storeId", OperType.eq, employee.getStoreId()));
		}
	}

	public Serializable getPrimary(Employee entity) {
		return entity.getEmployeeId();
	}

	public Class<Employee> getClazz() {
		return Employee.class;
	}

	public String[] ignores() {
		return new String[] { "status", "pass", "login", "flag" };
	}

	public void status(Employee entity) {
		entity.setStatus(-1);
	}

}
