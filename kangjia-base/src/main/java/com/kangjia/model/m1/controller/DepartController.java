package com.kangjia.model.m1.controller;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kangjia.general.controller.AbstractController;
import com.kangjia.general.dao.query.OperType;
import com.kangjia.general.dao.query.Param;
import com.kangjia.general.dao.query.QueryParam;
import com.kangjia.model.m1.entity.Depart;
import com.kangjia.model.m1.service.DepartService;
import com.kangjia.response.AbstractResponse;
import com.kangjia.response.PageResponse;

@Controller
@RequestMapping(value = "/b/depart", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class DepartController extends AbstractController<Depart> {

	@Autowired
	private DepartService departService;

	@RequestMapping(value = "/editor")
	@ResponseBody
	public AbstractResponse editor(@RequestBody Depart depart) {
		if (depart.getDeptId() == null) {
			depart.setStatus(0);
		}
		departService.createDepartCode(depart);
		return super.editorx(depart);
	}

	@RequestMapping(value = "/delete")
	@ResponseBody
	public AbstractResponse delete(@RequestBody Depart depart) {
		return super.delete(depart.getDeptId(), null);
	}

	@RequestMapping(value = "/find")
	@ResponseBody
	public AbstractResponse find(@RequestBody Depart depart) {
		return super.find(depart.getDeptId(), null);
	}

	@RequestMapping(value = "/search")
	@ResponseBody
	public Depart tree(@RequestBody Depart depart) {
		QueryParam params = new QueryParam();
		
		params.addParam(new Param<Integer>("status", OperType.eq, 0));
		PageResponse<Depart> pager = super.query(params);
		List<Depart> departs = pager.getData();
		return departService.createRoot(departs);
	}

	public Serializable getPrimary(Depart entity) {
		return entity.getDeptId();
	}

	public Class<Depart> getClazz() {
		return Depart.class;
	}

	public String[] ignores() {
		return new String[] { "parentId", "code", "orgId", "status" };
	}

	public void status(Depart entity) {
		entity.setStatus(-1);
	}

}
