package com.kangjia.model.m1.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kangjia.general.dao.GeneralDao;
import com.kangjia.model.m1.entity.Depart;
import com.kangjia.model.service.TreeService;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

@Service
public class DepartService extends TreeService<Depart> {
	
	@Autowired
	protected JedisPool jedisPool;
	@Autowired
	private GeneralDao generalDao;
	
	public void createDepartCode(Depart dept) {
		Integer orgId = dept.getOrgId();
		String fp = null;
		String preffix = "dept." + orgId;
		if (dept.getDeptId() == null) {
			if (dept.getParentId() != null) {
				Depart entity = generalDao.get(Depart.class, dept.getParentId());
				if (entity != null) {
					preffix += "," + entity.getCode();
					fp = entity.getCode();
				}				
			}
			Jedis jedis = jedisPool.getResource();
			try {
				Long l = jedis.hincrBy("key.dept.code", preffix, 1);
				String s = String.format("%03d", l);
				fp = (fp != null ? fp + s : s);
			} finally {
				if (jedis != null) {
					jedis.close();
				}
			}
			dept.setCode(fp);
		}
	}

	public Depart createT() {
		return new Depart();
	}

	public Integer getId(Depart entity) {
		return entity.getDeptId();
	}

	public Integer getParent(Depart entity) {
		return entity.getParentId();
	}

	public void addChildren(Depart entity, Depart node) {
		entity.getChildren().add(node);
	}

	public void checked(Depart entity, boolean is) {
		entity.setChecked(is);
	}

	public void expand(Depart entity, boolean is) {
		entity.setExpanded(is);
	}

}
