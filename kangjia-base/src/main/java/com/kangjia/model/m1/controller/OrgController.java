package com.kangjia.model.m1.controller;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kangjia.general.controller.AbstractController;
import com.kangjia.general.dao.query.OperType;
import com.kangjia.general.dao.query.Param;
import com.kangjia.general.dao.query.QueryParam;
import com.kangjia.model.m1.entity.Org;
import com.kangjia.model.m1.service.OrgService;
import com.kangjia.response.AbstractResponse;
import com.kangjia.response.PageResponse;

@Controller
@RequestMapping(value = "/b/org", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class OrgController extends AbstractController<Org> {

	@Autowired
	private OrgService orgService;

	@RequestMapping(value = "/editor")
	@ResponseBody
	public AbstractResponse editor(@RequestBody Org org) {
		if (org.getOrgId() == null) {
			org.setStatus(0);
		}
		orgService.createOrgCode(org);
		return super.editorx(org);
	}

	@RequestMapping(value = "/delete")
	@ResponseBody
	public AbstractResponse delete(@RequestBody Org org) {
		return super.delete(org.getOrgId(), null);
	}

	@RequestMapping(value = "/find")
	@ResponseBody
	public AbstractResponse find(@RequestBody Org org) {
		return super.find(org.getOrgId(), null);
	}

	@RequestMapping(value = "/search")
	@ResponseBody
	public Org tree(@RequestBody Org org) {
		QueryParam params = new QueryParam();
		params.addParam(new Param<Integer>("status", OperType.eq, 0));
		settingOrg(params);
		PageResponse<Org> pager = super.query(params);
		List<Org> orgs = pager.getData();
		return orgService.createRoot(orgs);
	}

	public Serializable getPrimary(Org entity) {
		return entity.getOrgId();
	}

	public Class<Org> getClazz() {
		return Org.class;
	}

	public String[] ignores() {
		return new String[] { "parentId", "code", "status" };
	}

	public void status(Org entity) {
		entity.setStatus(-1);
	}
}
