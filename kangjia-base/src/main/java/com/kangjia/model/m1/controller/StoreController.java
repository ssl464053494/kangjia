package com.kangjia.model.m1.controller;

import java.io.Serializable;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kangjia.general.controller.AbstractController;
import com.kangjia.general.dao.query.OperType;
import com.kangjia.general.dao.query.Page;
import com.kangjia.general.dao.query.Param;
import com.kangjia.general.dao.query.QueryParam;
import com.kangjia.jasckson.DataMapper;
import com.kangjia.model.m1.entity.Employee;
import com.kangjia.model.m1.entity.Store;
import com.kangjia.response.AbstractResponse;
import com.kangjia.response.ObjectResponse;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

@Controller
@RequestMapping(value = "/b/store", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class StoreController extends AbstractController<Store> {

	@Autowired
	protected JedisPool jedisPool;
	
	@RequestMapping(value = "/editor")
	@ResponseBody
	public AbstractResponse editor(@RequestBody Store store) {
		if (store.getStoreId() == null) {
			store.setStatus(0);
		}
		ObjectResponse<Store> response = super.editorx(store);
		Jedis jedis = jedisPool.getResource();
		try {
			jedis.hset("store", response.getData().getStoreId().toString(), DataMapper.write(response.getData()));
		} finally {
			jedis.close();
		}
		return response;
	}

	@RequestMapping(value = "/delete")
	@ResponseBody
	public AbstractResponse delete(@RequestBody Store store) {
		AbstractResponse response = super.delete(store.getStoreId(), null);
		Jedis jedis = jedisPool.getResource();
		try {
			jedis.hdel("store", store.getStoreId().toString());
		} finally {
			jedis.close();
		}
		return response;
	}

	@RequestMapping(value = "/find")
	@ResponseBody
	public AbstractResponse find(@RequestBody Store store) {
		return super.find(store.getStoreId(), null);
	}

	@RequestMapping(value = "/search")
	@ResponseBody
	public AbstractResponse tree(@RequestBody Store store) {
		Page page = new Page();
		page.setStart(store.getStart());
		page.setLimit(store.getLimit());
		QueryParam param = new QueryParam(page);
		setting(param);
		param.addParam(new Param<Integer>("status", OperType.eq, 0));
		if (StringUtils.isNoneBlank(store.getCname())) {
			param.addParam(new Param<String>("cname", OperType.like, store.getCname()));
		}
		return super.query(param);
	}

	public Serializable getPrimary(Store entity) {
		return entity.getStoreId();
	}

	public Class<Store> getClazz() {
		return Store.class;
	}

	public String[] ignores() {
		return new String[] { "status" };
	}

	public void status(Store entity) {
		entity.setStatus(-1);
	}

	public void setting(QueryParam param) {
		Employee employee = getEmployee();
		if (employee.getBusId().equals(1) || employee.getBusId().equals(0)) {
			param.addParam(new Param<Integer>("storeId", OperType.eq, employee.getStoreId()));			
		} else if (employee.getBusId().equals(2)) {
			
		} else {
			throw new RuntimeException("未知");
		}
	}
}
