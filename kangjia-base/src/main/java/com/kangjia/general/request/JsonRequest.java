package com.kangjia.general.request;

import javax.persistence.Transient;

import com.kangjia.general.dao.query.Page;

public class JsonRequest {

	/** grid,treegrid页号 */
	private Integer page;
	/** grid,treegrid开始 */
	private Integer start;
	/** grid,treegrid结果数量 */
	private Integer limit;
	/** combox查询参数 */
	private String query;
	/** grid,treegrid总记录数 */
	private Integer total;
	/**  */
	private String sort;
	/** treegrid异步加载需要用到 */
	private String node;
	/** treegrid显示名称 */
	private String text;
	/** treegrid复选框 */
	private Boolean checked;
	
	private Boolean expanded;

	@Transient
	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

	@Transient
	public Integer getStart() {
		return start;
	}

	public void setStart(Integer start) {
		this.start = start;
	}

	@Transient
	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	@Transient
	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	@Transient
	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	@Transient
	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	@Transient
	public String getNode() {
		return node;
	}

	public void setNode(String node) {
		this.node = node;
	}

	@Transient
	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	@Transient
	public Boolean getChecked() {
		return checked;
	}

	public void setChecked(Boolean checked) {
		this.checked = checked;
	}

	public Page createPage() {
		Page p = new Page();
		p.setStart(start);
		p.setLimit(limit == null ? 30 : limit);
		p.setCurrent(page);
		return p;
	}

	@Transient
	public Boolean getExpanded() {
		return expanded;
	}

	public void setExpanded(Boolean expanded) {
		this.expanded = expanded;
	}

	public String toString() {
		return "JsonRequest [page=" + page + ", start=" + start + ", limit=" + limit + ", query=" + query + ", total="
				+ total + ", sort=" + sort + ", node=" + node + ", text=" + text + ", checked=" + checked + "]";
	}
}
