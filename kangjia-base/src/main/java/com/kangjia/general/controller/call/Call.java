package com.kangjia.general.controller.call;

public interface Call<T> {

	public void call(T entity);
}
