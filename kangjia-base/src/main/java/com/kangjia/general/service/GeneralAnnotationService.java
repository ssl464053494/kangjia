package com.kangjia.general.service;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.kangjia.general.dao.GeneralAnnotationDao;
import com.kangjia.general.dao.query.QueryParam;

@Repository
public class GeneralAnnotationService<T> {

	@Autowired
	private GeneralAnnotationDao<T> dao;

	public void saveOrUpdate(T entity) {
		dao.saveOrUpdate(entity);
	}

	public T get(Class<T> clazz, Serializable id) {
		return dao.get(clazz, id);
	}

	public T load(Class<T> clazz, Serializable id) {
		return dao.load(clazz, id);
	}

	public void delete(Class<T> clazz, Serializable id) {
		dao.delete(clazz, id);
	}

	public List<T> pager(Class<T> clazz, QueryParam queryParam) {
		return dao.query(clazz, queryParam);
	}
}
