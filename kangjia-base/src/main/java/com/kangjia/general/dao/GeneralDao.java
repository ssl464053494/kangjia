package com.kangjia.general.dao;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.kangjia.general.dao.query.QueryParam;

@Repository
public class GeneralDao extends AbstractDao {

	/**
	 * 新增/更新
	 * 
	 * @param entity
	 */
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public <T> void saveOrUpdate(T entity) {
		Session session = getSession();
		session.saveOrUpdate(entity);
	}

	/**
	 * 查找,没有返回null
	 * 
	 * @param clazz
	 * @param id
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public <T> T get(Class<T> clazz, Serializable id) {
		Session session = getSession();
		return (T) session.get(clazz, id);
	}

	/**
	 * 查找,没有抛异常
	 * 
	 * @param clazz
	 * @param id
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public <T> T load(Class<T> clazz, Serializable id) {
		Session session = getSession();
		return (T) session.load(clazz, id);
	}

	/**
	 * 删除
	 * 
	 * @param clazz
	 * @param id
	 */
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public <T> void delete(Class<T> clazz, Serializable id) {
		Session session = getSession();
		T entity = get(clazz, id);
		if (entity != null) {
			session.delete(entity);
		}
	}

	/**
	 * 单表查询
	 * 
	 * @param clazz
	 * @param queryParam
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public <T> List<T> query(Class<T> clazz, QueryParam queryParam) {
		Criteria criteria = getSession().createCriteria(clazz);
		if (queryParam != null) {
			builderCondition(criteria, queryParam.getParams());
			if (queryParam.isPager()) {
				queryParam.setTotal(getTotal(criteria));
				criteria.setProjection(null);
				criteria.setFirstResult(queryParam.getStart());
				criteria.setMaxResults(queryParam.getLimit());
			}
		}
		return criteria.list();
	}
	
	/**
	 * 单表查询
	 * 
	 * @param clazz
	 * @param queryParam
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public <T> T sign(Class<T> clazz, QueryParam queryParam) {
		Criteria criteria = getSession().createCriteria(clazz);
		if (queryParam != null) {
			builderCondition(criteria, queryParam.getParams());
		}
		return (T) criteria.uniqueResult();
	}

	/**
	 * 单表统计记录数
	 * 
	 * @param clazz
	 * @param queryParam
	 * @return
	 */
	@Transactional(readOnly = true)
	public <T> Integer count(Class<T> clazz, QueryParam queryParam) {
		Criteria criteria = getSession().createCriteria(clazz);
		if (queryParam != null) {
			builderCondition(criteria, queryParam.getParams());
		}
		return getTotal(criteria);
	}
}
