package com.kangjia.general.dao.call;

import org.hibernate.Query;

public interface QueryCall {

	public void query(Query query);
}
