package com.kangjia.general.dao.query;

public enum OperType {

	eq(1, "=?"), le(2, "<=?"), ge(3, ">=?"), lt(4, "<?"), gt(5, ">?"), nq(6, "!=?"), isnull(7, "is null"), notnull(8,
			"not null"), like(9, "like ?"), in(10, "in"), notin(11, "not in"), between(12, "between and ");

	private Integer type;
	private String value;

	private OperType(Integer type, String value) {
		this.type = type;
		this.value = value;
	}

	public Integer getType() {
		return type;
	}

	public String getValue() {
		return value;
	}
}
