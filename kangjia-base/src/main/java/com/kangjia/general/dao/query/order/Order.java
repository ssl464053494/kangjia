package com.kangjia.general.dao.query.order;

public class Order {

	private String name;

	private OrderType orderType;

	public Order() {
		super();
	}

	public Order(String name, OrderType orderType) {
		super();
		this.name = name;
		this.orderType = orderType;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public OrderType getOrderType() {
		return orderType;
	}

	public void setOrderType(OrderType orderType) {
		this.orderType = orderType;
	}
}
