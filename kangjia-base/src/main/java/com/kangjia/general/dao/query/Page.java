package com.kangjia.general.dao.query;

public class Page {

	private Integer start = 0;

	private Integer current = 1;

	private Integer limit = 30;

	private Integer page;

	private Integer total;

	public Integer getStart() {
		return start;
	}

	public void setStart(Integer start) {
		this.start = start;
	}

	public Integer getCurrent() {
		return current;
	}

	public void setCurrent(Integer current) {
		this.current = current;
	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	public String toString() {
		return "Page [start=" + start + ", current=" + current + ", limit=" + limit + ", page=" + page + ", total="
				+ total + "]";
	}
}
