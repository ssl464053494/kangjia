package com.kangjia.general.dao.call;

import org.hibernate.SQLQuery;

public interface SqlExecuteUpdateCall {

	public void call(SQLQuery sqlQuery);
}
