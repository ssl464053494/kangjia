package com.kangjia.general.dao.query;

import java.util.ArrayList;
import java.util.List;

import com.kangjia.general.dao.query.order.Order;

public class QueryParam {

	private List<Param<?>> params = new ArrayList<>();

	private Page page;

	private List<Order> orders = new ArrayList<>();
	
	public QueryParam() {
		super();
	}

	public QueryParam(Page page) {
		super();
		this.page = page;
	}

	public void addParam(Param<?> param) {
		params.add(param);
	}
	
	public void addOrder(Order order) {
		orders.add(order);
	}

	public boolean isPager() {
		return page != null;
	}

	public void setTotal(Integer total) {
		if (page != null) {
			page.setTotal(total);
		}
	}
	
	public Integer getTotal() {
		if (page != null) {
			return page.getTotal();
		} else {
			return null;
		}
	}

	public Integer getStart() {
		if (page != null && page.getStart() != null) {
			return page.getStart();
		}
		return 0;
	}

	public Integer getLimit() {
		if (page != null && page.getLimit() != null) {
			return page.getLimit();
		}
		return 0;
	}

	public List<Param<?>> getParams() {
		return params;
	}
	
	public List<Order> getOrders() {
		return orders;
	}

	public String toString() {
		return "QueryParam [params=" + params + ", page=" + page + "]";
	}
}
