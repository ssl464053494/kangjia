package com.kangjia.general.dao.query;

public class Param<T> {

	private String name;

	private OperType oper;

	private T val;

	public Param(String name, OperType oper, T val) {
		super();
		this.name = name;
		this.oper = oper;
		this.val = val;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public OperType getOper() {
		return oper;
	}

	public void setOper(OperType oper) {
		this.oper = oper;
	}

	public T getVal() {
		return val;
	}

	public void setVal(T val) {
		this.val = val;
	}

	public String toString() {
		return "Param [name=" + name + ", oper=" + oper + ", val=" + val + "]";
	}
}
