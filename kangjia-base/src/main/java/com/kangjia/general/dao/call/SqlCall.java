package com.kangjia.general.dao.call;

import org.hibernate.SQLQuery;

public interface SqlCall {

	public void sqlQuery(SQLQuery sqlQuery);
}
