package com.kangjia.http;

import lombok.Data;

@Data
public class HttpResponse {

	private int rpc;
	private String response;

	public HttpResponse(int rpc, String response) {
		super();
		this.rpc = rpc;
		this.response = response;
	}

	public HttpResponse() {
		super();
	}

	public HttpResponse(int rpc) {
		super();
		this.rpc = rpc;
	}
}
