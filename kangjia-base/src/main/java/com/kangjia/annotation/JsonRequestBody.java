package com.kangjia.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface JsonRequestBody {

	boolean required() default true;
	
	boolean validator() default true;
	
	String ref() default "";
	
	String rmethod() default "execute";
	
	Class<?> clazz();
	
	String cmethod() default "execute";
	
	String path() default "";
}
