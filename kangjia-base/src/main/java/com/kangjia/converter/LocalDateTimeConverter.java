package com.kangjia.converter;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.format.FormatterRegistry;

public class LocalDateTimeConverter {
	
	public static void addConverter(FormatterRegistry registry) {
		registry.addConverter(new LocalDateTimeToString());
		registry.addConverter(new StringToLocalDateTime());
	}

	
	private static class LocalDateTimeToString implements Converter<LocalDateTime, String> {

		public String convert(LocalDateTime source) {
			return source.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
		}
	}
	
	private static class StringToLocalDateTime implements Converter<String, LocalDateTime> {

		public LocalDateTime convert(String source) {
			return LocalDateTime.parse(source, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
		}
	}
}
