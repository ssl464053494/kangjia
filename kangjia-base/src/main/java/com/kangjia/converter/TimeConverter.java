package com.kangjia.converter;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.format.FormatterRegistry;

public class TimeConverter {

	public static void addConverter(FormatterRegistry registry) {
		registry.addConverter(new LocalTimeToString());
		registry.addConverter(new StringToLocalTime());
	}

	
	private static class LocalTimeToString implements Converter<LocalTime, String> {

		public String convert(LocalTime source) {
			return source.format(DateTimeFormatter.ofPattern("HH:mm:ss"));
		}
	}
	
	private static class StringToLocalTime implements Converter<String, LocalTime> {

		public LocalTime convert(String source) {
			return LocalTime.parse(source, DateTimeFormatter.ofPattern("HH:mm:ss"));
		}
	}
}
