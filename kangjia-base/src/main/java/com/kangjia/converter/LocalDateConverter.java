package com.kangjia.converter;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.format.FormatterRegistry;

public class LocalDateConverter {
	
	public static void addConverter(FormatterRegistry registry) {
		registry.addConverter(new LocalDateToString());
		registry.addConverter(new StringToLocalDate());
	}

	private static class LocalDateToString implements Converter<LocalDate, String> {

		public String convert(LocalDate source) {
			return source.format(DateTimeFormatter.ISO_LOCAL_DATE);
		}
	}
	
	private static class StringToLocalDate implements Converter<String, LocalDate> {

		public LocalDate convert(String source) {
			return LocalDate.parse(source);
		}
	}
}
