package com.kangjia.converter;

import java.math.BigDecimal;

import org.springframework.core.convert.converter.Converter;
import org.springframework.format.FormatterRegistry;

import com.kangjia.type.Decimal;

public class BigDecimalConverter {

	public static void addConverter(FormatterRegistry registry) {
		registry.addConverter(new BigDecimalToString());
		registry.addConverter(new StringToBigDecimal());
	}

	private static class BigDecimalToString implements Converter<BigDecimal, String> {

		public String convert(BigDecimal source) {
			return Decimal.convert(source, Decimal.DECIMAL_DIGITS_HALF_UP, Decimal.DECIMAL_DIGITS_LENGTH)
					.toEngineeringString();
		}
	}

	private static class StringToBigDecimal implements Converter<String, BigDecimal> {

		public BigDecimal convert(String source) {
			return new BigDecimal(source);
		}
	}
}
