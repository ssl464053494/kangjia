package com.kangjia.resolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

/**
 * 自定义异常解析器将异常信息转发到/error请求
 * 
 * @author sunshulin
 *
 */
public class HandlerExceptionResolverImpl implements HandlerExceptionResolver {

	public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response, Object handler,
			Exception ex) {
		ModelAndView mav = new ModelAndView("forward:/error");
		mav.addObject("msg", ex.getMessage());
		mav.addObject("ex", ex);
		return mav;
	}

}
