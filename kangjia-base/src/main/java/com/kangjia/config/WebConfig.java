package com.kangjia.config;

import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.http.converter.xml.MappingJackson2XmlHttpMessageConverter;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import com.kangjia.converter.BigDecimalConverter;
import com.kangjia.converter.DateConverter;
import com.kangjia.converter.LocalDateConverter;
import com.kangjia.converter.LocalDateTimeConverter;
import com.kangjia.converter.TimeConverter;
import com.kangjia.filter.CommonFilter;
import com.kangjia.filter.LoginFilter;
import com.kangjia.holder.ContextHolder;
import com.kangjia.jasckson.DataMapper;
import com.kangjia.jasckson.XmlDataMapper;
import com.kangjia.resolver.ValidatorRequestResponseBodyMethodProcessor;
import com.squareup.okhttp.OkHttpClient;

import redis.clients.jedis.JedisPool;

@Configuration
public class WebConfig extends WebMvcConfigurationSupport {

	private List<HttpMessageConverter<?>> converters;
	

	@Bean
	public FilterRegistrationBean filterRegistrationBean() {
		FilterRegistrationBean registrationBean = new FilterRegistrationBean();
		registrationBean.setFilter(new CommonFilter());
		List<String> urlPatterns = new ArrayList<String>();
		urlPatterns.add("/b/*");
		urlPatterns.add("/a/*");
		registrationBean.setUrlPatterns(urlPatterns);
		return registrationBean;
	}

	@Bean
	public FilterRegistrationBean loginFilter(JedisPool jedisPool) {
		FilterRegistrationBean registrationBean = new FilterRegistrationBean();
		registrationBean.setFilter(new LoginFilter(jedisPool));
		List<String> urlPatterns = new ArrayList<String>();
		urlPatterns.add("/b/*");
		registrationBean.setUrlPatterns(urlPatterns);
		return registrationBean;
	}

	@Bean
	public OkHttpClient okHttpClient() {
		OkHttpClient okHttpClient = new OkHttpClient();
		return okHttpClient;
	}

	@Bean
	public ContextHolder contextHolder() {
		return new ContextHolder();
	}

	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/index.html").addResourceLocations("classpath:/html/");
		registry.addResourceHandler("/app/**").addResourceLocations("classpath:/app/");
		registry.addResourceHandler("/assets/**").addResourceLocations("classpath:/assets/");
		registry.addResourceHandler("/html/**").addResourceLocations("classpath:/html/");
		super.addResourceHandlers(registry);
	}

	public void addFormatters(FormatterRegistry registry) {
		DateConverter.addConverter(registry);
		LocalDateConverter.addConverter(registry);
		LocalDateTimeConverter.addConverter(registry);
		BigDecimalConverter.addConverter(registry);
		TimeConverter.addConverter(registry);
	}

	public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
		MappingJackson2XmlHttpMessageConverter jackson2xml = new MappingJackson2XmlHttpMessageConverter(
				XmlDataMapper.getInstance());
		MappingJackson2HttpMessageConverter jackson2 = new MappingJackson2HttpMessageConverter(
				DataMapper.getInstance());
		StringHttpMessageConverter stringConverter = new StringHttpMessageConverter();
		stringConverter.setWriteAcceptCharset(false);
		converters.add(new ByteArrayHttpMessageConverter());
		converters.add(stringConverter);
		converters.add(jackson2);
		converters.add(jackson2xml);
		this.converters = converters;
	}

	public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
		argumentResolvers
				.add(new ValidatorRequestResponseBodyMethodProcessor(this.converters, mvcContentNegotiationManager()));
	}
}
