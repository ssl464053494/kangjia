package com.kangjia.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DomainConfig {

	@Value("${hibernate.dialect}")
	private String domain;
}
