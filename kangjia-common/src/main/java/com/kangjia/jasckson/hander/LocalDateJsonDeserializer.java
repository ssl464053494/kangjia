package com.kangjia.jasckson.hander;

import java.io.IOException;
import java.time.LocalDate;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

/**
 * json转换日期实现类
 * 
 * @author sshl
 * 
 */
public class LocalDateJsonDeserializer extends JsonDeserializer<LocalDate> {

	public LocalDate deserialize(JsonParser parser, DeserializationContext ctx)
			throws IOException, JsonProcessingException {
		String value = parser.getText();
		if (StringUtils.isEmpty(value)) {
			return null;
		}
		return LocalDate.parse(value);
	}

}
