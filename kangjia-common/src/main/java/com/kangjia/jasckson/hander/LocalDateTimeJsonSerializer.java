package com.kangjia.jasckson.hander;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

/**
 * 日期转换json实现类
 * 
 * @author sshl
 * 
 */
public class LocalDateTimeJsonSerializer extends JsonSerializer<LocalDateTime> {

	public void serialize(LocalDateTime date, JsonGenerator jgen,
			SerializerProvider provider) throws IOException,
			JsonProcessingException {
		if (date == null) {
			return;
		}
		jgen.writeString(date.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
	}

}
