package com.kangjia.jasckson.hander;

import java.io.IOException;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

/**
 * json转换日期实现类
 * 
 * @author sshl
 * 
 */
public class LocalTimeJsonDeserializer extends JsonDeserializer<LocalTime> {

	public LocalTime deserialize(JsonParser parser, DeserializationContext ctx)
			throws IOException, JsonProcessingException {
		String value = parser.getText();
		if (StringUtils.isEmpty(value)) {
			return null;
		}
		return LocalTime.parse(value, DateTimeFormatter.ofPattern("HH:mm:ss"));
	}

}
