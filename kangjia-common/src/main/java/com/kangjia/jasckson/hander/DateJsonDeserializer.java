package com.kangjia.jasckson.hander;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

/**
 * json转换日期实现类
 * 
 * @author sshl
 * 
 */
public class DateJsonDeserializer extends JsonDeserializer<Date> {

	public Date deserialize(JsonParser parser, DeserializationContext ctx)
			throws IOException, JsonProcessingException {
		String value = parser.getText();
		if (StringUtils.isEmpty(value)) {
			return null;
		}
		SimpleDateFormat sdf = null;
		if (value.length() == 10) {
			sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		} else {
			sdf = new SimpleDateFormat("yyyy-MM-dd");
		}
		Date date = null;
		try {
			date = sdf.parse(value);
		} catch (ParseException e) {

		}
		return date;
	}

}
