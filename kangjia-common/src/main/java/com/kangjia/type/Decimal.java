package com.kangjia.type;

import java.math.BigDecimal;

public class Decimal {

	/**
	 * 全部舍弃
	 */
	public static final String DECIMAL_DIGITS_DOWN = "DOWN";

	/**
	 * 全部进位
	 */
	public static final String DECIMAL_DIGITS_UP = "UP";

	/**
	 * 五舍六入
	 */
	public static final String DECIMAL_DIGITS_HALF_DOWN = "HALF_DOWN";
	/**
	 * 四舍五入
	 */
	public static final String DECIMAL_DIGITS_HALF_UP = "HALF_UP";

	/**
	 * 小数位数
	 */
	public static final int DECIMAL_DIGITS_LENGTH = 2;

	/**
	 * 科学计数法
	 * 
	 * @param value
	 * @param scale
	 * @param decimalDigits
	 *            小数位数
	 * @return
	 */
	public static BigDecimal convert(BigDecimal value, String scale, int decimalDigits) {
		if (scale.equalsIgnoreCase(DECIMAL_DIGITS_HALF_UP)) {
			return value.setScale(decimalDigits, BigDecimal.ROUND_HALF_UP);
		} else if (scale.equalsIgnoreCase(DECIMAL_DIGITS_HALF_DOWN)) {
			return value.setScale(decimalDigits, BigDecimal.ROUND_HALF_DOWN);
		} else if (scale.equalsIgnoreCase(DECIMAL_DIGITS_UP)) {
			return value.setScale(decimalDigits, BigDecimal.ROUND_UP);
		} else if (scale.equalsIgnoreCase(DECIMAL_DIGITS_DOWN)) {
			return value.setScale(decimalDigits, BigDecimal.ROUND_DOWN);
		}
		return value.setScale(decimalDigits, BigDecimal.ROUND_HALF_UP);
	}
}
