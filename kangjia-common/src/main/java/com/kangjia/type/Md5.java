package com.kangjia.type;

import java.security.MessageDigest;

public class Md5 {
	
	private static char md5String[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e',
			'f' };

	public static String encrypt(String str) {
		try {
			byte[] btInput = str.getBytes();
			MessageDigest mdInst = MessageDigest.getInstance("MD5");
			mdInst.update(btInput);
			byte[] md = mdInst.digest();
			int j = md.length;
			char chars[] = new char[j * 2];
			int k = 0;
			for (int i = 0; i < j; i++) { // i = 0
				byte byte0 = md[i]; // 95
				chars[k++] = md5String[byte0 >>> 4 & 0xf]; // 5
				chars[k++] = md5String[byte0 & 0xf]; // F
			}
			return new String(chars);
		} catch (Exception e) {
			return null;
		}
	}
}
