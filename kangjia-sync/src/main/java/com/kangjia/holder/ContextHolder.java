package com.kangjia.holder;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.ListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public class ContextHolder implements ApplicationContextAware {

	private static ApplicationContext context;

	public void setApplicationContext(ApplicationContext context) throws BeansException {
		ContextHolder.context = context;
	}

	public static Object getBean(String name) {
		return ContextHolder.context.getBean(name);
	}

	public static Object getBean(Class<?> clazz) {
		return ContextHolder.context.getBean(clazz);
	}

	public static ListableBeanFactory getContext() {
		return ContextHolder.context;
	}
}
