package com.kangjia.holder;

public class TableContextHolder {

	private String targetTable;

	private String tempTable;

	public static final String persion = "ts_persion";
	public static final String store = "ts_store";

	private TableContextHolder(String targetTable, Long current) {
		this.targetTable = targetTable;
		this.tempTable = targetTable;
		if (current != null) {
			this.tempTable = targetTable + current;
		}
	}

	public static void mod(String targetTable, Long molecule, Long denominator) {
		Long current = null;
		if (molecule > 0) {
			current = molecule % denominator;
		}
		TableContextHolder holder = new TableContextHolder(targetTable, current);
		VariableThreadLocal.set("TableContextHolder", holder);
	}

	public String getTargetTable() {
		return targetTable;
	}

	public String getTempTable() {
		return tempTable;
	}

}
