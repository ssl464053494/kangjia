package com.kangjia.config;

import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.kangjia.filter.CommonFilter;
import com.kangjia.filter.LoginFilter;

import redis.clients.jedis.JedisPool;

@Configuration
public class WebConfig {

	@Bean
	public FilterRegistrationBean filterRegistrationBean() {
		FilterRegistrationBean registrationBean = new FilterRegistrationBean();
		registrationBean.setFilter(new CommonFilter());
		List<String> urlPatterns = new ArrayList<String>();
		urlPatterns.add("/b/*");
		registrationBean.setUrlPatterns(urlPatterns);
		return registrationBean;
	}

	@Bean
	public FilterRegistrationBean loginFilter(JedisPool jedisPool) {
		FilterRegistrationBean registrationBean = new FilterRegistrationBean();
		registrationBean.setFilter(new LoginFilter(jedisPool));
		List<String> urlPatterns = new ArrayList<String>();
		urlPatterns.add("/b/*");
		registrationBean.setUrlPatterns(urlPatterns);
		return registrationBean;
	}
}
