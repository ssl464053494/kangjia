package com.kangjia.config;

import java.util.concurrent.TimeUnit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.squareup.okhttp.OkHttpClient;

@Configuration
public class OkHttpConfig {

	@Bean
	public OkHttpClient okHttpClient() {
		OkHttpClient okHttpClient = new OkHttpClient();
		okHttpClient.setReadTimeout(120, TimeUnit.SECONDS);
		okHttpClient.setWriteTimeout(120, TimeUnit.SECONDS);
		okHttpClient.setConnectTimeout(120, TimeUnit.SECONDS);
		return okHttpClient;
	}
	
}
