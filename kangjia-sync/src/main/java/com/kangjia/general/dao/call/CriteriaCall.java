package com.kangjia.general.dao.call;

import org.hibernate.Criteria;

public interface CriteriaCall {

	public void criteria(Criteria criteria);
}
