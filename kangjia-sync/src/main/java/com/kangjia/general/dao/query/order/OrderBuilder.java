package com.kangjia.general.dao.query.order;

import org.hibernate.Criteria;

public class OrderBuilder {

	private Criteria criteria;

	public OrderBuilder(Criteria criteria) {
		super();
		this.criteria = criteria;
	}

	public void builder(Order order) {
		switch (order.getOrderType()) {
		case ASC:
			criteria.addOrder(org.hibernate.criterion.Order.asc(order.getName()));
			break;
		case DESC:
			criteria.addOrder(org.hibernate.criterion.Order.desc(order.getName()));
			break;
		}
	}
}
