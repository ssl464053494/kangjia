package com.kangjia.general.dao.query;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

public class ConditionBuilder {

	private Criteria criteria;

	public ConditionBuilder(Criteria criteria) {
		super();
		this.criteria = criteria;
	}

	public void builder(Param<?> param) {
		switch (param.getOper()) {
		case eq:
			criteria.add(Restrictions.eq(param.getName(), param.getVal()));
			break;
		case le:
			criteria.add(Restrictions.le(param.getName(), param.getVal()));
			break;
		case ge:
			criteria.add(Restrictions.ge(param.getName(), param.getVal()));
			break;
		case lt:
			criteria.add(Restrictions.lt(param.getName(), param.getVal()));
			break;
		case gt:
			criteria.add(Restrictions.gt(param.getName(), param.getVal()));
			break;
		case nq:
			criteria.add(Restrictions.ne(param.getName(), param.getVal()));
			break;
		case isnull:
			criteria.add(Restrictions.isNull(param.getName()));
			break;
		case notnull:
			criteria.add(Restrictions.isNotNull(param.getName()));
			break;
		case like:
			criteria.add(Restrictions.like(param.getName(), String.valueOf(param.getVal()), MatchMode.START));
			break;
		case in:
			criteria.add(Restrictions.in(param.getName(), (List<?>) param.getVal()));
			break;
		case notin:
			criteria.add(Restrictions.not(Restrictions.in(param.getName(), (List<?>) param.getVal())));
			break;
		case between:
			Object[] objs = (Object[]) param.getVal();
			criteria.add(Restrictions.between(param.getName(), objs[0], objs[1]));
			break;
		default:

		}
	}
}
