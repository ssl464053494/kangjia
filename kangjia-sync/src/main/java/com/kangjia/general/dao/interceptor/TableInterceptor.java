package com.kangjia.general.dao.interceptor;

import org.hibernate.EmptyInterceptor;

import com.kangjia.holder.TableContextHolder;
import com.kangjia.holder.VariableThreadLocal;

public class TableInterceptor extends EmptyInterceptor {

	private static final long serialVersionUID = 5998122889200684302L;

	public java.lang.String onPrepareStatement(java.lang.String sql) {
		TableContextHolder holder = VariableThreadLocal.get("TableContextHolder");
		if (holder != null) {
			sql = sql.replaceAll(holder.getTargetTable(), holder.getTempTable());
		}
		return sql;
	}
}
