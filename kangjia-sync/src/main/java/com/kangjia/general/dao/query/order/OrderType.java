package com.kangjia.general.dao.query.order;

public enum OrderType {

	ASC(1, "=?"), DESC(2, "<=?");

	private Integer type;
	private String value;

	private OrderType(Integer type, String value) {
		this.type = type;
		this.value = value;
	}

	public Integer getType() {
		return type;
	}

	public String getValue() {
		return value;
	}
}
