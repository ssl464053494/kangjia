package com.kangjia.general.dao.call;

import org.hibernate.Query;

public interface QueryExecuteUpdateCall {

	public void call(Query query);
}
