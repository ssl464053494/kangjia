package com.kangjia.util;

import com.fasterxml.jackson.databind.JsonNode;

public class JsonNodeUtil {

	public static String string(JsonNode node, String field) {
		JsonNode j = node.get(field);
		if (j != null) {
			return j.asText();
		}
		return null;
	}
	
	public static Integer integer(JsonNode node, String field) {
		JsonNode j = node.get(field);
		if (j != null) {
			return j.asInt();
		}
		return null;
	}
	
	public static Long longText(JsonNode node, String field) {
		JsonNode j = node.get(field);
		if (j != null) {
			return j.asLong();
		}
		return null;
	}
}
