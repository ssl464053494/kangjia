package com.kangjia.api.controller;

import org.springframework.beans.factory.annotation.Autowired;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

public class AbstractApiController {

	@Autowired
	protected JedisPool jedisPool;
	
	protected void set(String field, String value) {
		Jedis jedis = jedisPool.getResource();
		try {
			jedis.set(field, value);
			jedis.expire(field, 7200);
		} finally {
			jedis.close();
		}
	}

	protected String get(String field) {
		Jedis jedis = jedisPool.getResource();
		try {
			return jedis.get(field);
		} finally {
			jedis.close();
		}
	}
}
