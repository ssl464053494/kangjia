package com.kangjia.api.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.JsonNode;
import com.kangjia.api.ParserReportService;
import com.kangjia.api.ReportApiImpl;
import com.kangjia.api.report.BasePage;
import com.kangjia.api.report.Copy;
import com.kangjia.api.report.HealthIndex;
import com.kangjia.api.report.HomePage;
import com.kangjia.api.report.Score;
import com.kangjia.api.report.Sign;
import com.kangjia.api.report.SuggestPage;
import com.kangjia.general.dao.query.OperType;
import com.kangjia.general.dao.query.Page;
import com.kangjia.general.dao.query.Param;
import com.kangjia.general.dao.query.QueryParam;
import com.kangjia.general.dao.query.order.Order;
import com.kangjia.general.dao.query.order.OrderType;
import com.kangjia.holder.TableContextHolder;
import com.kangjia.jasckson.DataMapper;
import com.kangjia.model.m0.entity.Employee;
import com.kangjia.model.m0.entity.PersionReport;
import com.kangjia.model.m0.entity.Reminder;
import com.kangjia.model.m0.entity.User;
import com.kangjia.model.m0.service.ProductService;
import com.kangjia.model.m0.service.ReportService;
import com.kangjia.model.m0.service.UserService;
import com.kangjia.response.AbstractResponse;
import com.kangjia.response.ErrorResponse;
import com.kangjia.response.ObjectResponse;
import com.kangjia.response.PageResponse;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import lombok.extern.slf4j.Slf4j;
import redis.clients.jedis.Jedis;

/**
 * 健康报告查询接口
 * 
 * @author sunshulin
 *
 */
@Controller
@Slf4j
@RequestMapping(value = "/api")
public class ReportApiController extends AbstractApiController {

	@Autowired
	private ReportService reportService;
	@Autowired
	private ReportApiImpl reportApiImpl;
	@Autowired
	private UserService userService;
	@Autowired
	private ProductService productService;

	@ApiOperation(value = "个人健康报告---个人建议方案页信息", notes = "个人建议方案页信息")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "uid", value = "用户唯一标识", required = true, dataType = "String", paramType = "query"),
			@ApiImplicitParam(name = "reportId", value = "开始行", required = false, dataType = "String", paramType = "query") })
	@RequestMapping(value = "/getCopy", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiResponse(response = Copy.class, code = 200, message = "返回结果数据")
	@ResponseBody
	public Copy getCopy(Long uid, String reportId) {
		log.info("getCopy");
		User user = userService.find(uid);
		if (user == null) {
			log.warn("没有查询到用户,uid={}", uid);
			return null;
		}
		if (StringUtils.isBlank(reportId)) { // 报告不存在获取默认报告
			reportId = reportService.getCurrent(uid);
			if (StringUtils.isBlank(reportId)) { // 默认报告没有返回null
				return null;
			}
		}
		String report = queryReport(reportId);
		ParserReportService service = new ParserReportService(report, productService);
		return service.getCopy();
	}

	@ApiOperation(value = "个人健康报告---个人建议方案详细信息", notes = "个人建议方案详细信息")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "uid", value = "用户唯一标识", required = true, dataType = "String", paramType = "query"),
			@ApiImplicitParam(name = "reportId", value = "开始行", required = false, dataType = "String", paramType = "query") })
	@RequestMapping(value = "/getSuggestDetail", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiResponse(response = Copy.class, code = 200, message = "返回结果数据")
	@ResponseBody
	public String getSuggestDetail(Long uid, String reportId) {
		log.info("getSuggestDetail");
		User user = userService.find(uid);
		if (user == null) {
			log.warn("没有查询到用户,uid={}", uid);
			return null;
		}
		if (StringUtils.isBlank(reportId)) { // 报告不存在获取默认报告
			reportId = reportService.getCurrent(uid);
			if (StringUtils.isBlank(reportId)) { // 默认报告没有返回null
				return null;
			}
		}
		String report = queryReport(reportId);
		ParserReportService service = new ParserReportService(report, productService);
		return service.createSuggestDetail();
	}

	@ApiOperation(value = "个人健康报告---单个指标页信息", notes = "单个指标页信息")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "uid", value = "用户唯一标识", required = true, dataType = "String", paramType = "query"),
			@ApiImplicitParam(name = "reportId", value = "开始行", required = false, dataType = "String", paramType = "query"),
			@ApiImplicitParam(name = "resultFirstId", value = "一级指标", required = false, dataType = "String", paramType = "query"),
			@ApiImplicitParam(name = "resultSecondId", value = "二级指标", required = false, dataType = "String", paramType = "query"),
			@ApiImplicitParam(name = "resultThirdId", value = "三级指标", required = false, dataType = "String", paramType = "query") })
	@RequestMapping(value = "/getSignHealthIndex", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiResponse(response = Sign.class, code = 200, message = "返回结果数据")
	@ResponseBody
	public Sign getSignHealthIndex(Long uid, String reportId, String resultFirstId, String resultSecondId,
			String resultThirdId) {
		log.info("getSignHealthIndex");
		User user = userService.find(uid);
		if (user == null) {
			log.warn("没有查询到用户,uid={}", uid);
			return null;
		}
		if (StringUtils.isBlank(reportId)) { // 报告不存在获取默认报告
			reportId = reportService.getCurrent(uid);
			if (StringUtils.isBlank(reportId)) { // 默认报告没有返回null
				return null;
			}
		}
		String report = queryReport(reportId);
		ParserReportService service = new ParserReportService(report, productService);
		return service.queryKpi(resultFirstId, resultSecondId, resultThirdId);
	}

	@ApiOperation(value = "个人健康报告---个人健康建议", notes = "获取个人健康建议")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "uid", value = "用户唯一标识", required = true, dataType = "String", paramType = "query"),
			@ApiImplicitParam(name = "reportId", value = "报告id", required = false, dataType = "String", paramType = "query") })
	@ApiResponse(response = SuggestPage.class, code = 200, message = "个人健康建议")
	@RequestMapping(value = "/getSuggest", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public SuggestPage getSuggest(String reportId, Long uid) {
		log.info("getSuggest获取一级健康指数reportId={}", reportId);
		User user = userService.find(uid);
		if (user == null) {
			log.warn("没有查询到用户,uid={}", uid);
			return null;
		}
		if (StringUtils.isBlank(reportId)) { // 报告不存在获取默认报告
			reportId = reportService.getCurrent(uid);
			if (StringUtils.isBlank(reportId)) { // 默认报告没有返回null
				return null;
			}
		}
		String report = queryReport(reportId);
		ParserReportService service = new ParserReportService(report, productService);
		return service.getSuggest();
	}

	@ApiOperation(value = "个人健康报告---获取一级指标所有指标", notes = "获取一级指标所有指标")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "uid", value = "用户唯一标识", required = true, dataType = "String", paramType = "query"),
			@ApiImplicitParam(name = "reportId", value = "报告id", required = false, dataType = "String", paramType = "query"),
			@ApiImplicitParam(name = "resultFirstId", value = "一级指标", required = true, dataType = "String", paramType = "query") })
	@ApiResponse(response = HomePage.class, code = 200, message = "报告首页")
	@RequestMapping(value = "/getFirstFullPage", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public HomePage getFirstFullPage(String reportId, Long uid, String resultFirstId) {
		log.info("getFirstFullPage获取一级健康指数reportId={}", reportId);
		User user = userService.find(uid);
		if (user == null) {
			log.warn("没有查询到用户,uid={}", uid);
			return null;
		}
		if (StringUtils.isBlank(reportId)) { // 报告不存在获取默认报告
			reportId = reportService.getCurrent(uid);
			if (StringUtils.isBlank(reportId)) { // 默认报告没有返回null
				return null;
			}
		}
		String report = queryReport(reportId);
		ParserReportService service = new ParserReportService(report, productService);
		return service.getFirstFullPage(resultFirstId);
	}

	@ApiOperation(value = "个人健康报告---获取健康状态", notes = "获取健康状态")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "uid", value = "用户唯一标识", required = true, dataType = "String", paramType = "query"),
			@ApiImplicitParam(name = "reportId", value = "报告id", required = false, dataType = "String", paramType = "query") })
	@ApiResponse(response = HomePage.class, code = 200, message = "报告首页")
	@RequestMapping(value = "/queryByHealthStatus", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public HomePage queryByHealthStatus(String reportId, Long uid) {
		log.info("queryByHealthStatus获取一级健康指数reportId={}", reportId);
		User user = userService.find(uid);
		if (user == null) {
			log.warn("没有查询到用户,uid={}", uid);
			return null;
		}
		if (StringUtils.isBlank(reportId)) { // 报告不存在获取默认报告
			reportId = reportService.getCurrent(uid);
			if (StringUtils.isBlank(reportId)) { // 默认报告没有返回null
				return null;
			}
		}
		String report = queryReport(reportId);
		ParserReportService service = new ParserReportService(report, productService);
		return service.getHealthStatPage();
	}

	@ApiOperation(value = "个人健康报告---获取一级健康指数", notes = "获取一级健康指数")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "uid", value = "用户唯一标识", required = true, dataType = "String", paramType = "query"),
			@ApiImplicitParam(name = "reportId", value = "报告id", required = false, dataType = "String", paramType = "query") })
	@ApiResponse(response = HomePage.class, code = 200, message = "报告首页")
	@RequestMapping(value = "/queryByFirstHealthIndex", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public HomePage queryByFirstHealthIndex(String reportId, Long uid) {
		log.info("queryByFirstHealthIndex获取一级健康指数reportId={}", reportId);
		User user = userService.find(uid);
		if (user == null) {
			log.warn("没有查询到用户,uid={}", uid);
			return null;
		}
		if (StringUtils.isBlank(reportId)) { // 报告不存在获取默认报告
			reportId = reportService.getCurrent(uid);
			if (StringUtils.isBlank(reportId)) { // 默认报告没有返回null
				return null;
			}
		}
		String report = queryReport(reportId);
		ParserReportService service = new ParserReportService(report, productService);
		return service.getFirstPage();
	}

	@ApiOperation(value = "个人健康报告---顾客信息界面", notes = "个人健康默认获取顾客信息界面")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "reportId", value = "报告id", required = false, dataType = "String", paramType = "query"),
			@ApiImplicitParam(name = "uid", value = "用户唯一标识", required = true, dataType = "String", paramType = "query") })
	@ApiResponse(response = BasePage.class, code = 200, message = "信息首页")
	@RequestMapping(value = "/queryCustomerReport", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public BasePage queryCustomerReport(String reportId, Long uid) {
		log.info("queryCustomerReport获取reportId={}", reportId);
		User user = userService.find(uid);
		if (user == null) {
			log.warn("没有查询到用户,uid={}", uid);
			return null;
		}
		if (StringUtils.isBlank(reportId)) { // 报告不存在获取默认报告
			reportId = reportService.getCurrent(uid);
			if (StringUtils.isBlank(reportId)) { // 默认报告没有返回null
				return null;
			}
		}
		String report = queryReport(reportId);
		ParserReportService service = new ParserReportService(report, productService);
		BasePage page = service.getBasePage();
		page.getPersonal().setHistoryCount(reportService.count(user.getUid()));
		return page;
	}

	@ApiOperation(value = "个人健康报告---默认获取个人当前报告", notes = "个人健康默认获取最新报告信息")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "uid", value = "用户唯一标识", required = true, dataType = "String", paramType = "query") })
	@ApiResponse(response = HomePage.class, code = 200, message = "报告首页")
	@RequestMapping(value = "/queryPersionCurrentReport", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public HomePage queryPersionCurrentReport(Long uid) {
		log.info("queryPersionCurrentReport获取mobile={}最新报告", uid);
		User user = userService.find(uid);
		if (user == null) {
			log.warn("没有查询到用户,uid={}", uid);
			return null;
		}
		String reportId = reportService.getCurrent(uid);
		return getHomePage(reportId, user);
	}

	@ApiOperation(value = "个人健康报告---报告id获取指定报告", notes = "报告id获取指定报告")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "uid", value = "用户唯一标识", required = true, dataType = "String", paramType = "query"),
			@ApiImplicitParam(name = "reportId", value = "报告id", required = false, dataType = "String", paramType = "query") })
	@ApiResponse(response = HomePage.class, code = 200, message = "报告首页")
	@RequestMapping(value = "/queryByReportId", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public HomePage queryByReportId(String reportId, Long uid) {
		log.info("queryByReportId获取reportId={}最新报告, uid={}", reportId, uid);
		User user = userService.find(uid);
		if (user == null) {
			log.warn("没有查询到用户,uid={}", uid);
			return null;
		}
		if (StringUtils.isNotBlank(reportId)) { // 如果报告id存在
			log.info("发送报告请求");
			try {
				long s = System.currentTimeMillis();
				String reportData = reportApiImpl.getReport(reportId);
				if (StringUtils.isBlank(reportData)) {
					reportData = reportApiImpl.getReport(uid, reportId, user.getMobile(), user.getNickName(),
							getAge(user), user);
				} else {
					log.info("已存在报告={}", reportId);
				}
				set(reportId, reportData);
				log.info("耗时={}", System.currentTimeMillis() - s);
			} catch (Exception e) {
				log.error("获取报告异常,检测服务端接收.", e);
				return null;
			}
			log.info("结束报告解析");
		} else { // 报告不存在,获取这个人的最新报告
			reportId = reportService.getCurrent(uid);
		}
		return getHomePage(reportId, user);
	}

	@ApiOperation(value = "个人健康报告---历史记录查询", notes = "个人健康报告历史记录查询,根据用户唯一标识查询报告")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "uid", value = "用户唯一标识", required = false, dataType = "String", paramType = "query"),
			@ApiImplicitParam(name = "start", value = "开始行", required = true, dataType = "int", paramType = "query"),
			@ApiImplicitParam(name = "limit", value = "记录数", required = true, dataType = "int", paramType = "query") })
	@ApiResponse(response = PageResponse.class, code = 200, message = "")
	@RequestMapping(value = "/query", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public PageResponse<PersionReport> query(Long uid, int start, int limit) {
		log.info("query个人健康报告历史记录查询 mobile={}", uid);
		Page page = new Page();
		page.setStart(start);
		page.setLimit(limit);
		QueryParam param = new QueryParam(page);
		param.addParam(new Param<String>("persionPk.userId", OperType.eq, uid.toString()));
		param.addOrder(new Order("crtime", OrderType.DESC));
		TableContextHolder.mod(TableContextHolder.persion, new Long(uid), 10L);
		List<PersionReport> list = reportService.pager(PersionReport.class, param);
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		for (PersionReport pr : list) {
			cal.setTimeInMillis(pr.getCrtime());
			pr.setDateTime(sdf.format(cal.getTime()));
		}
		return new PageResponse<>(param != null ? param.getTotal() : null, list);
	}

	/**
	 * 报告ID查询报告信息内容,报告临时存储2个小时
	 * 
	 * @param reportId
	 * @return
	 */
	private String queryReport(String reportId) {
		String report = get(reportId);
		if (StringUtils.isBlank(report)) {
			report = reportApiImpl.getReport(reportId);
			set(reportId, report);
		}
		return report;
	}

	/**
	 * 获取报告
	 * 
	 * @param reportId
	 * @param mobile
	 * @return
	 */
	private HomePage getHomePage(String reportId, User user) {
		String report = queryReport(reportId);
		ParserReportService service = new ParserReportService(report, productService);
		return service.getHomePage();
	}

	private Integer getAge(User user) {
		if (StringUtils.isNotBlank(user.getBirth())) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Date birthDay = null;
			try {
				birthDay = sdf.parse(user.getBirth());
			} catch (ParseException e) {
				log.error("日期解析错误", e);
				return 0;
			}
			Calendar cal = Calendar.getInstance();
			if (cal.before(birthDay)) {
				throw new IllegalArgumentException("The birthDay is before Now.It's unbelievable!");
			}
			int yearNow = cal.get(Calendar.YEAR);
			int monthNow = cal.get(Calendar.MONTH);
			int dayOfMonthNow = cal.get(Calendar.DAY_OF_MONTH);
			cal.setTime(birthDay);

			int yearBirth = cal.get(Calendar.YEAR);
			int monthBirth = cal.get(Calendar.MONTH);
			int dayOfMonthBirth = cal.get(Calendar.DAY_OF_MONTH);

			int age = yearNow - yearBirth;

			if (monthNow <= monthBirth) {
				if (monthNow == monthBirth) {
					if (dayOfMonthNow < dayOfMonthBirth)
						age--;
				} else {
					age--;
				}
			}
			return age;
		}

		return 0;
	}

	@ApiOperation(value = "根据报告查询门店信息", notes = "根据报告查询门店信息")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "reportId", value = "报告id", required = true, dataType = "String", paramType = "query") })
	@ApiResponse(response = PageResponse.class, code = 200, message = "")
	@RequestMapping(value = "/getStore", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public AbstractResponse getStore(String reportId) {
		log.info("getStore根据报告查询门店信息 reportId={}", reportId);
		if (StringUtils.isBlank(reportId) || reportId.length() < 19) {
			return new ErrorResponse("报告id不存在");
		}
		String mac = reportId.substring(0, 17);
		Jedis jedis = jedisPool.getResource();
		try {
			String json = jedis.hget("device", mac);
			if (StringUtils.isBlank(json)) {
				return new ErrorResponse("mac地址没有找到设备");
			}
			JsonNode josnNode = DataMapper.getInstance().readTree(json);
			if (josnNode == null) {
				return new ErrorResponse("没有找到设备");
			}
			String storeId = josnNode.get("storeId").asText();
			json = jedis.hget("store", storeId);
			josnNode = DataMapper.getInstance().readTree(json);
			if (josnNode == null) {
				return new ErrorResponse("没有找到门店");
			}
			Map<String, String> store = new HashMap<>();
			store.put("storeId", josnNode.get("storeId").toString());
			store.put("storeNum", josnNode.get("storeNum").toString());
			store.put("cname", josnNode.get("cname").toString());
			store.put("address", josnNode.get("address").toString());
			return new ObjectResponse<Map<String, String>>(store);
		} catch(Exception e) {
			return new ErrorResponse("执行失败");
		} finally {
			jedis.close();
		}
	}

	
	@ApiOperation(value = "店员信息", notes = "店员信息")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "reportId", value = "报告id", required = false, dataType = "String", paramType = "query"),
			@ApiImplicitParam(name = "employeeId", value = "员工id", required = true, dataType = "long", paramType = "query"),
			@ApiImplicitParam(name = "employeeName", value = "员工名称", required = true, dataType = "String", paramType = "query")})
	@ApiResponse(response = PageResponse.class, code = 200, message = "")
	@RequestMapping(value = "/getEmployee", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public AbstractResponse getEmployee(String reportId, Long employeeId, String employeeName) {
		log.info("getStore根据报告查询门店信息 reportId={}", reportId);
		if (StringUtils.isBlank(reportId) || reportId.length() < 19) {
			return new ErrorResponse("报告id不存在");
		}
		Employee employee = new Employee();
		employee.setEmployeeId(employeeId);
		employee.setEmployeeName(employeeName);
		employee.setReportId(reportId);
		reportService.saveEmployee(employee);
		return new ObjectResponse<>(employee);
	}
	
	@ApiOperation(value = "个人健康报告---历史趋势", notes = "历史趋势标签页面")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "reportId", value = "报告id", required = false, dataType = "String", paramType = "query"),
			@ApiImplicitParam(name = "uid", value = "用户唯一标识", required = true, dataType = "String", paramType = "query"),
			@ApiImplicitParam(name = "resultFirstId", value = "一级指标id", required = true, dataType = "String", paramType = "query") })
	@ApiResponse(response = HealthIndex.class, code = 200, message = "")
	@RequestMapping(value = "/getHealthIndex", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public List<HealthIndex> getHealthIndex(String reportId, Long uid, String resultFirstId) {
		log.info("getHealthIndex获取reportId={}, uid={}", reportId, uid);
		User user = userService.find(uid);
		if (user == null) {
			log.warn("没有查询到用户,uid={}", uid);
			return null;
		}
		if (StringUtils.isBlank(reportId)) { // 报告不存在获取默认报告
			reportId = reportService.getCurrent(uid);
			if (StringUtils.isBlank(reportId)) { // 默认报告没有返回null
				return null;
			}
		}
		String report = queryReport(reportId);
		ParserReportService service = new ParserReportService(report, productService);
		return service.getFirst(resultFirstId);
	}
	
	@ApiOperation(value = "个人健康报告---历史趋势", notes = "历史趋势标签页面")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "uid", value = "用户唯一标识", required = false, dataType = "String", paramType = "query"),
			@ApiImplicitParam(name = "tp", value = "等级", required = true, dataType = "int", paramType = "query"),
			@ApiImplicitParam(name = "targetId", value = "指标id", required = true, dataType = "int", paramType = "query") })
	@ApiResponse(response = Score.class, code = 200, message = "")
	@RequestMapping(value = "/getScore", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public List<Score> getScore(Long uid, Integer tp, Integer targetId) {
		log.info("getScore获取tp={}, uid={}, targetId={}", tp, uid, targetId);
		User user = userService.find(uid);
		if (user == null) {
			log.warn("没有查询到用户,uid={}", uid);
			return null;
		}
		if (tp == 1) {
			return reportService.getSecond(new Long(uid), new Long(targetId));
		} else {
			return reportService.getThird(new Long(uid), new Long(targetId));
		}
	}
	
	@ApiOperation(value = "个人健康报告---检测提醒", notes = "检测提醒")
	@ApiImplicitParams({ })
	@ApiResponse(response = Reminder.class, code = 200, message = "")
	@RequestMapping(value = "/getReminder", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public List<Reminder> getReminder() {
		log.info("getReminder");
		return reportService.getReminder();
	}
}
