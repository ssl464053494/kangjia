package com.kangjia.api.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kangjia.model.m0.entity.User;
import com.kangjia.model.m0.service.UserService;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import lombok.extern.slf4j.Slf4j;

@Controller
@Slf4j
@RequestMapping(value = "/api/user")
public class UserApiController {

	@Autowired
	private UserService userService;

	@ApiOperation(value = "微信openid查询用户信息", notes = "微信openid查询用户信息")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "openid", value = "openid", required = true, dataType = "String", paramType = "query") })
	@RequestMapping(value = "/queryByOpenId", method = RequestMethod.GET)
	@ApiResponse(response = User.class, code = 200, message = "用户信息")
	@ResponseBody
	public User queryByOpenId(String openid) {
		log.info("openid={}", openid);
		return userService.searchByOpenid(openid);
	}

	@ApiOperation(value = "用户id查询用户信息", notes = "用户id查询用户信息")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "uid", value = "用户id", required = true, dataType = "int", paramType = "query") })
	@RequestMapping(value = "/queryByUid", method = RequestMethod.GET)
	@ApiResponse(response = User.class, code = 200, message = "用户信息")
	@ResponseBody
	public User queryByUid(Long uid) {
		log.info("uid={}", uid);
		return userService.find(uid);
	}

	@ApiOperation(value = "用户id查询用户信息", notes = "用户id查询用户信息")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "uid", value = "用户id", required = true, dataType = "int", paramType = "query"),
			@ApiImplicitParam(name = "mobile", value = "手机号", required = true, dataType = "string", paramType = "query"),
			@ApiImplicitParam(name = "birth", value = "生日", required = true, dataType = "string", paramType = "query"),
			@ApiImplicitParam(name = "imgUrl", value = "头像", required = true, dataType = "string", paramType = "query"),
			@ApiImplicitParam(name = "nickName", value = "昵称", required = true, dataType = "string", paramType = "query")})
	@RequestMapping(value = "/editor", method = RequestMethod.POST)
	@ApiResponse(response = User.class, code = 200, message = "用户信息")
	@ResponseBody
	public String editor(User user) {
		log.info("user={}", user.toString());
		userService.editor(user);
		return "success";
	}
	
	@ApiOperation(value = "用户id查询用户信息", notes = "用户id查询用户信息")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "uid", value = "用户id", required = true, dataType = "int", paramType = "query")})
	@ApiResponse(response = Map.class, code = 200, message = "用户信息")
	@RequestMapping(value = "/find")
	@ResponseBody
	public Map<String, Object> findUser() {
		Map<String, Object> m = new HashMap<>();
		m.put("uid", 123456);
		m.put("avatar", "http://wx.qlogo.cn/mmopen/PiajxSqBRaEKVpzdVpq8Tk90yaKJQH9rMzSZI0k4IXvicInw2POKJnS359BD0E2C4x69HLpEzuJUxGxGj7w1HFpQ/0");
		m.put("birthday", "1981-1-2");
		m.put("mobile", "18612751980");
		m.put("uname", "柜子");
		m.put("openid", "oojVKw-Rc_cMx3yjNiuOcvhsH3UQ");
		return m;
	}
}
