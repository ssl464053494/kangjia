package com.kangjia.api.condition;

import org.apache.commons.lang3.StringUtils;

import com.mongodb.BasicDBObject;

import lombok.Data;
import lombok.ToString;

/**
 * 存储mongo db报告查询条件
 * 
 * @author sunshulin
 *
 */
@ToString
@Data
public class MongoReportCondition {

	private String id;
	private String userId;
	private Long startTime;
	private Long endTime;

	private int limit = 20;
	private String lastId;

	public MongoReportCondition(String id) {
		super();
		this.id = id;
	}

	public MongoReportCondition(String userId, Long startTime, Long endTime) {
		super();
		this.userId = userId;
		this.startTime = startTime;
		this.endTime = endTime;
	}

	public MongoReportCondition(String id, String userId, Long startTime, Long endTime, String lastId) {
		super();
		this.id = id;
		this.userId = userId;
		this.startTime = startTime;
		this.endTime = endTime;
		this.lastId = lastId;
	}

	public MongoReportCondition(String userId, Long startTime, Long endTime, int limit, String lastId) {
		super();
		this.userId = userId;
		this.startTime = startTime;
		this.endTime = endTime;
		this.limit = limit;
		this.lastId = lastId;
	}

	public BasicDBObject createCondition() {
		BasicDBObject bdbo = new BasicDBObject();
		if (StringUtils.isNotBlank(id)) {
			bdbo.append("_id", id);
		}
		if (StringUtils.isNotBlank(lastId)) {
			bdbo.append("_id", new BasicDBObject("$gt", lastId));
		}
		if (StringUtils.isNotBlank(userId)) {
			bdbo.append("userId", new BasicDBObject("$eq", userId));
		}
		if (startTime != null && endTime != null) {
			bdbo.append("crtime", new BasicDBObject("$gte", startTime)).append("crtime", new BasicDBObject("$lte", endTime));
		}
		return bdbo;
	}
}
