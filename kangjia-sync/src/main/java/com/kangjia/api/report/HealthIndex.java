package com.kangjia.api.report;

import java.util.ArrayList;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

@ApiModel(value = "健康指数")
@Data
@ToString
public class HealthIndex {

	@ApiModelProperty(value = "分数")
	private String score;
	@ApiModelProperty(value = "指数名称")
	private String name;
	@ApiModelProperty(value = "指标ID")
	private String targetId;
	@ApiModelProperty(value = "一级指标ID")
	private String resultFirstId;
	@ApiModelProperty(value = "一级指标ID")
	private String resultSecondId;
	@ApiModelProperty(value = "三级指标ID")
	private String resultThirdId;
	@ApiModelProperty(value = "说明")
	private String info;
	@ApiModelProperty(value = "指标解释")
	private String inspectExplain;
	@ApiModelProperty(value = "指导意义内容")
	private String guideContent;
	@ApiModelProperty(value = "改进建议")
	private String suggestContent;
	@ApiModelProperty(value = "改善方案")
	private String improvementProgram;
	@ApiModelProperty(value = "检测异常程度,1:正常,2:轻度,3:中度,4:重度")
	private String abnormalLevel;
	@ApiModelProperty(value = "检查标准,1:正常,2:偏高,3:偏低")
	private String inspectStandard;
	@ApiModelProperty(value = "膳食建议")
	private List<Advice> diat;
	@ApiModelProperty(value = "营养建议")
	private List<Advice> nutrition;
	@ApiModelProperty(value = "运动和生活方式建议")
	private List<Advice> motion;
	@ApiModelProperty(value = "子项")
	private List<HealthIndex> children;
	@ApiModelProperty(value = "生理年龄")
	private String age;
	
	public void add(HealthIndex index) {
		if (children == null) {
			children = new ArrayList<>();
		}
		children.add(index);
	}
}
