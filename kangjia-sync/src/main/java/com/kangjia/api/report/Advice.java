package com.kangjia.api.report;

import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

@ApiModel(value = "建议信息")
@Data
@ToString
public class Advice {

	@ApiModelProperty(value = "建议ID")
	private String indexTypeId;
	@ApiModelProperty(value = "名称")
	private String indexName;
	@ApiModelProperty(value = "描述")
	private String describe;
	@ApiModelProperty(value = "简单建议")
	private String simpleAdvice;
	@ApiModelProperty(value = "个性化建议")
	private String personalizedAdvice;
	@ApiModelProperty(value = "推荐商品")
	private List<Product> products;
	@ApiModelProperty(value = "轻重程度")
	private String abnormalLevel;

	public boolean equals(Object obj) {
		Advice other = (Advice) obj;
		return other.indexTypeId.equals(this.indexTypeId);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((indexTypeId == null) ? 0 : indexTypeId.hashCode());
		return result;
	}
}