package com.kangjia.api.report;

import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

@ApiModel(value = "单个指标建议信息")
@Data
@ToString
public class Sign {

	@ApiModelProperty(value = "个人信息")
	private Personal personal;
	@ApiModelProperty(value = "分数")
	private String score;
	@ApiModelProperty(value = "身体状况")
	private String ps;
	@ApiModelProperty(value = "检测时间")
	private String detectionTime;
	@ApiModelProperty(value = "指标信息")
	private HealthIndex index;
	@ApiModelProperty(value = "生理年龄")
	private String physicalAge;
	@ApiModelProperty(value = "膳食建议")
	private List<Advice> diat;
	@ApiModelProperty(value = "营养建议")
	private List<Advice> nutrition;
	@ApiModelProperty(value = "运动和生活方式建议")
	private List<Advice> motion;
}
