package com.kangjia.api.report;

import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

@ApiModel(value = "建议信息")
@Data
@ToString
public class SuggestPage {

	@ApiModelProperty(value = "身体状况")
	private String ps;
	@ApiModelProperty(value = "生理年龄")
	private String physiologicalAge;
	@ApiModelProperty(value = "百分率")
	private String percentage;
	@ApiModelProperty(value = "异常指标")
	private String abnormals;
	@ApiModelProperty(value = "异常指标数量")
	private String abnormalsSize;
	@ApiModelProperty(value = "膳食建议")
	List<HealthIndex> diets;
	@ApiModelProperty(value = "营养建议")
	List<HealthIndex> nutritions; 
	@ApiModelProperty(value = "运动建议")
	List<HealthIndex> motions;
}
