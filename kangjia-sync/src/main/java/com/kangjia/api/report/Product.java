package com.kangjia.api.report;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

@ApiModel(value = "商品信息")
@Data
@ToString
public class Product {

	@ApiModelProperty(value = "商品名称")
	private String productName;
	@ApiModelProperty(value = "说明")
	private String info;
	@ApiModelProperty(value = "商品图片url")
	private String imgUrl;
	@ApiModelProperty(value = "商品url")
	private String url;
	@ApiModelProperty(value = "商品价格")
	private String price;
	@ApiModelProperty(value = "商品价格")
	private String mktprice;
	@ApiModelProperty(value = "商品id")
	private String productId;
}
