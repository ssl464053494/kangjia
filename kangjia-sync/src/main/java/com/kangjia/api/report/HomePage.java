package com.kangjia.api.report;

import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

@ApiModel(value = "报告首页")
@Data
@ToString
public class HomePage {

	@ApiModelProperty(value = "检测时间")
	private String detectionTime;
	@ApiModelProperty(value = "个人信息")
	private Personal personal;
	@ApiModelProperty(value = "生理年龄")
	private String physicalAge;
	@ApiModelProperty(value = "分数")
	private String score;
	@ApiModelProperty(value = "健康状态")
	private List<Health> health;
	@ApiModelProperty(value = "健康建议")
	private String healthAdvice;
	@ApiModelProperty(value = "健康指数")
	private List<HealthIndex> healthIndex;
	@ApiModelProperty(value = "身体状况")
	private String ps;
	@ApiModelProperty(value = "生理年龄")
	private List<Age> ages;
	@ApiModelProperty(value = "比例")
	private String percentage;
	@ApiModelProperty(value = "健康建议内容")
	private Copy copy;
	@ApiModelProperty(value = "膳食建议")
	private List<Advice> diat;
	@ApiModelProperty(value = "营养建议")
	private List<Advice> nutrition;
	@ApiModelProperty(value = "运动和生活方式建议")
	private List<Advice> motion;
	@ApiModelProperty(value = "提醒")
	private String reminder;
	
}
