package com.kangjia.api.report;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

@ApiModel(value = "健康状态")
@Data
@ToString
public class Health {

	@ApiModelProperty(value = "名称")
	private String name;
	@ApiModelProperty(value = "检测异常成都,数值越小越分数越高")
	private String abnormalLevel;
	@ApiModelProperty(value = "分析结果,酸碱度(正常,酸性,碱性)")
	private String analysisResults;
	@ApiModelProperty(value = "指标解释")
	private String inspectExplain;
	@ApiModelProperty(value = "指导意义内容")
	private String guideContent;
	@ApiModelProperty(value = "改进建议")
	private String suggestContent;
	@ApiModelProperty(value = "改善方案")
	private String improvementProgram;
}
