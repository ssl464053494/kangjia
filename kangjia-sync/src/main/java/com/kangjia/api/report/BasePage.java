package com.kangjia.api.report;

import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

@ApiModel(value = "个人信息也")
@Data
@ToString
public class BasePage {

	@ApiModelProperty(value = "个人信息")
	private Personal personal;
	@ApiModelProperty(value = "分数")
	private String score;
	@ApiModelProperty(value = "身体状况")
	private String ps;
	@ApiModelProperty(value = "膳食建议")
	List<HealthIndex> diets;
	@ApiModelProperty(value = "营养建议")
	List<HealthIndex> nutritions; 
	@ApiModelProperty(value = "运动建议")
	List<HealthIndex> motions;
}
