package com.kangjia.api.report;

import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

@ApiModel(value = "文案信息")
@Data
@ToString
public class Copy {

	@ApiModelProperty(value = "名称")
	private String name;
	@ApiModelProperty(value = "性别")
	private String sex;
	@ApiModelProperty(value = "状态")
	private String ps;
	@ApiModelProperty(value = "生理年龄")
	private String physicalAge;
	@ApiModelProperty(value = "比例")
	private String percentage;
	@ApiModelProperty(value = "异常指标")
	private String abnormals;
	@ApiModelProperty(value = "异常指标数量")
	private String abnormalsSize;
	@ApiModelProperty(value = "膳食建议")
	private List<Advice> diat;
	@ApiModelProperty(value = "营养建议")
	private List<Advice> nutrition;
	@ApiModelProperty(value = "运动和生活方式建议")
	private List<Advice> motion;
	@ApiModelProperty(value = "个人信息")
	private Personal personal;
	@ApiModelProperty(value = "分数")
	private String score;
	@ApiModelProperty(value = "提醒")
	private String reminder;
	
}
