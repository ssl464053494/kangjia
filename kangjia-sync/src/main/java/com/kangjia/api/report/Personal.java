package com.kangjia.api.report;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

@ApiModel(value = "个人信息")
@Data
@ToString
public class Personal {

	@ApiModelProperty(value = "用户ID")
	private String userId;
	@ApiModelProperty(value = "手机号码")
	private String mobile;
	@ApiModelProperty(value = "微信头像")
	private String headPortrait;
	@ApiModelProperty(value = "昵称")
	private String nickname;
	@ApiModelProperty(value = "性别")
	private String sex;
	@ApiModelProperty(value = "出生日期")
	private String birth;
	@ApiModelProperty(value = "历史报告")
	private Integer historyCount;
}
