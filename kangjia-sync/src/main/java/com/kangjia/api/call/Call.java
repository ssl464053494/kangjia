package com.kangjia.api.call;

import com.fasterxml.jackson.databind.JsonNode;
import com.kangjia.api.bean.Kpi;

public interface Call {

	public Kpi create();
	
	public void handler(Kpi entity, JsonNode node);
	
}
