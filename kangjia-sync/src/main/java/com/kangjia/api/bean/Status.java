package com.kangjia.api.bean;

public class Status extends Kpi {

	private String abnormalLevel;
	private String analysisResults;
	private String text;

	public String getAbnormalLevel() {
		return abnormalLevel;
	}

	public void setAbnormalLevel(String abnormalLevel) {
		this.abnormalLevel = abnormalLevel;
	}

	public String getAnalysisResults() {
		return analysisResults;
	}

	public void setAnalysisResults(String analysisResults) {
		this.analysisResults = analysisResults;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
}
