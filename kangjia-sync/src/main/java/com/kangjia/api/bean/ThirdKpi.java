package com.kangjia.api.bean;

public class ThirdKpi extends SecondKpi {

	private String resultThirdId;
	private String inspectStandard;
	private String abnormalLevel;

	public String getResultThirdId() {
		return resultThirdId;
	}

	public void setResultThirdId(String resultThirdId) {
		this.resultThirdId = resultThirdId;
	}

	public String getInspectStandard() {
		return inspectStandard;
	}

	public void setInspectStandard(String inspectStandard) {
		this.inspectStandard = inspectStandard;
	}

	public String getAbnormalLevel() {
		return abnormalLevel;
	}

	public void setAbnormalLevel(String abnormalLevel) {
		this.abnormalLevel = abnormalLevel;
	}
}
