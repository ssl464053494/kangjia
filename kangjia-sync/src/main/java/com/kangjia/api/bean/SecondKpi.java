package com.kangjia.api.bean;

public class SecondKpi extends FirstKpi {

	private String resultSecondId;

	public String getResultSecondId() {
		return resultSecondId;
	}

	public void setResultSecondId(String resultSecondId) {
		this.resultSecondId = resultSecondId;
	}
}
