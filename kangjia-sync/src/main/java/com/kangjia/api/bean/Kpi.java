package com.kangjia.api.bean;

public abstract class Kpi {

	private String inspectLevel;
	private String targetId;
	private String inspectName;
	private String inspectExplain;
	private String score;
	private String sort;

	public String getInspectLevel() {
		return inspectLevel;
	}

	public void setInspectLevel(String inspectLevel) {
		this.inspectLevel = inspectLevel;
	}

	public String getTargetId() {
		return targetId;
	}

	public void setTargetId(String targetId) {
		this.targetId = targetId;
	}

	public String getInspectName() {
		return inspectName;
	}

	public void setInspectName(String inspectName) {
		this.inspectName = inspectName;
	}

	public String getInspectExplain() {
		return inspectExplain;
	}

	public void setInspectExplain(String inspectExplain) {
		this.inspectExplain = inspectExplain;
	}

	public String getScore() {
		return score;
	}

	public void setScore(String score) {
		this.score = score;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}
}
