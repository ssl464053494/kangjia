package com.kangjia.http;

import lombok.Data;

/**
 * http 请求参数及值
 * 
 * @author sunshulin
 *
 */
@Data
public class HttpParam {

	private String param;

	private String value;

	public HttpParam(String param, String value) {
		super();
		this.param = param;
		this.value = value;
	}

	public String toString() {
		return "HttpParam [param=" + param + ", value=" + value + "]";
	}
	
	public String string() {
		return param + "=" + value;
	}
}
