package com.kangjia.model.m0.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class ReportFirstId implements java.io.Serializable {

	private static final long serialVersionUID = 7652973899318272149L;
	private Long ymd;
	private Long uid;
	private Long targetId;

	public ReportFirstId() {
	}

	@Column(name = "ymd", nullable = false)
	public Long getYmd() {
		return this.ymd;
	}

	public void setYmd(Long ymd) {
		this.ymd = ymd;
	}

	@Column(name = "uid", nullable = false)
	public Long getUid() {
		return this.uid;
	}

	public void setUid(Long uid) {
		this.uid = uid;
	}

	@Column(name = "target_id", nullable = false)
	public Long getTargetId() {
		return this.targetId;
	}

	public void setTargetId(Long targetId) {
		this.targetId = targetId;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof ReportFirstId))
			return false;
		ReportFirstId castOther = (ReportFirstId) other;

		return ((this.getYmd() == castOther.getYmd())
				|| (this.getYmd() != null && castOther.getYmd() != null && this.getYmd().equals(castOther.getYmd())))
				&& ((this.getUid() == castOther.getUid()) || (this.getUid() != null && castOther.getUid() != null
						&& this.getUid().equals(castOther.getUid())))
				&& ((this.getTargetId() == castOther.getTargetId()) || (this.getTargetId() != null
						&& castOther.getTargetId() != null && this.getTargetId().equals(castOther.getTargetId())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result + (getYmd() == null ? 0 : this.getYmd().hashCode());
		result = 37 * result + (getUid() == null ? 0 : this.getUid().hashCode());
		result = 37 * result + (getTargetId() == null ? 0 : this.getTargetId().hashCode());
		return result;
	}

}