package com.kangjia.model.m0.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "ts_report_second")
public class ReportSecond implements java.io.Serializable {

	private static final long serialVersionUID = -7028603382505688029L;
	private ReportSecondId id;
	private Long firstId;
	private Integer score;

	public ReportSecond() {
	}

	@EmbeddedId
	@AttributeOverrides({ @AttributeOverride(name = "ymd", column = @Column(name = "ymd", nullable = false)),
			@AttributeOverride(name = "uid", column = @Column(name = "uid", nullable = false)),
			@AttributeOverride(name = "targetId", column = @Column(name = "target_id", nullable = false)) })
	public ReportSecondId getId() {
		return this.id;
	}

	public void setId(ReportSecondId id) {
		this.id = id;
	}

	@Column(name = "first_id")
	public Long getFirstId() {
		return this.firstId;
	}

	public void setFirstId(Long firstId) {
		this.firstId = firstId;
	}

	@Column(name = "score")
	public Integer getScore() {
		return this.score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

}