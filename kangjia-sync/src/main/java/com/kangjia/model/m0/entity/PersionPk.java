package com.kangjia.model.m0.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@Embeddable
@ApiModel(value = "个人报告唯一标识")
public class PersionPk implements Serializable {

	private static final long serialVersionUID = 7160709209558417406L;
	private String userId;
	@ApiModelProperty(value = "手机号")
	private String mobile;
	@ApiModelProperty(value = "报告编号")
	private String inspectId;

	@Column(name = "user_id", nullable = false, length = 32)
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	@Column(name = "mobile", nullable = false, length = 11)
	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	@Column(name = "inspect_id", nullable = false, length = 32)
	public String getInspectId() {
		return inspectId;
	}

	public void setInspectId(String inspectId) {
		this.inspectId = inspectId;
	}
}
