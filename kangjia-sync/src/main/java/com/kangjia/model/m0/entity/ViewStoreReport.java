package com.kangjia.model.m0.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang3.StringUtils;

@Entity
@Table(name = "v_store")
public class ViewStoreReport implements Serializable {

	private static final long serialVersionUID = -5918639308422629605L;

	private String inspectId;
	private Integer storeId;
	private Long crtime;
	private String mobile;
	private String name;
	private String userId;
	private String sn;
	private String phone;
	private Integer score;
	
	private String dateTime;
	private String storeName;
	private String employeeName;

	@Id
	@Column(name = "inspect_id", nullable = false, length = 64)
	public String getInspectId() {
		return this.inspectId;
	}

	public void setInspectId(String inspectId) {
		this.inspectId = inspectId;
	}

	@Column(name = "store_id", nullable = false)
	public Integer getStoreId() {
		return this.storeId;
	}

	public void setStoreId(Integer storeId) {
		this.storeId = storeId;
	}

	@Column(name = "crtime")
	public Long getCrtime() {
		return this.crtime;
	}

	public void setCrtime(Long crtime) {
		this.crtime = crtime;
	}

	@Column(name = "mobile", length = 32)
	public String getMobile() {
		return this.mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	@Column(name = "name", length = 32)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "user_id", length = 32)
	public String getUserId() {
		return this.userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	@Column(name = "sn", length = 17)
	public String getSn() {
		return this.sn;
	}

	public void setSn(String sn) {
		this.sn = sn;
	}

	@Column(name = "phone", length = 11)
	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Column(name = "score")
	public Integer getScore() {
		return this.score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	@Transient
	public String getDateTime() {
		return dateTime;
	}

	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}

	@Transient
	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public List<String> toCsv() {
		List<String> list = new ArrayList<>();
		list.add(getString(storeName));
		list.add(getString(inspectId));
		list.add(getString(name));
		list.add(getString(mobile));
		list.add(getString(employeeName));
		list.add((score != null ? String.valueOf(score) : ""));
		list.add(getString(dateTime));
		return  list;
	}
	
	private String getString(String key) {
		return StringUtils.isNoneBlank(key) ? key : "";
	}

	@Transient
	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}
}
