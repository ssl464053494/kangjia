package com.kangjia.model.m0.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class StorePk implements Serializable {

	private static final long serialVersionUID = 3438107960247726525L;
	private Integer storeId;
	private String inspectId;

	public StorePk() {
		super();
	}

	public StorePk(Integer storeId, String inspectId) {
		super();
		this.storeId = storeId;
		this.inspectId = inspectId;
	}

	@Column(name = "store_id", nullable = false)
	public Integer getStoreId() {
		return storeId;
	}

	public void setStoreId(Integer storeId) {
		this.storeId = storeId;
	}

	@Column(name = "inspect_id", nullable = false, length = 32)
	public String getInspectId() {
		return inspectId;
	}

	public void setInspectId(String inspectId) {
		this.inspectId = inspectId;
	}
}
