package com.kangjia.model.m0.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "ts_report_first")
public class ReportFirst implements java.io.Serializable {

	private static final long serialVersionUID = -5284601597579170796L;
	private ReportFirstId id;
	private Integer score;

	public ReportFirst() {
	}

	@EmbeddedId
	@AttributeOverrides({ @AttributeOverride(name = "ymd", column = @Column(name = "ymd", nullable = false)),
			@AttributeOverride(name = "uid", column = @Column(name = "uid", nullable = false)),
			@AttributeOverride(name = "targetId", column = @Column(name = "target_id", nullable = false)) })
	public ReportFirstId getId() {
		return this.id;
	}

	public void setId(ReportFirstId id) {
		this.id = id;
	}

	@Column(name = "score")
	public Integer getScore() {
		return this.score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

}