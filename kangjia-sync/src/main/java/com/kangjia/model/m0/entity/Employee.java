package com.kangjia.model.m0.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ts_employee")
public class Employee implements Serializable {

	private static final long serialVersionUID = 8199516820728732520L;
	private String reportId;
	private Long employeeId;
	private String employeeName;

	@Id
	@Column(name = "report_id", unique = true, nullable = false, length = 64)
	public String getReportId() {
		return reportId;
	}

	public void setReportId(String reportId) {
		this.reportId = reportId;
	}

	@Column(name = "employee_id")
	public Long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}

	@Column(name = "employee_name", length = 64)
	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}
}
