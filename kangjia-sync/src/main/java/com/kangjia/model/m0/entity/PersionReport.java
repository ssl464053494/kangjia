package com.kangjia.model.m0.entity;

import java.io.Serializable;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "ts_persion")
@ApiModel(value = "个人报告信息")
public class PersionReport implements Serializable {

	private static final long serialVersionUID = 8680470487897187462L;
	private PersionPk persionPk;
	private String name;
	private Long crtime;
	private Integer score;
	
	@ApiModelProperty(value = "报告时间")
	private String dateTime;

	@EmbeddedId
	@AttributeOverrides({ @AttributeOverride(name = "userId", column = @Column(name = "user_id", nullable = false, length = 32)),
			@AttributeOverride(name = "mobile", column = @Column(name = "mobile", nullable = false, length = 11)),
			@AttributeOverride(name = "inspectId", column = @Column(name = "inspect_id", nullable = false, length = 32)) })
	public PersionPk getPersionPk() {
		return persionPk;
	}

	public void setPersionPk(PersionPk persionPk) {
		this.persionPk = persionPk;
	}

	@Column(name = "name", length = 32)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "crtime")
	public Long getCrtime() {
		return crtime;
	}

	public void setCrtime(Long crtime) {
		this.crtime = crtime;
	}

	@Column(name = "score")
	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	@Transient
	public String getDateTime() {
		return dateTime;
	}

	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}

}
