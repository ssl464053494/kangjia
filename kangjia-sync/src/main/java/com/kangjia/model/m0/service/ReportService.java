package com.kangjia.model.m0.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kangjia.api.report.Score;
import com.kangjia.general.dao.query.OperType;
import com.kangjia.general.dao.query.Page;
import com.kangjia.general.dao.query.Param;
import com.kangjia.general.dao.query.QueryParam;
import com.kangjia.general.dao.query.order.Order;
import com.kangjia.general.dao.query.order.OrderType;
import com.kangjia.holder.TableContextHolder;
import com.kangjia.model.m0.controller.Report;
import com.kangjia.model.m0.dao.ReportDao;
import com.kangjia.model.m0.entity.Employee;
import com.kangjia.model.m0.entity.PersionReport;
import com.kangjia.model.m0.entity.Reminder;
import com.kangjia.model.m0.entity.ReportSecond;
import com.kangjia.model.m0.entity.ReportThird;
import com.kangjia.model.m0.entity.StorePk;
import com.kangjia.model.m0.entity.StoreReport;

@Service
public class ReportService {

	@Autowired
	private ReportDao reportDao;
	
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
	
	public Integer count(Long uid) {
		TableContextHolder.mod(TableContextHolder.persion, uid, 10L);
		QueryParam queryParam = new QueryParam();
		queryParam.addParam(new Param<String>("persionPk.userId", OperType.eq, uid.toString()));
		return reportDao.count(PersionReport.class, queryParam);
	}
	
	public String getCurrent(Long uid) {
		QueryParam queryParam = new QueryParam();
		queryParam.addParam(new Param<String>("persionPk.userId", OperType.eq, uid.toString()));
		queryParam.addOrder(new Order("crtime", OrderType.DESC));
		TableContextHolder.mod(TableContextHolder.persion, uid, 10L);
		PersionReport persionReport = reportDao.find(PersionReport.class, queryParam);
		return persionReport.getPersionPk().getInspectId();
	}

	public void bind(String sn, Integer storeId) {
		int start = 0;
		Page page = new Page(0, 100);
		QueryParam queryParam = new QueryParam(page);
		queryParam.addParam(new Param<Integer>("storePk.storeId", OperType.eq, -1));
		queryParam.addParam(new Param<String>("sn", OperType.eq, sn));
		while (true) {
			TableContextHolder.mod(TableContextHolder.store, -1L, 10L);
			List<StoreReport> list = reportDao.query(StoreReport.class, queryParam);
			if (list != null && !list.isEmpty()) {
				for (StoreReport sr : list) {
					sr.getStorePk().setStoreId(storeId);
					TableContextHolder.mod(TableContextHolder.store, -1L, 10L);
					reportDao.delete(StoreReport.class, new StorePk(-1, sr.getStorePk().getInspectId()));
					TableContextHolder.mod(TableContextHolder.store, new Long(sr.getStorePk().getStoreId()), 10L);
					reportDao.saveOrUpdate(sr);
				}
				queryParam.setStart(start + 1);
			} else {
				break;
			}
		}
	}

	public <T> List<T> pager(Class<T> clazz, QueryParam queryParam) {
		return reportDao.query(clazz, queryParam);
	}

	/**
	 * 报告修改名称和电话
	 * 
	 * @param report
	 */
	public void update(Report report) {
		StorePk storePk = new StorePk();
		storePk.setInspectId(report.getInspectId());
		storePk.setStoreId(report.getStoreId());
		TableContextHolder.mod(TableContextHolder.store, new Long(report.getStoreId()), 10L);
		StoreReport sr = reportDao.find(StoreReport.class, storePk);
		sr.setMobile(report.getMobile());
		sr.setName(report.getName());
		reportDao.saveOrUpdate(sr);
	}
	
	public void saveEmployee(Employee employee) {
		Employee old = reportDao.find(Employee.class, employee.getReportId());
		if (old == null) {
			reportDao.saveOrUpdate(employee);
		}
	}
	
	public Employee getEmployee(String reportId) {
		return reportDao.find(Employee.class, reportId);
	}

	public List<Score> getSecond(Long uid, Long targetId) {
		List<Score> scores = new ArrayList<>();
		QueryParam queryParam = new QueryParam();
		queryParam.addParam(new Param<Long>("id.uid", OperType.eq, uid));
		queryParam.addParam(new Param<Long>("id.targetId", OperType.eq, targetId));
		queryParam.addOrder(new Order("id.ymd", OrderType.ASC));
		List<ReportSecond> list = reportDao.query(ReportSecond.class, queryParam);
		for (ReportSecond rs : list) {
			Score score = new Score();
			score.setDate(getDate(String.valueOf(rs.getId().getYmd())));
			score.setScore(rs.getScore());
			scores.add(score);
		}
		return scores;
	}
	
	public List<Score> getThird(Long uid, Long targetId) {
		List<Score> scores = new ArrayList<>();
		QueryParam queryParam = new QueryParam();
		queryParam.addParam(new Param<Long>("id.uid", OperType.eq, uid));
		queryParam.addParam(new Param<Long>("id.targetId", OperType.eq, targetId));
		queryParam.addOrder(new Order("id.ymd", OrderType.ASC));
		List<ReportThird> list = reportDao.query(ReportThird.class, queryParam);
		for (ReportThird rs : list) {
			Score score = new Score();
			score.setDate(getDate(String.valueOf(rs.getId().getYmd())));
			score.setScore(rs.getScore());
			scores.add(score);
		}
		return scores;
	}
	
	private String getDate(String value) {
		return value.substring(0, 4) + "-" + value.substring(4, 6) + "-" + value.substring(6);
	}
	
	public List<Reminder> getReminder() {
		Calendar cal = Calendar.getInstance();
		QueryParam queryParam = new QueryParam();
		queryParam.addParam(new Param<Long>("crtime", OperType.eq, format(cal.getTime())));
		List<Reminder> list = reportDao.query(Reminder.class, queryParam);
		return list;
	}
	
	private Long format(Date date) {
		return new Long(sdf.format(date));
	}
	
}