package com.kangjia.model.m0.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.kangjia.http.HttpParam;
import com.kangjia.http.HttpRequest;
import com.kangjia.http.HttpResponse;
import com.kangjia.model.m0.dao.ReportDao;
import com.kangjia.model.m0.entity.OpenId;
import com.kangjia.model.m0.entity.User;

import lombok.extern.slf4j.Slf4j;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

@Service
@Slf4j
public class UserService {

	@Autowired
	protected ReportDao reportDao;

	@Autowired
	private JedisPool jedisPool;

	@Autowired
	protected HttpRequest httpRequest;
	@Value("${user.url}")
	private String url;

	public User searchByOpenid(String openid) {
		OpenId openId = reportDao.find(OpenId.class, openid);
		if (openId != null) {
			User old = reportDao.find(User.class, openId.getUid());
			return old;
		} else {
			openId = new OpenId();
			openId.setOpenid(openid);
			Long uid = getUid();
			openId.setUid(uid);
			reportDao.saveOrUpdate(openId); // 保存openid

			User user = new User();
			user.setUid(uid);
			user.setOpenid(openid);
			user.setCrtime(System.currentTimeMillis());
			reportDao.saveOrUpdate(user);
			return user;
		}
	}

	private Long getUid() {
		Jedis jedis = jedisPool.getResource();
		Long inc = 0L;
		try {
			inc = jedis.hincrBy("hwinc", "user.id", 1);
		} finally {
			jedis.close();
		}
		return inc;
	}

	public void editor(User user) {
		if (user.getUid() != null) {
			User old = reportDao.find(User.class, user.getUid());
			copy(user, old);
			reportDao.saveOrUpdate(old);
		}
	}

	public User find(Long uid) {
		log.info("uid={}", uid);
		List<HttpParam> params = new ArrayList<>();
		params.add(new HttpParam("uid", uid.toString()));
		HttpResponse response = httpRequest.form(url, params);
		String res = response.getResponse();
		if (StringUtils.isNotBlank(res)) {
			User user = new User();
			JSONObject json = new JSONObject(res);
			if (!json.isNull("uid")) {
				user.setUid(json.getLong("uid"));
			}
			if (!json.isNull("avatar")) {
				user.setImgUrl(json.getString("avatar"));
			}
			if (!json.isNull("birthday")) {
				String birth = json.getString("birthday");
				if (StringUtils.isNoneBlank(birth)) {
					String[] bs = birth.split("-");
					user.setBirth(bs[0] + "-" + String.format("%02d", Integer.parseInt(bs[1])) + "-"
							+ String.format("%02d", Integer.parseInt(bs[2])));
				}
			}
			if (!json.isNull("mobile")) {
				user.setMobile(json.getString("mobile"));
			}
			if (!json.isNull("uname")) {
				user.setNickName(json.getString("uname"));
			}
			if (!json.isNull("openid")) {
				user.setOpenid(json.getString("openid"));
			}
			return user;
		}
		return null;
	}

	private void copy(User user, User old) {
		if (StringUtils.isNotBlank(user.getBirth())) {
			old.setBirth(user.getBirth());
		}
		if (StringUtils.isNotBlank(user.getImgUrl())) {
			old.setImgUrl(user.getImgUrl());
		}
		if (StringUtils.isNotBlank(user.getMobile())) {
			old.setMobile(user.getMobile());
		}
		if (StringUtils.isNotBlank(user.getNickName())) {
			old.setNickName(user.getNickName());
		}
		if (StringUtils.isNotBlank(user.getOpenid())) {
			old.setOpenid(user.getOpenid());
		}
	}

}
