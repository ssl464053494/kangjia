package com.kangjia.model.m0.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "用户信息")
@Entity
@Table(name = "ts_user")
public class User implements Serializable {

	private static final long serialVersionUID = -8454153585493449915L;

	@ApiModelProperty(value = "用户id")
	private Long uid;
	@ApiModelProperty(value = "昵称")
	private String nickName;
	@ApiModelProperty(value = "头像")
	private String imgUrl;
	@ApiModelProperty(value = "生日")
	private String birth;
	@ApiModelProperty(value = "openid")
	private String openid;
	@ApiModelProperty(value = "手机")
	private String mobile;
	@ApiModelProperty(value = "创建时间")
	private Long crtime;

	@Id
	@Column(name = "uid", unique = true, nullable = false)
	public Long getUid() {
		return uid;
	}

	public void setUid(Long uid) {
		this.uid = uid;
	}

	@Column(name = "nick_name", length = 60)
	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	@Column(name = "img_url", length = 128)
	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}

	@Column(name = "birth", length = 30)
	public String getBirth() {
		return birth;
	}

	public void setBirth(String birth) {
		this.birth = birth;
	}

	@Column(name = "openid", length = 64)
	public String getOpenid() {
		return openid;
	}

	public void setOpenid(String openid) {
		this.openid = openid;
	}

	@Column(name = "mobile", length = 11)
	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	@Column(name = "crtime")
	public Long getCrtime() {
		return crtime;
	}

	public void setCrtime(Long crtime) {
		this.crtime = crtime;
	}

	public String toString() {
		return "User [uid=" + uid + ", nickName=" + nickName + ", imgUrl=" + imgUrl + ", birth=" + birth + ", openid="
				+ openid + ", mobile=" + mobile + ", crtime=" + crtime + "]";
	}
}
