package com.kangjia.model.m0.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ts_openid")
public class OpenId implements Serializable {

	private static final long serialVersionUID = 3332793184581950727L;
	private Long uid;
	private String openid;

	@Id
	@Column(name = "openid", unique = true, nullable = false)
	public String getOpenid() {
		return openid;
	}

	public void setOpenid(String openid) {
		this.openid = openid;
	}

	@Column(name = "uid")
	public Long getUid() {
		return uid;
	}

	public void setUid(Long uid) {
		this.uid = uid;
	}

}
