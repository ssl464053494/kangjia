package com.kangjia.model.m0.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang3.StringUtils;

@Entity
@Table(name = "ts_store")
public class StoreReport implements Serializable {

	private static final long serialVersionUID = 4256102703411817320L;

	private StorePk storePk;
	private String userId;
	private String mobile;
	private String name;
	private Long crtime;
	private String sn;
	private String phone;
	private Integer score;

	private String storeName;
	private Integer storeId;
	private String inspectId;
	private String dateTime;
	private String employeeName;

	@EmbeddedId
	@AttributeOverrides({ @AttributeOverride(name = "storeId", column = @Column(name = "store_id", nullable = false)),
			@AttributeOverride(name = "inspectId", column = @Column(name = "inspect_id", nullable = false, length = 32)) })
	public StorePk getStorePk() {
		return storePk;
	}

	public void setStorePk(StorePk storePk) {
		this.storePk = storePk;
	}

	@Column(name = "user_id", length = 32)
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	@Column(name = "mobile", length = 32)
	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	@Column(name = "name", length = 32)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "crtime")
	public Long getCrtime() {
		return crtime;
	}

	public void setCrtime(Long crtime) {
		this.crtime = crtime;
	}

	@Column(name = "sn", length = 17)
	public String getSn() {
		return sn;
	}

	public void setSn(String sn) {
		this.sn = sn;
	}

	@Column(name = "phone", length = 17)
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Column(name = "score")
	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	@Transient
	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	@Transient
	public Integer getStoreId() {
		return storeId;
	}

	public void setStoreId(Integer storeId) {
		this.storeId = storeId;
	}

	@Transient
	public String getInspectId() {
		return inspectId;
	}

	public void setInspectId(String inspectId) {
		this.inspectId = inspectId;
	}

	@Transient
	public String getDateTime() {
		return dateTime;
	}

	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}

	@Transient
	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public List<String> toCsv() {
		List<String> list = new ArrayList<>();
		list.add(getString(storeName));
		list.add(getString(inspectId));
		list.add(getString(name));
		list.add(getString(mobile));
		list.add(getString(employeeName));
		list.add((score != null ? String.valueOf(score) : ""));
		list.add(getString(dateTime));
		return  list;
	}

	private String getString(String key) {
		return StringUtils.isNoneBlank(key) ? key : "";
	}
}
