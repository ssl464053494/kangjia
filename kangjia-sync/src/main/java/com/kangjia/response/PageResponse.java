package com.kangjia.response;

import java.util.ArrayList;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "结果")
public class PageResponse<T> extends AbstractResponse {

	@ApiModelProperty(value = "总记录数")
	private Integer total;
	@ApiModelProperty(value = "返回结果结合")
	private List<T> data = new ArrayList<>();

	public PageResponse(Integer total, List<T> data) {
		super(200);
		this.total = total;
		this.data = data;
	}

	public PageResponse(List<T> data) {
		super(200);
		this.data = data;
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	public List<T> getData() {
		return data;
	}

	public void setData(List<T> data) {
		this.data = data;
	}
}
