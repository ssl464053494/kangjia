package com.kangjia.response;

public class ObjectResponse<T> extends AbstractResponse {

	private T data;

	public ObjectResponse(T data) {
		super(200);
		this.data = data;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

}