package com.kangjia.response;

import io.swagger.annotations.ApiModelProperty;

/**
 * 响应结果
 * 
 * @author sunshulin
 *
 */
public abstract class AbstractResponse {

	/** 响应结果 */
	private Boolean success;
	/** 响应码 */
	@ApiModelProperty(value = "状态标识200成功,500错误或没有查询到数据")
	private Integer rpc;

	public AbstractResponse(Integer rpc) {
		super();
		this.rpc = rpc;
		this.success = this.rpc == 200;
	}

	public Boolean getSuccess() {
		return success;
	}

	public void setSuccess(Boolean success) {
		this.success = success;
	}

	public Integer getRpc() {
		return rpc;
	}

	public void setRpc(Integer rpc) {
		this.rpc = rpc;
	}
}
